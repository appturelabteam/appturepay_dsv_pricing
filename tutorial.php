<?php
/**
 * This file aims to show you how to use this generated package.
 * In addition, the goal is to show which methods are available and the first needed parameter(s)
 * You have to use an associative array such as:
 * - the key must be a constant beginning with WSDL_ from AbstractSoapClientBase class (each generated ServiceType class extends this class)
 * - the value must be the corresponding key value (each option matches a {@link http://www.php.net/manual/en/soapclient.soapclient.php} option)
 * $options = [
 * WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_URL => 'F:\Dewald\Appture\AppturePay\Globeflight\DSV\Integration\WSDL\Pricing\TMSPricingService.wsdl',
 * WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_TRACE => true,
 * WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_LOGIN => 'you_secret_login',
 * WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_PASSWORD => 'you_secret_password',
 * ];
 * etc...
 */
require_once __DIR__ . '/vendor/autoload.php';
/**
 * Minimal options
 */
$options = [
    WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_URL => 'F:\Dewald\Appture\AppturePay\Globeflight\DSV\Integration\WSDL\Pricing\TMSPricingService.wsdl',
    WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_CLASSMAP => \AppturePay\DSV\ClassMap::get(),
    WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_TRACE => true,
    WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_LOGIN => 'api.applabs.tst',
    WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_PASSWORD => '&$SS7V6B5zk5YvtiuCmT$P6b',
];
/**
 * Samples for Pricing ServiceType
 */
$pricing = new \AppturePay\DSV\ServiceType\Pricing($options);
/**
 * Sample call for pricingRequest operation/method
 */

$pricingType = new \AppturePay\DSV\StructType\PricingType(
        $ediCustomerNumber = null, // ?int 
        $ediCustomerDepartment = null, // ?\AppturePay\DSV\StructType\EdiCustomerDepartmentTypeComplex 
        $ediParm1 = null, // ?int 
        $ediParm2 = null, // ?string 
        $ediParm3 = null, // ?string 
        $transmitter = null, // ?string 
        $receiver = null, // ?string 
        $ediReference = null, // ?string 
        $referenceIndication = null, // ?string 
        $ediFunction1 = null, // ?string 
        $ediCustomerSearchName = null, // ?string 
        $dateTimeZone = null, // ?string 
        $file = null, // ?\AppturePay\DSV\StructType\FileType 
        $type = null // ?string
    );

$request = new \AppturePay\DSV\StructType\PricingRequest($pricingType);


if ($pricing->pricingRequest(new \AppturePay\DSV\StructType\PricingRequest()) !== false) {
    $responseBody = $pricing->getResult();
} else {
    $responseBody = $pricing->getLastError();
}

header("Content-type: application/json");
print json_encode($responseBody);
exit;
