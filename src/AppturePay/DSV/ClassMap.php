<?php

declare(strict_types=1);

namespace AppturePay\DSV;

/**
 * Class which returns the class map definition
 */
class ClassMap
{
    /**
     * Returns the mapping between the WSDL Structs and generated Structs' classes
     * This array is sent to the \SoapClient when calling the WS
     * @return string[]
     */
    final public static function get(): array
    {
        return [
            'pricingRequest' => '\\AppturePay\\DSV\\StructType\\PricingRequest',
            'pricingType' => '\\AppturePay\\DSV\\StructType\\PricingType',
            'ediCustomerDepartmentTypeComplex' => '\\AppturePay\\DSV\\StructType\\EdiCustomerDepartmentTypeComplex',
            'fileType' => '\\AppturePay\\DSV\\StructType\\FileType',
            'totalsType' => '\\AppturePay\\DSV\\StructType\\TotalsType',
            'dimensionType' => '\\AppturePay\\DSV\\StructType\\DimensionType',
            'parametersType' => '\\AppturePay\\DSV\\StructType\\ParametersType',
            'extraParametersType' => '\\AppturePay\\DSV\\StructType\\ExtraParametersType',
            'textKeysType' => '\\AppturePay\\DSV\\StructType\\TextKeysType',
            'addressType' => '\\AppturePay\\DSV\\StructType\\AddressType',
            'additionalType' => '\\AppturePay\\DSV\\StructType\\AdditionalType',
            'addressDetailsType' => '\\AppturePay\\DSV\\StructType\\AddressDetailsType',
            'contactInformationType' => '\\AppturePay\\DSV\\StructType\\ContactInformationType',
            'additionalServicesType' => '\\AppturePay\\DSV\\StructType\\AdditionalServicesType',
            'transportInstructionType' => '\\AppturePay\\DSV\\StructType\\TransportInstructionType',
            'planCharacteristicType' => '\\AppturePay\\DSV\\StructType\\PlanCharacteristicType',
            'goodsLineType' => '\\AppturePay\\DSV\\StructType\\GoodsLineType',
            'dangerousGoodsType' => '\\AppturePay\\DSV\\StructType\\DangerousGoodsType',
            'pricingRequestResponse' => '\\AppturePay\\DSV\\StructType\\PricingRequestResponse',
            'ediParametersType' => '\\AppturePay\\DSV\\StructType\\EdiParametersType',
            'loggingType' => '\\AppturePay\\DSV\\StructType\\LoggingType',
            'fileHeaderType' => '\\AppturePay\\DSV\\StructType\\FileHeaderType',
            'extraAmountType' => '\\AppturePay\\DSV\\StructType\\ExtraAmountType',
            'invoiceLineType' => '\\AppturePay\\DSV\\StructType\\InvoiceLineType',
            'activityCodeTypeComplex' => '\\AppturePay\\DSV\\StructType\\ActivityCodeTypeComplex',
            'vatType' => '\\AppturePay\\DSV\\StructType\\VatType',
            'weightCalculationType' => '\\AppturePay\\DSV\\StructType\\WeightCalculationType',
            'tariffBaseType' => '\\AppturePay\\DSV\\StructType\\TariffBaseType',
            'tariffSpecificationType' => '\\AppturePay\\DSV\\StructType\\TariffSpecificationType',
            'childType' => '\\AppturePay\\DSV\\StructType\\ChildType',
        ];
    }
}
