<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for planCharacteristicType StructType
 * @subpackage Structs
 */
class PlanCharacteristicType extends AbstractStructBase
{
    /**
     * The characteristicCode
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $characteristicCode = null;
    /**
     * The type
     * @var string|null
     */
    protected ?string $type = null;
    /**
     * Constructor method for planCharacteristicType
     * @uses PlanCharacteristicType::setCharacteristicCode()
     * @uses PlanCharacteristicType::setType()
     * @param string $characteristicCode
     * @param string $type
     */
    public function __construct(?string $characteristicCode = null, ?string $type = null)
    {
        $this
            ->setCharacteristicCode($characteristicCode)
            ->setType($type);
    }
    /**
     * Get characteristicCode value
     * @return string|null
     */
    public function getCharacteristicCode(): ?string
    {
        return $this->characteristicCode;
    }
    /**
     * Set characteristicCode value
     * @param string $characteristicCode
     * @return \AppturePay\DSV\StructType\PlanCharacteristicType
     */
    public function setCharacteristicCode(?string $characteristicCode = null): self
    {
        // validation for constraint: string
        if (!is_null($characteristicCode) && !is_string($characteristicCode)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($characteristicCode, true), gettype($characteristicCode)), __LINE__);
        }
        $this->characteristicCode = $characteristicCode;
        
        return $this;
    }
    /**
     * Get type value
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }
    /**
     * Set type value
     * @param string $type
     * @return \AppturePay\DSV\StructType\PlanCharacteristicType
     */
    public function setType(?string $type = null): self
    {
        // validation for constraint: string
        if (!is_null($type) && !is_string($type)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($type, true), gettype($type)), __LINE__);
        }
        $this->type = $type;
        
        return $this;
    }
}
