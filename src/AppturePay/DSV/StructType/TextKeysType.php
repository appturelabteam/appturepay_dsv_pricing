<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for textKeysType StructType
 * @subpackage Structs
 */
class TextKeysType extends AbstractStructBase
{
    /**
     * The textKey
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $textKey = null;
    /**
     * The date
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $date = null;
    /**
     * The fromTime
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $fromTime = null;
    /**
     * The toTime
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $toTime = null;
    /**
     * The additionalInformation
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $additionalInformation = null;
    /**
     * The type
     * @var string|null
     */
    protected ?string $type = null;
    /**
     * Constructor method for textKeysType
     * @uses TextKeysType::setTextKey()
     * @uses TextKeysType::setDate()
     * @uses TextKeysType::setFromTime()
     * @uses TextKeysType::setToTime()
     * @uses TextKeysType::setAdditionalInformation()
     * @uses TextKeysType::setType()
     * @param string $textKey
     * @param string $date
     * @param string $fromTime
     * @param string $toTime
     * @param string $additionalInformation
     * @param string $type
     */
    public function __construct(?string $textKey = null, ?string $date = null, ?string $fromTime = null, ?string $toTime = null, ?string $additionalInformation = null, ?string $type = null)
    {
        $this
            ->setTextKey($textKey)
            ->setDate($date)
            ->setFromTime($fromTime)
            ->setToTime($toTime)
            ->setAdditionalInformation($additionalInformation)
            ->setType($type);
    }
    /**
     * Get textKey value
     * @return string|null
     */
    public function getTextKey(): ?string
    {
        return $this->textKey;
    }
    /**
     * Set textKey value
     * @param string $textKey
     * @return \AppturePay\DSV\StructType\TextKeysType
     */
    public function setTextKey(?string $textKey = null): self
    {
        // validation for constraint: string
        if (!is_null($textKey) && !is_string($textKey)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($textKey, true), gettype($textKey)), __LINE__);
        }
        $this->textKey = $textKey;
        
        return $this;
    }
    /**
     * Get date value
     * @return string|null
     */
    public function getDate(): ?string
    {
        return $this->date;
    }
    /**
     * Set date value
     * @param string $date
     * @return \AppturePay\DSV\StructType\TextKeysType
     */
    public function setDate(?string $date = null): self
    {
        // validation for constraint: string
        if (!is_null($date) && !is_string($date)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($date, true), gettype($date)), __LINE__);
        }
        $this->date = $date;
        
        return $this;
    }
    /**
     * Get fromTime value
     * @return string|null
     */
    public function getFromTime(): ?string
    {
        return $this->fromTime;
    }
    /**
     * Set fromTime value
     * @param string $fromTime
     * @return \AppturePay\DSV\StructType\TextKeysType
     */
    public function setFromTime(?string $fromTime = null): self
    {
        // validation for constraint: string
        if (!is_null($fromTime) && !is_string($fromTime)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($fromTime, true), gettype($fromTime)), __LINE__);
        }
        $this->fromTime = $fromTime;
        
        return $this;
    }
    /**
     * Get toTime value
     * @return string|null
     */
    public function getToTime(): ?string
    {
        return $this->toTime;
    }
    /**
     * Set toTime value
     * @param string $toTime
     * @return \AppturePay\DSV\StructType\TextKeysType
     */
    public function setToTime(?string $toTime = null): self
    {
        // validation for constraint: string
        if (!is_null($toTime) && !is_string($toTime)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($toTime, true), gettype($toTime)), __LINE__);
        }
        $this->toTime = $toTime;
        
        return $this;
    }
    /**
     * Get additionalInformation value
     * @return string|null
     */
    public function getAdditionalInformation(): ?string
    {
        return $this->additionalInformation;
    }
    /**
     * Set additionalInformation value
     * @param string $additionalInformation
     * @return \AppturePay\DSV\StructType\TextKeysType
     */
    public function setAdditionalInformation(?string $additionalInformation = null): self
    {
        // validation for constraint: string
        if (!is_null($additionalInformation) && !is_string($additionalInformation)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($additionalInformation, true), gettype($additionalInformation)), __LINE__);
        }
        $this->additionalInformation = $additionalInformation;
        
        return $this;
    }
    /**
     * Get type value
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }
    /**
     * Set type value
     * @param string $type
     * @return \AppturePay\DSV\StructType\TextKeysType
     */
    public function setType(?string $type = null): self
    {
        // validation for constraint: string
        if (!is_null($type) && !is_string($type)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($type, true), gettype($type)), __LINE__);
        }
        $this->type = $type;
        
        return $this;
    }
}
