<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ediCustomerDepartmentTypeComplex StructType
 * @subpackage Structs
 */
class EdiCustomerDepartmentTypeComplex extends AbstractStructBase
{
    /**
     * The _
     * @var string|null
     */
    protected ?string $_ = null;
    /**
     * The routing1
     * @var string|null
     */
    protected ?string $routing1 = null;
    /**
     * The routing2
     * @var string|null
     */
    protected ?string $routing2 = null;
    /**
     * The routing3
     * @var string|null
     */
    protected ?string $routing3 = null;
    /**
     * The routing4
     * @var string|null
     */
    protected ?string $routing4 = null;
    /**
     * The routing5
     * @var string|null
     */
    protected ?string $routing5 = null;
    /**
     * Constructor method for ediCustomerDepartmentTypeComplex
     * @uses EdiCustomerDepartmentTypeComplex::set_()
     * @uses EdiCustomerDepartmentTypeComplex::setRouting1()
     * @uses EdiCustomerDepartmentTypeComplex::setRouting2()
     * @uses EdiCustomerDepartmentTypeComplex::setRouting3()
     * @uses EdiCustomerDepartmentTypeComplex::setRouting4()
     * @uses EdiCustomerDepartmentTypeComplex::setRouting5()
     * @param string $_
     * @param string $routing1
     * @param string $routing2
     * @param string $routing3
     * @param string $routing4
     * @param string $routing5
     */
    public function __construct(?string $_ = null, ?string $routing1 = null, ?string $routing2 = null, ?string $routing3 = null, ?string $routing4 = null, ?string $routing5 = null)
    {
        $this
            ->set_($_)
            ->setRouting1($routing1)
            ->setRouting2($routing2)
            ->setRouting3($routing3)
            ->setRouting4($routing4)
            ->setRouting5($routing5);
    }
    /**
     * Get _ value
     * @return string|null
     */
    public function get_(): ?string
    {
        return $this->_;
    }
    /**
     * Set _ value
     * @param string $_
     * @return \AppturePay\DSV\StructType\EdiCustomerDepartmentTypeComplex
     */
    public function set_(?string $_ = null): self
    {
        // validation for constraint: string
        if (!is_null($_) && !is_string($_)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($_, true), gettype($_)), __LINE__);
        }
        $this->_ = $_;
        
        return $this;
    }
    /**
     * Get routing1 value
     * @return string|null
     */
    public function getRouting1(): ?string
    {
        return $this->routing1;
    }
    /**
     * Set routing1 value
     * @param string $routing1
     * @return \AppturePay\DSV\StructType\EdiCustomerDepartmentTypeComplex
     */
    public function setRouting1(?string $routing1 = null): self
    {
        // validation for constraint: string
        if (!is_null($routing1) && !is_string($routing1)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($routing1, true), gettype($routing1)), __LINE__);
        }
        $this->routing1 = $routing1;
        
        return $this;
    }
    /**
     * Get routing2 value
     * @return string|null
     */
    public function getRouting2(): ?string
    {
        return $this->routing2;
    }
    /**
     * Set routing2 value
     * @param string $routing2
     * @return \AppturePay\DSV\StructType\EdiCustomerDepartmentTypeComplex
     */
    public function setRouting2(?string $routing2 = null): self
    {
        // validation for constraint: string
        if (!is_null($routing2) && !is_string($routing2)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($routing2, true), gettype($routing2)), __LINE__);
        }
        $this->routing2 = $routing2;
        
        return $this;
    }
    /**
     * Get routing3 value
     * @return string|null
     */
    public function getRouting3(): ?string
    {
        return $this->routing3;
    }
    /**
     * Set routing3 value
     * @param string $routing3
     * @return \AppturePay\DSV\StructType\EdiCustomerDepartmentTypeComplex
     */
    public function setRouting3(?string $routing3 = null): self
    {
        // validation for constraint: string
        if (!is_null($routing3) && !is_string($routing3)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($routing3, true), gettype($routing3)), __LINE__);
        }
        $this->routing3 = $routing3;
        
        return $this;
    }
    /**
     * Get routing4 value
     * @return string|null
     */
    public function getRouting4(): ?string
    {
        return $this->routing4;
    }
    /**
     * Set routing4 value
     * @param string $routing4
     * @return \AppturePay\DSV\StructType\EdiCustomerDepartmentTypeComplex
     */
    public function setRouting4(?string $routing4 = null): self
    {
        // validation for constraint: string
        if (!is_null($routing4) && !is_string($routing4)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($routing4, true), gettype($routing4)), __LINE__);
        }
        $this->routing4 = $routing4;
        
        return $this;
    }
    /**
     * Get routing5 value
     * @return string|null
     */
    public function getRouting5(): ?string
    {
        return $this->routing5;
    }
    /**
     * Set routing5 value
     * @param string $routing5
     * @return \AppturePay\DSV\StructType\EdiCustomerDepartmentTypeComplex
     */
    public function setRouting5(?string $routing5 = null): self
    {
        // validation for constraint: string
        if (!is_null($routing5) && !is_string($routing5)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($routing5, true), gettype($routing5)), __LINE__);
        }
        $this->routing5 = $routing5;
        
        return $this;
    }
}
