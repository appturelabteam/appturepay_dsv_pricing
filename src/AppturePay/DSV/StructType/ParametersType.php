<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for parametersType StructType
 * @subpackage Structs
 */
class ParametersType extends AbstractStructBase
{
    /**
     * The parameterYn01
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $parameterYn01 = null;
    /**
     * The parameterYn02
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $parameterYn02 = null;
    /**
     * The parameterYn03
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $parameterYn03 = null;
    /**
     * The parameterYn04
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $parameterYn04 = null;
    /**
     * The parameterYn05
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $parameterYn05 = null;
    /**
     * The parameterYn06
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $parameterYn06 = null;
    /**
     * The parameterYn07
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $parameterYn07 = null;
    /**
     * The parameterYn08
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $parameterYn08 = null;
    /**
     * The parameterYn09
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $parameterYn09 = null;
    /**
     * The parameterYn10
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $parameterYn10 = null;
    /**
     * Constructor method for parametersType
     * @uses ParametersType::setParameterYn01()
     * @uses ParametersType::setParameterYn02()
     * @uses ParametersType::setParameterYn03()
     * @uses ParametersType::setParameterYn04()
     * @uses ParametersType::setParameterYn05()
     * @uses ParametersType::setParameterYn06()
     * @uses ParametersType::setParameterYn07()
     * @uses ParametersType::setParameterYn08()
     * @uses ParametersType::setParameterYn09()
     * @uses ParametersType::setParameterYn10()
     * @param string $parameterYn01
     * @param string $parameterYn02
     * @param string $parameterYn03
     * @param string $parameterYn04
     * @param string $parameterYn05
     * @param string $parameterYn06
     * @param string $parameterYn07
     * @param string $parameterYn08
     * @param string $parameterYn09
     * @param string $parameterYn10
     */
    public function __construct(?string $parameterYn01 = null, ?string $parameterYn02 = null, ?string $parameterYn03 = null, ?string $parameterYn04 = null, ?string $parameterYn05 = null, ?string $parameterYn06 = null, ?string $parameterYn07 = null, ?string $parameterYn08 = null, ?string $parameterYn09 = null, ?string $parameterYn10 = null)
    {
        $this
            ->setParameterYn01($parameterYn01)
            ->setParameterYn02($parameterYn02)
            ->setParameterYn03($parameterYn03)
            ->setParameterYn04($parameterYn04)
            ->setParameterYn05($parameterYn05)
            ->setParameterYn06($parameterYn06)
            ->setParameterYn07($parameterYn07)
            ->setParameterYn08($parameterYn08)
            ->setParameterYn09($parameterYn09)
            ->setParameterYn10($parameterYn10);
    }
    /**
     * Get parameterYn01 value
     * @return string|null
     */
    public function getParameterYn01(): ?string
    {
        return $this->parameterYn01;
    }
    /**
     * Set parameterYn01 value
     * @param string $parameterYn01
     * @return \AppturePay\DSV\StructType\ParametersType
     */
    public function setParameterYn01(?string $parameterYn01 = null): self
    {
        // validation for constraint: string
        if (!is_null($parameterYn01) && !is_string($parameterYn01)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($parameterYn01, true), gettype($parameterYn01)), __LINE__);
        }
        $this->parameterYn01 = $parameterYn01;
        
        return $this;
    }
    /**
     * Get parameterYn02 value
     * @return string|null
     */
    public function getParameterYn02(): ?string
    {
        return $this->parameterYn02;
    }
    /**
     * Set parameterYn02 value
     * @param string $parameterYn02
     * @return \AppturePay\DSV\StructType\ParametersType
     */
    public function setParameterYn02(?string $parameterYn02 = null): self
    {
        // validation for constraint: string
        if (!is_null($parameterYn02) && !is_string($parameterYn02)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($parameterYn02, true), gettype($parameterYn02)), __LINE__);
        }
        $this->parameterYn02 = $parameterYn02;
        
        return $this;
    }
    /**
     * Get parameterYn03 value
     * @return string|null
     */
    public function getParameterYn03(): ?string
    {
        return $this->parameterYn03;
    }
    /**
     * Set parameterYn03 value
     * @param string $parameterYn03
     * @return \AppturePay\DSV\StructType\ParametersType
     */
    public function setParameterYn03(?string $parameterYn03 = null): self
    {
        // validation for constraint: string
        if (!is_null($parameterYn03) && !is_string($parameterYn03)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($parameterYn03, true), gettype($parameterYn03)), __LINE__);
        }
        $this->parameterYn03 = $parameterYn03;
        
        return $this;
    }
    /**
     * Get parameterYn04 value
     * @return string|null
     */
    public function getParameterYn04(): ?string
    {
        return $this->parameterYn04;
    }
    /**
     * Set parameterYn04 value
     * @param string $parameterYn04
     * @return \AppturePay\DSV\StructType\ParametersType
     */
    public function setParameterYn04(?string $parameterYn04 = null): self
    {
        // validation for constraint: string
        if (!is_null($parameterYn04) && !is_string($parameterYn04)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($parameterYn04, true), gettype($parameterYn04)), __LINE__);
        }
        $this->parameterYn04 = $parameterYn04;
        
        return $this;
    }
    /**
     * Get parameterYn05 value
     * @return string|null
     */
    public function getParameterYn05(): ?string
    {
        return $this->parameterYn05;
    }
    /**
     * Set parameterYn05 value
     * @param string $parameterYn05
     * @return \AppturePay\DSV\StructType\ParametersType
     */
    public function setParameterYn05(?string $parameterYn05 = null): self
    {
        // validation for constraint: string
        if (!is_null($parameterYn05) && !is_string($parameterYn05)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($parameterYn05, true), gettype($parameterYn05)), __LINE__);
        }
        $this->parameterYn05 = $parameterYn05;
        
        return $this;
    }
    /**
     * Get parameterYn06 value
     * @return string|null
     */
    public function getParameterYn06(): ?string
    {
        return $this->parameterYn06;
    }
    /**
     * Set parameterYn06 value
     * @param string $parameterYn06
     * @return \AppturePay\DSV\StructType\ParametersType
     */
    public function setParameterYn06(?string $parameterYn06 = null): self
    {
        // validation for constraint: string
        if (!is_null($parameterYn06) && !is_string($parameterYn06)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($parameterYn06, true), gettype($parameterYn06)), __LINE__);
        }
        $this->parameterYn06 = $parameterYn06;
        
        return $this;
    }
    /**
     * Get parameterYn07 value
     * @return string|null
     */
    public function getParameterYn07(): ?string
    {
        return $this->parameterYn07;
    }
    /**
     * Set parameterYn07 value
     * @param string $parameterYn07
     * @return \AppturePay\DSV\StructType\ParametersType
     */
    public function setParameterYn07(?string $parameterYn07 = null): self
    {
        // validation for constraint: string
        if (!is_null($parameterYn07) && !is_string($parameterYn07)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($parameterYn07, true), gettype($parameterYn07)), __LINE__);
        }
        $this->parameterYn07 = $parameterYn07;
        
        return $this;
    }
    /**
     * Get parameterYn08 value
     * @return string|null
     */
    public function getParameterYn08(): ?string
    {
        return $this->parameterYn08;
    }
    /**
     * Set parameterYn08 value
     * @param string $parameterYn08
     * @return \AppturePay\DSV\StructType\ParametersType
     */
    public function setParameterYn08(?string $parameterYn08 = null): self
    {
        // validation for constraint: string
        if (!is_null($parameterYn08) && !is_string($parameterYn08)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($parameterYn08, true), gettype($parameterYn08)), __LINE__);
        }
        $this->parameterYn08 = $parameterYn08;
        
        return $this;
    }
    /**
     * Get parameterYn09 value
     * @return string|null
     */
    public function getParameterYn09(): ?string
    {
        return $this->parameterYn09;
    }
    /**
     * Set parameterYn09 value
     * @param string $parameterYn09
     * @return \AppturePay\DSV\StructType\ParametersType
     */
    public function setParameterYn09(?string $parameterYn09 = null): self
    {
        // validation for constraint: string
        if (!is_null($parameterYn09) && !is_string($parameterYn09)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($parameterYn09, true), gettype($parameterYn09)), __LINE__);
        }
        $this->parameterYn09 = $parameterYn09;
        
        return $this;
    }
    /**
     * Get parameterYn10 value
     * @return string|null
     */
    public function getParameterYn10(): ?string
    {
        return $this->parameterYn10;
    }
    /**
     * Set parameterYn10 value
     * @param string $parameterYn10
     * @return \AppturePay\DSV\StructType\ParametersType
     */
    public function setParameterYn10(?string $parameterYn10 = null): self
    {
        // validation for constraint: string
        if (!is_null($parameterYn10) && !is_string($parameterYn10)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($parameterYn10, true), gettype($parameterYn10)), __LINE__);
        }
        $this->parameterYn10 = $parameterYn10;
        
        return $this;
    }
}
