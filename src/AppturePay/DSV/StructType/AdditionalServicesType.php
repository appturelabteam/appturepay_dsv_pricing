<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for additionalServicesType StructType
 * @subpackage Structs
 */
class AdditionalServicesType extends AbstractStructBase
{
    /**
     * The serviceCode
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $serviceCode = null;
    /**
     * The tariffBaseValue
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $tariffBaseValue = null;
    /**
     * The conditionValue
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $conditionValue = null;
    /**
     * The type
     * @var string|null
     */
    protected ?string $type = null;
    /**
     * Constructor method for additionalServicesType
     * @uses AdditionalServicesType::setServiceCode()
     * @uses AdditionalServicesType::setTariffBaseValue()
     * @uses AdditionalServicesType::setConditionValue()
     * @uses AdditionalServicesType::setType()
     * @param string $serviceCode
     * @param float $tariffBaseValue
     * @param string $conditionValue
     * @param string $type
     */
    public function __construct(?string $serviceCode = null, ?float $tariffBaseValue = null, ?string $conditionValue = null, ?string $type = null)
    {
        $this
            ->setServiceCode($serviceCode)
            ->setTariffBaseValue($tariffBaseValue)
            ->setConditionValue($conditionValue)
            ->setType($type);
    }
    /**
     * Get serviceCode value
     * @return string|null
     */
    public function getServiceCode(): ?string
    {
        return $this->serviceCode;
    }
    /**
     * Set serviceCode value
     * @param string $serviceCode
     * @return \AppturePay\DSV\StructType\AdditionalServicesType
     */
    public function setServiceCode(?string $serviceCode = null): self
    {
        // validation for constraint: string
        if (!is_null($serviceCode) && !is_string($serviceCode)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($serviceCode, true), gettype($serviceCode)), __LINE__);
        }
        $this->serviceCode = $serviceCode;
        
        return $this;
    }
    /**
     * Get tariffBaseValue value
     * @return float|null
     */
    public function getTariffBaseValue(): ?float
    {
        return $this->tariffBaseValue;
    }
    /**
     * Set tariffBaseValue value
     * @param float $tariffBaseValue
     * @return \AppturePay\DSV\StructType\AdditionalServicesType
     */
    public function setTariffBaseValue(?float $tariffBaseValue = null): self
    {
        // validation for constraint: float
        if (!is_null($tariffBaseValue) && !(is_float($tariffBaseValue) || is_numeric($tariffBaseValue))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($tariffBaseValue, true), gettype($tariffBaseValue)), __LINE__);
        }
        $this->tariffBaseValue = $tariffBaseValue;
        
        return $this;
    }
    /**
     * Get conditionValue value
     * @return string|null
     */
    public function getConditionValue(): ?string
    {
        return $this->conditionValue;
    }
    /**
     * Set conditionValue value
     * @param string $conditionValue
     * @return \AppturePay\DSV\StructType\AdditionalServicesType
     */
    public function setConditionValue(?string $conditionValue = null): self
    {
        // validation for constraint: string
        if (!is_null($conditionValue) && !is_string($conditionValue)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($conditionValue, true), gettype($conditionValue)), __LINE__);
        }
        $this->conditionValue = $conditionValue;
        
        return $this;
    }
    /**
     * Get type value
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }
    /**
     * Set type value
     * @param string $type
     * @return \AppturePay\DSV\StructType\AdditionalServicesType
     */
    public function setType(?string $type = null): self
    {
        // validation for constraint: string
        if (!is_null($type) && !is_string($type)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($type, true), gettype($type)), __LINE__);
        }
        $this->type = $type;
        
        return $this;
    }
}
