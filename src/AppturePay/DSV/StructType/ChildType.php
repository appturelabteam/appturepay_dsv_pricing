<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for childType StructType
 * @subpackage Structs
 */
class ChildType extends AbstractStructBase
{
    /**
     * The ediReference
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $ediReference = null;
    /**
     * The referenceIndication
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $referenceIndication = null;
    /**
     * The internalNumber
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $internalNumber = null;
    /**
     * The ediFunction1
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $ediFunction1 = null;
    /**
     * The logging
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\LoggingType[]
     */
    protected ?array $logging = null;
    /**
     * The file
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\FileType|null
     */
    protected ?\AppturePay\DSV\StructType\FileType $file = null;
    /**
     * The type
     * @var string|null
     */
    protected ?string $type = null;
    /**
     * Constructor method for childType
     * @uses ChildType::setEdiReference()
     * @uses ChildType::setReferenceIndication()
     * @uses ChildType::setInternalNumber()
     * @uses ChildType::setEdiFunction1()
     * @uses ChildType::setLogging()
     * @uses ChildType::setFile()
     * @uses ChildType::setType()
     * @param string $ediReference
     * @param string $referenceIndication
     * @param int $internalNumber
     * @param string $ediFunction1
     * @param \AppturePay\DSV\StructType\LoggingType[] $logging
     * @param \AppturePay\DSV\StructType\FileType $file
     * @param string $type
     */
    public function __construct(?string $ediReference = null, ?string $referenceIndication = null, ?int $internalNumber = null, ?string $ediFunction1 = null, ?array $logging = null, ?\AppturePay\DSV\StructType\FileType $file = null, ?string $type = null)
    {
        $this
            ->setEdiReference($ediReference)
            ->setReferenceIndication($referenceIndication)
            ->setInternalNumber($internalNumber)
            ->setEdiFunction1($ediFunction1)
            ->setLogging($logging)
            ->setFile($file)
            ->setType($type);
    }
    /**
     * Get ediReference value
     * @return string|null
     */
    public function getEdiReference(): ?string
    {
        return $this->ediReference;
    }
    /**
     * Set ediReference value
     * @param string $ediReference
     * @return \AppturePay\DSV\StructType\ChildType
     */
    public function setEdiReference(?string $ediReference = null): self
    {
        // validation for constraint: string
        if (!is_null($ediReference) && !is_string($ediReference)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($ediReference, true), gettype($ediReference)), __LINE__);
        }
        $this->ediReference = $ediReference;
        
        return $this;
    }
    /**
     * Get referenceIndication value
     * @return string|null
     */
    public function getReferenceIndication(): ?string
    {
        return $this->referenceIndication;
    }
    /**
     * Set referenceIndication value
     * @uses \AppturePay\DSV\EnumType\ReferenceIndicationType::valueIsValid()
     * @uses \AppturePay\DSV\EnumType\ReferenceIndicationType::getValidValues()
     * @throws InvalidArgumentException
     * @param string $referenceIndication
     * @return \AppturePay\DSV\StructType\ChildType
     */
    public function setReferenceIndication(?string $referenceIndication = null): self
    {
        // validation for constraint: enumeration
        if (!\AppturePay\DSV\EnumType\ReferenceIndicationType::valueIsValid($referenceIndication)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \AppturePay\DSV\EnumType\ReferenceIndicationType', is_array($referenceIndication) ? implode(', ', $referenceIndication) : var_export($referenceIndication, true), implode(', ', \AppturePay\DSV\EnumType\ReferenceIndicationType::getValidValues())), __LINE__);
        }
        $this->referenceIndication = $referenceIndication;
        
        return $this;
    }
    /**
     * Get internalNumber value
     * @return int|null
     */
    public function getInternalNumber(): ?int
    {
        return $this->internalNumber;
    }
    /**
     * Set internalNumber value
     * @param int $internalNumber
     * @return \AppturePay\DSV\StructType\ChildType
     */
    public function setInternalNumber(?int $internalNumber = null): self
    {
        // validation for constraint: int
        if (!is_null($internalNumber) && !(is_int($internalNumber) || ctype_digit($internalNumber))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($internalNumber, true), gettype($internalNumber)), __LINE__);
        }
        $this->internalNumber = $internalNumber;
        
        return $this;
    }
    /**
     * Get ediFunction1 value
     * @return string|null
     */
    public function getEdiFunction1(): ?string
    {
        return $this->ediFunction1;
    }
    /**
     * Set ediFunction1 value
     * @param string $ediFunction1
     * @return \AppturePay\DSV\StructType\ChildType
     */
    public function setEdiFunction1(?string $ediFunction1 = null): self
    {
        // validation for constraint: string
        if (!is_null($ediFunction1) && !is_string($ediFunction1)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($ediFunction1, true), gettype($ediFunction1)), __LINE__);
        }
        $this->ediFunction1 = $ediFunction1;
        
        return $this;
    }
    /**
     * Get logging value
     * @return \AppturePay\DSV\StructType\LoggingType[]
     */
    public function getLogging(): ?array
    {
        return $this->logging;
    }
    /**
     * This method is responsible for validating the values passed to the setLogging method
     * This method is willingly generated in order to preserve the one-line inline validation within the setLogging method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateLoggingForArrayConstraintsFromSetLogging(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $childTypeLoggingItem) {
            // validation for constraint: itemType
            if (!$childTypeLoggingItem instanceof \AppturePay\DSV\StructType\LoggingType) {
                $invalidValues[] = is_object($childTypeLoggingItem) ? get_class($childTypeLoggingItem) : sprintf('%s(%s)', gettype($childTypeLoggingItem), var_export($childTypeLoggingItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The logging property can only contain items of type \AppturePay\DSV\StructType\LoggingType, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set logging value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\LoggingType[] $logging
     * @return \AppturePay\DSV\StructType\ChildType
     */
    public function setLogging(?array $logging = null): self
    {
        // validation for constraint: array
        if ('' !== ($loggingArrayErrorMessage = self::validateLoggingForArrayConstraintsFromSetLogging($logging))) {
            throw new InvalidArgumentException($loggingArrayErrorMessage, __LINE__);
        }
        $this->logging = $logging;
        
        return $this;
    }
    /**
     * Add item to logging value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\LoggingType $item
     * @return \AppturePay\DSV\StructType\ChildType
     */
    public function addToLogging(\AppturePay\DSV\StructType\LoggingType $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \AppturePay\DSV\StructType\LoggingType) {
            throw new InvalidArgumentException(sprintf('The logging property can only contain items of type \AppturePay\DSV\StructType\LoggingType, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->logging[] = $item;
        
        return $this;
    }
    /**
     * Get file value
     * @return \AppturePay\DSV\StructType\FileType|null
     */
    public function getFile(): ?\AppturePay\DSV\StructType\FileType
    {
        return $this->file;
    }
    /**
     * Set file value
     * @param \AppturePay\DSV\StructType\FileType $file
     * @return \AppturePay\DSV\StructType\ChildType
     */
    public function setFile(?\AppturePay\DSV\StructType\FileType $file = null): self
    {
        $this->file = $file;
        
        return $this;
    }
    /**
     * Get type value
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }
    /**
     * Set type value
     * @param string $type
     * @return \AppturePay\DSV\StructType\ChildType
     */
    public function setType(?string $type = null): self
    {
        // validation for constraint: string
        if (!is_null($type) && !is_string($type)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($type, true), gettype($type)), __LINE__);
        }
        $this->type = $type;
        
        return $this;
    }
}
