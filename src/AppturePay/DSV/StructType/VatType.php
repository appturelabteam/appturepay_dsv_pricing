<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for vatType StructType
 * @subpackage Structs
 */
class VatType extends AbstractStructBase
{
    /**
     * The countryCode
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $countryCode = null;
    /**
     * The typeOfVat
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $typeOfVat = null;
    /**
     * The vatCode
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $vatCode = null;
    /**
     * The vatAmount
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $vatAmount = null;
    /**
     * Constructor method for vatType
     * @uses VatType::setCountryCode()
     * @uses VatType::setTypeOfVat()
     * @uses VatType::setVatCode()
     * @uses VatType::setVatAmount()
     * @param string $countryCode
     * @param int $typeOfVat
     * @param int $vatCode
     * @param float $vatAmount
     */
    public function __construct(?string $countryCode = null, ?int $typeOfVat = null, ?int $vatCode = null, ?float $vatAmount = null)
    {
        $this
            ->setCountryCode($countryCode)
            ->setTypeOfVat($typeOfVat)
            ->setVatCode($vatCode)
            ->setVatAmount($vatAmount);
    }
    /**
     * Get countryCode value
     * @return string|null
     */
    public function getCountryCode(): ?string
    {
        return $this->countryCode;
    }
    /**
     * Set countryCode value
     * @param string $countryCode
     * @return \AppturePay\DSV\StructType\VatType
     */
    public function setCountryCode(?string $countryCode = null): self
    {
        // validation for constraint: string
        if (!is_null($countryCode) && !is_string($countryCode)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($countryCode, true), gettype($countryCode)), __LINE__);
        }
        $this->countryCode = $countryCode;
        
        return $this;
    }
    /**
     * Get typeOfVat value
     * @return int|null
     */
    public function getTypeOfVat(): ?int
    {
        return $this->typeOfVat;
    }
    /**
     * Set typeOfVat value
     * @param int $typeOfVat
     * @return \AppturePay\DSV\StructType\VatType
     */
    public function setTypeOfVat(?int $typeOfVat = null): self
    {
        // validation for constraint: int
        if (!is_null($typeOfVat) && !(is_int($typeOfVat) || ctype_digit($typeOfVat))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($typeOfVat, true), gettype($typeOfVat)), __LINE__);
        }
        $this->typeOfVat = $typeOfVat;
        
        return $this;
    }
    /**
     * Get vatCode value
     * @return int|null
     */
    public function getVatCode(): ?int
    {
        return $this->vatCode;
    }
    /**
     * Set vatCode value
     * @param int $vatCode
     * @return \AppturePay\DSV\StructType\VatType
     */
    public function setVatCode(?int $vatCode = null): self
    {
        // validation for constraint: int
        if (!is_null($vatCode) && !(is_int($vatCode) || ctype_digit($vatCode))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($vatCode, true), gettype($vatCode)), __LINE__);
        }
        $this->vatCode = $vatCode;
        
        return $this;
    }
    /**
     * Get vatAmount value
     * @return float|null
     */
    public function getVatAmount(): ?float
    {
        return $this->vatAmount;
    }
    /**
     * Set vatAmount value
     * @param float $vatAmount
     * @return \AppturePay\DSV\StructType\VatType
     */
    public function setVatAmount(?float $vatAmount = null): self
    {
        // validation for constraint: float
        if (!is_null($vatAmount) && !(is_float($vatAmount) || is_numeric($vatAmount))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($vatAmount, true), gettype($vatAmount)), __LINE__);
        }
        $this->vatAmount = $vatAmount;
        
        return $this;
    }
}
