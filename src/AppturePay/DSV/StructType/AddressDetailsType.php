<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for addressDetailsType StructType
 * @subpackage Structs
 */
class AddressDetailsType extends AbstractStructBase
{
    /**
     * The nameLine1
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $nameLine1 = null;
    /**
     * The nameLine2
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $nameLine2 = null;
    /**
     * The nameLine3
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $nameLine3 = null;
    /**
     * The addressLine1
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $addressLine1 = null;
    /**
     * The addressLine2
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $addressLine2 = null;
    /**
     * The addressLine3
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $addressLine3 = null;
    /**
     * The cityName
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $cityName = null;
    /**
     * The cityName2
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $cityName2 = null;
    /**
     * The destinationCode
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $destinationCode = null;
    /**
     * The subentityCode
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $subentityCode = null;
    /**
     * The postalCode
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $postalCode = null;
    /**
     * The countryCode
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $countryCode = null;
    /**
     * Constructor method for addressDetailsType
     * @uses AddressDetailsType::setNameLine1()
     * @uses AddressDetailsType::setNameLine2()
     * @uses AddressDetailsType::setNameLine3()
     * @uses AddressDetailsType::setAddressLine1()
     * @uses AddressDetailsType::setAddressLine2()
     * @uses AddressDetailsType::setAddressLine3()
     * @uses AddressDetailsType::setCityName()
     * @uses AddressDetailsType::setCityName2()
     * @uses AddressDetailsType::setDestinationCode()
     * @uses AddressDetailsType::setSubentityCode()
     * @uses AddressDetailsType::setPostalCode()
     * @uses AddressDetailsType::setCountryCode()
     * @param string $nameLine1
     * @param string $nameLine2
     * @param string $nameLine3
     * @param string $addressLine1
     * @param string $addressLine2
     * @param string $addressLine3
     * @param string $cityName
     * @param string $cityName2
     * @param string $destinationCode
     * @param string $subentityCode
     * @param string $postalCode
     * @param string $countryCode
     */
    public function __construct(?string $nameLine1 = null, ?string $nameLine2 = null, ?string $nameLine3 = null, ?string $addressLine1 = null, ?string $addressLine2 = null, ?string $addressLine3 = null, ?string $cityName = null, ?string $cityName2 = null, ?string $destinationCode = null, ?string $subentityCode = null, ?string $postalCode = null, ?string $countryCode = null)
    {
        $this
            ->setNameLine1($nameLine1)
            ->setNameLine2($nameLine2)
            ->setNameLine3($nameLine3)
            ->setAddressLine1($addressLine1)
            ->setAddressLine2($addressLine2)
            ->setAddressLine3($addressLine3)
            ->setCityName($cityName)
            ->setCityName2($cityName2)
            ->setDestinationCode($destinationCode)
            ->setSubentityCode($subentityCode)
            ->setPostalCode($postalCode)
            ->setCountryCode($countryCode);
    }
    /**
     * Get nameLine1 value
     * @return string|null
     */
    public function getNameLine1(): ?string
    {
        return $this->nameLine1;
    }
    /**
     * Set nameLine1 value
     * @param string $nameLine1
     * @return \AppturePay\DSV\StructType\AddressDetailsType
     */
    public function setNameLine1(?string $nameLine1 = null): self
    {
        // validation for constraint: string
        if (!is_null($nameLine1) && !is_string($nameLine1)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($nameLine1, true), gettype($nameLine1)), __LINE__);
        }
        $this->nameLine1 = $nameLine1;
        
        return $this;
    }
    /**
     * Get nameLine2 value
     * @return string|null
     */
    public function getNameLine2(): ?string
    {
        return $this->nameLine2;
    }
    /**
     * Set nameLine2 value
     * @param string $nameLine2
     * @return \AppturePay\DSV\StructType\AddressDetailsType
     */
    public function setNameLine2(?string $nameLine2 = null): self
    {
        // validation for constraint: string
        if (!is_null($nameLine2) && !is_string($nameLine2)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($nameLine2, true), gettype($nameLine2)), __LINE__);
        }
        $this->nameLine2 = $nameLine2;
        
        return $this;
    }
    /**
     * Get nameLine3 value
     * @return string|null
     */
    public function getNameLine3(): ?string
    {
        return $this->nameLine3;
    }
    /**
     * Set nameLine3 value
     * @param string $nameLine3
     * @return \AppturePay\DSV\StructType\AddressDetailsType
     */
    public function setNameLine3(?string $nameLine3 = null): self
    {
        // validation for constraint: string
        if (!is_null($nameLine3) && !is_string($nameLine3)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($nameLine3, true), gettype($nameLine3)), __LINE__);
        }
        $this->nameLine3 = $nameLine3;
        
        return $this;
    }
    /**
     * Get addressLine1 value
     * @return string|null
     */
    public function getAddressLine1(): ?string
    {
        return $this->addressLine1;
    }
    /**
     * Set addressLine1 value
     * @param string $addressLine1
     * @return \AppturePay\DSV\StructType\AddressDetailsType
     */
    public function setAddressLine1(?string $addressLine1 = null): self
    {
        // validation for constraint: string
        if (!is_null($addressLine1) && !is_string($addressLine1)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($addressLine1, true), gettype($addressLine1)), __LINE__);
        }
        $this->addressLine1 = $addressLine1;
        
        return $this;
    }
    /**
     * Get addressLine2 value
     * @return string|null
     */
    public function getAddressLine2(): ?string
    {
        return $this->addressLine2;
    }
    /**
     * Set addressLine2 value
     * @param string $addressLine2
     * @return \AppturePay\DSV\StructType\AddressDetailsType
     */
    public function setAddressLine2(?string $addressLine2 = null): self
    {
        // validation for constraint: string
        if (!is_null($addressLine2) && !is_string($addressLine2)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($addressLine2, true), gettype($addressLine2)), __LINE__);
        }
        $this->addressLine2 = $addressLine2;
        
        return $this;
    }
    /**
     * Get addressLine3 value
     * @return string|null
     */
    public function getAddressLine3(): ?string
    {
        return $this->addressLine3;
    }
    /**
     * Set addressLine3 value
     * @param string $addressLine3
     * @return \AppturePay\DSV\StructType\AddressDetailsType
     */
    public function setAddressLine3(?string $addressLine3 = null): self
    {
        // validation for constraint: string
        if (!is_null($addressLine3) && !is_string($addressLine3)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($addressLine3, true), gettype($addressLine3)), __LINE__);
        }
        $this->addressLine3 = $addressLine3;
        
        return $this;
    }
    /**
     * Get cityName value
     * @return string|null
     */
    public function getCityName(): ?string
    {
        return $this->cityName;
    }
    /**
     * Set cityName value
     * @param string $cityName
     * @return \AppturePay\DSV\StructType\AddressDetailsType
     */
    public function setCityName(?string $cityName = null): self
    {
        // validation for constraint: string
        if (!is_null($cityName) && !is_string($cityName)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($cityName, true), gettype($cityName)), __LINE__);
        }
        $this->cityName = $cityName;
        
        return $this;
    }
    /**
     * Get cityName2 value
     * @return string|null
     */
    public function getCityName2(): ?string
    {
        return $this->cityName2;
    }
    /**
     * Set cityName2 value
     * @param string $cityName2
     * @return \AppturePay\DSV\StructType\AddressDetailsType
     */
    public function setCityName2(?string $cityName2 = null): self
    {
        // validation for constraint: string
        if (!is_null($cityName2) && !is_string($cityName2)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($cityName2, true), gettype($cityName2)), __LINE__);
        }
        $this->cityName2 = $cityName2;
        
        return $this;
    }
    /**
     * Get destinationCode value
     * @return string|null
     */
    public function getDestinationCode(): ?string
    {
        return $this->destinationCode;
    }
    /**
     * Set destinationCode value
     * @param string $destinationCode
     * @return \AppturePay\DSV\StructType\AddressDetailsType
     */
    public function setDestinationCode(?string $destinationCode = null): self
    {
        // validation for constraint: string
        if (!is_null($destinationCode) && !is_string($destinationCode)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($destinationCode, true), gettype($destinationCode)), __LINE__);
        }
        $this->destinationCode = $destinationCode;
        
        return $this;
    }
    /**
     * Get subentityCode value
     * @return string|null
     */
    public function getSubentityCode(): ?string
    {
        return $this->subentityCode;
    }
    /**
     * Set subentityCode value
     * @param string $subentityCode
     * @return \AppturePay\DSV\StructType\AddressDetailsType
     */
    public function setSubentityCode(?string $subentityCode = null): self
    {
        // validation for constraint: string
        if (!is_null($subentityCode) && !is_string($subentityCode)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($subentityCode, true), gettype($subentityCode)), __LINE__);
        }
        $this->subentityCode = $subentityCode;
        
        return $this;
    }
    /**
     * Get postalCode value
     * @return string|null
     */
    public function getPostalCode(): ?string
    {
        return $this->postalCode;
    }
    /**
     * Set postalCode value
     * @param string $postalCode
     * @return \AppturePay\DSV\StructType\AddressDetailsType
     */
    public function setPostalCode(?string $postalCode = null): self
    {
        // validation for constraint: string
        if (!is_null($postalCode) && !is_string($postalCode)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($postalCode, true), gettype($postalCode)), __LINE__);
        }
        $this->postalCode = $postalCode;
        
        return $this;
    }
    /**
     * Get countryCode value
     * @return string|null
     */
    public function getCountryCode(): ?string
    {
        return $this->countryCode;
    }
    /**
     * Set countryCode value
     * @param string $countryCode
     * @return \AppturePay\DSV\StructType\AddressDetailsType
     */
    public function setCountryCode(?string $countryCode = null): self
    {
        // validation for constraint: string
        if (!is_null($countryCode) && !is_string($countryCode)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($countryCode, true), gettype($countryCode)), __LINE__);
        }
        $this->countryCode = $countryCode;
        
        return $this;
    }
}
