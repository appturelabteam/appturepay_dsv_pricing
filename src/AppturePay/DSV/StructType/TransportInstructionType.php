<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for transportInstructionType StructType
 * @subpackage Structs
 */
class TransportInstructionType extends AbstractStructBase
{
    /**
     * The carriageCondition
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $carriageCondition = null;
    /**
     * The description
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $description = null;
    /**
     * The type
     * @var string|null
     */
    protected ?string $type = null;
    /**
     * Constructor method for transportInstructionType
     * @uses TransportInstructionType::setCarriageCondition()
     * @uses TransportInstructionType::setDescription()
     * @uses TransportInstructionType::setType()
     * @param int $carriageCondition
     * @param string $description
     * @param string $type
     */
    public function __construct(?int $carriageCondition = null, ?string $description = null, ?string $type = null)
    {
        $this
            ->setCarriageCondition($carriageCondition)
            ->setDescription($description)
            ->setType($type);
    }
    /**
     * Get carriageCondition value
     * @return int|null
     */
    public function getCarriageCondition(): ?int
    {
        return $this->carriageCondition;
    }
    /**
     * Set carriageCondition value
     * @param int $carriageCondition
     * @return \AppturePay\DSV\StructType\TransportInstructionType
     */
    public function setCarriageCondition(?int $carriageCondition = null): self
    {
        // validation for constraint: int
        if (!is_null($carriageCondition) && !(is_int($carriageCondition) || ctype_digit($carriageCondition))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($carriageCondition, true), gettype($carriageCondition)), __LINE__);
        }
        $this->carriageCondition = $carriageCondition;
        
        return $this;
    }
    /**
     * Get description value
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }
    /**
     * Set description value
     * @param string $description
     * @return \AppturePay\DSV\StructType\TransportInstructionType
     */
    public function setDescription(?string $description = null): self
    {
        // validation for constraint: string
        if (!is_null($description) && !is_string($description)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($description, true), gettype($description)), __LINE__);
        }
        $this->description = $description;
        
        return $this;
    }
    /**
     * Get type value
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }
    /**
     * Set type value
     * @param string $type
     * @return \AppturePay\DSV\StructType\TransportInstructionType
     */
    public function setType(?string $type = null): self
    {
        // validation for constraint: string
        if (!is_null($type) && !is_string($type)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($type, true), gettype($type)), __LINE__);
        }
        $this->type = $type;
        
        return $this;
    }
}
