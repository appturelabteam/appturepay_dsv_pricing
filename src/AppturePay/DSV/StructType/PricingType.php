<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for pricingType StructType
 * @subpackage Structs
 */
class PricingType extends AbstractStructBase
{
    /**
     * The ediCustomerNumber
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $ediCustomerNumber = null;
    /**
     * The ediCustomerDepartment
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\EdiCustomerDepartmentTypeComplex|null
     */
    protected ?\AppturePay\DSV\StructType\EdiCustomerDepartmentTypeComplex $ediCustomerDepartment = null;
    /**
     * The ediParm1
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $ediParm1 = null;
    /**
     * The ediParm2
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $ediParm2 = null;
    /**
     * The ediParm3
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $ediParm3 = null;
    /**
     * The transmitter
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $transmitter = null;
    /**
     * The receiver
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $receiver = null;
    /**
     * The ediReference
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $ediReference = null;
    /**
     * The referenceIndication
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $referenceIndication = null;
    /**
     * The ediFunction1
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $ediFunction1 = null;
    /**
     * The ediCustomerSearchName
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $ediCustomerSearchName = null;
    /**
     * The dateTimeZone
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $dateTimeZone = null;
    /**
     * The file
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\FileType|null
     */
    protected ?\AppturePay\DSV\StructType\FileType $file = null;
    /**
     * The type
     * @var string|null
     */
    protected ?string $type = null;
    /**
     * Constructor method for pricingType
     * @uses PricingType::setEdiCustomerNumber()
     * @uses PricingType::setEdiCustomerDepartment()
     * @uses PricingType::setEdiParm1()
     * @uses PricingType::setEdiParm2()
     * @uses PricingType::setEdiParm3()
     * @uses PricingType::setTransmitter()
     * @uses PricingType::setReceiver()
     * @uses PricingType::setEdiReference()
     * @uses PricingType::setReferenceIndication()
     * @uses PricingType::setEdiFunction1()
     * @uses PricingType::setEdiCustomerSearchName()
     * @uses PricingType::setDateTimeZone()
     * @uses PricingType::setFile()
     * @uses PricingType::setType()
     * @param int $ediCustomerNumber
     * @param \AppturePay\DSV\StructType\EdiCustomerDepartmentTypeComplex $ediCustomerDepartment
     * @param int $ediParm1
     * @param string $ediParm2
     * @param string $ediParm3
     * @param string $transmitter
     * @param string $receiver
     * @param string $ediReference
     * @param string $referenceIndication
     * @param string $ediFunction1
     * @param string $ediCustomerSearchName
     * @param string $dateTimeZone
     * @param \AppturePay\DSV\StructType\FileType $file
     * @param string $type
     */
    public function __construct(?int $ediCustomerNumber = null, ?\AppturePay\DSV\StructType\EdiCustomerDepartmentTypeComplex $ediCustomerDepartment = null, ?int $ediParm1 = null, ?string $ediParm2 = null, ?string $ediParm3 = null, ?string $transmitter = null, ?string $receiver = null, ?string $ediReference = null, ?string $referenceIndication = null, ?string $ediFunction1 = null, ?string $ediCustomerSearchName = null, ?string $dateTimeZone = null, ?\AppturePay\DSV\StructType\FileType $file = null, ?string $type = null)
    {
        $this
            ->setEdiCustomerNumber($ediCustomerNumber)
            ->setEdiCustomerDepartment($ediCustomerDepartment)
            ->setEdiParm1($ediParm1)
            ->setEdiParm2($ediParm2)
            ->setEdiParm3($ediParm3)
            ->setTransmitter($transmitter)
            ->setReceiver($receiver)
            ->setEdiReference($ediReference)
            ->setReferenceIndication($referenceIndication)
            ->setEdiFunction1($ediFunction1)
            ->setEdiCustomerSearchName($ediCustomerSearchName)
            ->setDateTimeZone($dateTimeZone)
            ->setFile($file)
            ->setType($type);
    }
    /**
     * Get ediCustomerNumber value
     * @return int|null
     */
    public function getEdiCustomerNumber(): ?int
    {
        return $this->ediCustomerNumber;
    }
    /**
     * Set ediCustomerNumber value
     * @param int $ediCustomerNumber
     * @return \AppturePay\DSV\StructType\PricingType
     */
    public function setEdiCustomerNumber(?int $ediCustomerNumber = null): self
    {
        // validation for constraint: int
        if (!is_null($ediCustomerNumber) && !(is_int($ediCustomerNumber) || ctype_digit($ediCustomerNumber))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($ediCustomerNumber, true), gettype($ediCustomerNumber)), __LINE__);
        }
        $this->ediCustomerNumber = $ediCustomerNumber;
        
        return $this;
    }
    /**
     * Get ediCustomerDepartment value
     * @return \AppturePay\DSV\StructType\EdiCustomerDepartmentTypeComplex|null
     */
    public function getEdiCustomerDepartment(): ?\AppturePay\DSV\StructType\EdiCustomerDepartmentTypeComplex
    {
        return $this->ediCustomerDepartment;
    }
    /**
     * Set ediCustomerDepartment value
     * @param \AppturePay\DSV\StructType\EdiCustomerDepartmentTypeComplex $ediCustomerDepartment
     * @return \AppturePay\DSV\StructType\PricingType
     */
    public function setEdiCustomerDepartment(?\AppturePay\DSV\StructType\EdiCustomerDepartmentTypeComplex $ediCustomerDepartment = null): self
    {
        $this->ediCustomerDepartment = $ediCustomerDepartment;
        
        return $this;
    }
    /**
     * Get ediParm1 value
     * @return int|null
     */
    public function getEdiParm1(): ?int
    {
        return $this->ediParm1;
    }
    /**
     * Set ediParm1 value
     * @param int $ediParm1
     * @return \AppturePay\DSV\StructType\PricingType
     */
    public function setEdiParm1(?int $ediParm1 = null): self
    {
        // validation for constraint: int
        if (!is_null($ediParm1) && !(is_int($ediParm1) || ctype_digit($ediParm1))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($ediParm1, true), gettype($ediParm1)), __LINE__);
        }
        $this->ediParm1 = $ediParm1;
        
        return $this;
    }
    /**
     * Get ediParm2 value
     * @return string|null
     */
    public function getEdiParm2(): ?string
    {
        return $this->ediParm2;
    }
    /**
     * Set ediParm2 value
     * @param string $ediParm2
     * @return \AppturePay\DSV\StructType\PricingType
     */
    public function setEdiParm2(?string $ediParm2 = null): self
    {
        // validation for constraint: string
        if (!is_null($ediParm2) && !is_string($ediParm2)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($ediParm2, true), gettype($ediParm2)), __LINE__);
        }
        $this->ediParm2 = $ediParm2;
        
        return $this;
    }
    /**
     * Get ediParm3 value
     * @return string|null
     */
    public function getEdiParm3(): ?string
    {
        return $this->ediParm3;
    }
    /**
     * Set ediParm3 value
     * @param string $ediParm3
     * @return \AppturePay\DSV\StructType\PricingType
     */
    public function setEdiParm3(?string $ediParm3 = null): self
    {
        // validation for constraint: string
        if (!is_null($ediParm3) && !is_string($ediParm3)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($ediParm3, true), gettype($ediParm3)), __LINE__);
        }
        $this->ediParm3 = $ediParm3;
        
        return $this;
    }
    /**
     * Get transmitter value
     * @return string|null
     */
    public function getTransmitter(): ?string
    {
        return $this->transmitter;
    }
    /**
     * Set transmitter value
     * @param string $transmitter
     * @return \AppturePay\DSV\StructType\PricingType
     */
    public function setTransmitter(?string $transmitter = null): self
    {
        // validation for constraint: string
        if (!is_null($transmitter) && !is_string($transmitter)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($transmitter, true), gettype($transmitter)), __LINE__);
        }
        $this->transmitter = $transmitter;
        
        return $this;
    }
    /**
     * Get receiver value
     * @return string|null
     */
    public function getReceiver(): ?string
    {
        return $this->receiver;
    }
    /**
     * Set receiver value
     * @param string $receiver
     * @return \AppturePay\DSV\StructType\PricingType
     */
    public function setReceiver(?string $receiver = null): self
    {
        // validation for constraint: string
        if (!is_null($receiver) && !is_string($receiver)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($receiver, true), gettype($receiver)), __LINE__);
        }
        $this->receiver = $receiver;
        
        return $this;
    }
    /**
     * Get ediReference value
     * @return string|null
     */
    public function getEdiReference(): ?string
    {
        return $this->ediReference;
    }
    /**
     * Set ediReference value
     * @param string $ediReference
     * @return \AppturePay\DSV\StructType\PricingType
     */
    public function setEdiReference(?string $ediReference = null): self
    {
        // validation for constraint: string
        if (!is_null($ediReference) && !is_string($ediReference)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($ediReference, true), gettype($ediReference)), __LINE__);
        }
        $this->ediReference = $ediReference;
        
        return $this;
    }
    /**
     * Get referenceIndication value
     * @return string|null
     */
    public function getReferenceIndication(): ?string
    {
        return $this->referenceIndication;
    }
    /**
     * Set referenceIndication value
     * @uses \AppturePay\DSV\EnumType\ReferenceIndicationType::valueIsValid()
     * @uses \AppturePay\DSV\EnumType\ReferenceIndicationType::getValidValues()
     * @throws InvalidArgumentException
     * @param string $referenceIndication
     * @return \AppturePay\DSV\StructType\PricingType
     */
    public function setReferenceIndication(?string $referenceIndication = null): self
    {
        // validation for constraint: enumeration
        if (!\AppturePay\DSV\EnumType\ReferenceIndicationType::valueIsValid($referenceIndication)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \AppturePay\DSV\EnumType\ReferenceIndicationType', is_array($referenceIndication) ? implode(', ', $referenceIndication) : var_export($referenceIndication, true), implode(', ', \AppturePay\DSV\EnumType\ReferenceIndicationType::getValidValues())), __LINE__);
        }
        $this->referenceIndication = $referenceIndication;
        
        return $this;
    }
    /**
     * Get ediFunction1 value
     * @return string|null
     */
    public function getEdiFunction1(): ?string
    {
        return $this->ediFunction1;
    }
    /**
     * Set ediFunction1 value
     * @param string $ediFunction1
     * @return \AppturePay\DSV\StructType\PricingType
     */
    public function setEdiFunction1(?string $ediFunction1 = null): self
    {
        // validation for constraint: string
        if (!is_null($ediFunction1) && !is_string($ediFunction1)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($ediFunction1, true), gettype($ediFunction1)), __LINE__);
        }
        $this->ediFunction1 = $ediFunction1;
        
        return $this;
    }
    /**
     * Get ediCustomerSearchName value
     * @return string|null
     */
    public function getEdiCustomerSearchName(): ?string
    {
        return $this->ediCustomerSearchName;
    }
    /**
     * Set ediCustomerSearchName value
     * @param string $ediCustomerSearchName
     * @return \AppturePay\DSV\StructType\PricingType
     */
    public function setEdiCustomerSearchName(?string $ediCustomerSearchName = null): self
    {
        // validation for constraint: string
        if (!is_null($ediCustomerSearchName) && !is_string($ediCustomerSearchName)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($ediCustomerSearchName, true), gettype($ediCustomerSearchName)), __LINE__);
        }
        $this->ediCustomerSearchName = $ediCustomerSearchName;
        
        return $this;
    }
    /**
     * Get dateTimeZone value
     * @return string|null
     */
    public function getDateTimeZone(): ?string
    {
        return $this->dateTimeZone;
    }
    /**
     * Set dateTimeZone value
     * @param string $dateTimeZone
     * @return \AppturePay\DSV\StructType\PricingType
     */
    public function setDateTimeZone(?string $dateTimeZone = null): self
    {
        // validation for constraint: string
        if (!is_null($dateTimeZone) && !is_string($dateTimeZone)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($dateTimeZone, true), gettype($dateTimeZone)), __LINE__);
        }
        $this->dateTimeZone = $dateTimeZone;
        
        return $this;
    }
    /**
     * Get file value
     * @return \AppturePay\DSV\StructType\FileType|null
     */
    public function getFile(): ?\AppturePay\DSV\StructType\FileType
    {
        return $this->file;
    }
    /**
     * Set file value
     * @param \AppturePay\DSV\StructType\FileType $file
     * @return \AppturePay\DSV\StructType\PricingType
     */
    public function setFile(?\AppturePay\DSV\StructType\FileType $file = null): self
    {
        $this->file = $file;
        
        return $this;
    }
    /**
     * Get type value
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }
    /**
     * Set type value
     * @param string $type
     * @return \AppturePay\DSV\StructType\PricingType
     */
    public function setType(?string $type = null): self
    {
        // validation for constraint: string
        if (!is_null($type) && !is_string($type)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($type, true), gettype($type)), __LINE__);
        }
        $this->type = $type;
        
        return $this;
    }
}
