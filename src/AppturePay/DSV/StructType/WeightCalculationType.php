<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for weightCalculationType StructType
 * @subpackage Structs
 */
class WeightCalculationType extends AbstractStructBase
{
    /**
     * The conversionBase
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $conversionBase = null;
    /**
     * The conversionFactor
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $conversionFactor = null;
    /**
     * The chargeableWeight
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $chargeableWeight = null;
    /**
     * Constructor method for weightCalculationType
     * @uses WeightCalculationType::setConversionBase()
     * @uses WeightCalculationType::setConversionFactor()
     * @uses WeightCalculationType::setChargeableWeight()
     * @param string $conversionBase
     * @param float $conversionFactor
     * @param float $chargeableWeight
     */
    public function __construct(?string $conversionBase = null, ?float $conversionFactor = null, ?float $chargeableWeight = null)
    {
        $this
            ->setConversionBase($conversionBase)
            ->setConversionFactor($conversionFactor)
            ->setChargeableWeight($chargeableWeight);
    }
    /**
     * Get conversionBase value
     * @return string|null
     */
    public function getConversionBase(): ?string
    {
        return $this->conversionBase;
    }
    /**
     * Set conversionBase value
     * @param string $conversionBase
     * @return \AppturePay\DSV\StructType\WeightCalculationType
     */
    public function setConversionBase(?string $conversionBase = null): self
    {
        // validation for constraint: string
        if (!is_null($conversionBase) && !is_string($conversionBase)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($conversionBase, true), gettype($conversionBase)), __LINE__);
        }
        $this->conversionBase = $conversionBase;
        
        return $this;
    }
    /**
     * Get conversionFactor value
     * @return float|null
     */
    public function getConversionFactor(): ?float
    {
        return $this->conversionFactor;
    }
    /**
     * Set conversionFactor value
     * @param float $conversionFactor
     * @return \AppturePay\DSV\StructType\WeightCalculationType
     */
    public function setConversionFactor(?float $conversionFactor = null): self
    {
        // validation for constraint: float
        if (!is_null($conversionFactor) && !(is_float($conversionFactor) || is_numeric($conversionFactor))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($conversionFactor, true), gettype($conversionFactor)), __LINE__);
        }
        $this->conversionFactor = $conversionFactor;
        
        return $this;
    }
    /**
     * Get chargeableWeight value
     * @return float|null
     */
    public function getChargeableWeight(): ?float
    {
        return $this->chargeableWeight;
    }
    /**
     * Set chargeableWeight value
     * @param float $chargeableWeight
     * @return \AppturePay\DSV\StructType\WeightCalculationType
     */
    public function setChargeableWeight(?float $chargeableWeight = null): self
    {
        // validation for constraint: float
        if (!is_null($chargeableWeight) && !(is_float($chargeableWeight) || is_numeric($chargeableWeight))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($chargeableWeight, true), gettype($chargeableWeight)), __LINE__);
        }
        $this->chargeableWeight = $chargeableWeight;
        
        return $this;
    }
}
