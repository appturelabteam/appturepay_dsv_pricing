<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for pricingRequestResponse StructType
 * @subpackage Structs
 */
class PricingRequestResponse extends AbstractStructBase
{
    /**
     * The pricingRequestResult
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\EdiParametersType|null
     */
    protected ?\AppturePay\DSV\StructType\EdiParametersType $pricingRequestResult = null;
    /**
     * Constructor method for pricingRequestResponse
     * @uses PricingRequestResponse::setPricingRequestResult()
     * @param \AppturePay\DSV\StructType\EdiParametersType $pricingRequestResult
     */
    public function __construct(?\AppturePay\DSV\StructType\EdiParametersType $pricingRequestResult = null)
    {
        $this
            ->setPricingRequestResult($pricingRequestResult);
    }
    /**
     * Get pricingRequestResult value
     * @return \AppturePay\DSV\StructType\EdiParametersType|null
     */
    public function getPricingRequestResult(): ?\AppturePay\DSV\StructType\EdiParametersType
    {
        return $this->pricingRequestResult;
    }
    /**
     * Set pricingRequestResult value
     * @param \AppturePay\DSV\StructType\EdiParametersType $pricingRequestResult
     * @return \AppturePay\DSV\StructType\PricingRequestResponse
     */
    public function setPricingRequestResult(?\AppturePay\DSV\StructType\EdiParametersType $pricingRequestResult = null): self
    {
        $this->pricingRequestResult = $pricingRequestResult;
        
        return $this;
    }
}
