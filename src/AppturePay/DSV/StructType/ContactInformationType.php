<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for contactInformationType StructType
 * @subpackage Structs
 */
class ContactInformationType extends AbstractStructBase
{
    /**
     * The languageCode
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $languageCode = null;
    /**
     * The e_mailAddress
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $e_mailAddress = null;
    /**
     * The internetAddress
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $internetAddress = null;
    /**
     * The contactPerson
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $contactPerson = null;
    /**
     * The telephoneNumber
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $telephoneNumber = null;
    /**
     * The telefaxNumber
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $telefaxNumber = null;
    /**
     * Constructor method for contactInformationType
     * @uses ContactInformationType::setLanguageCode()
     * @uses ContactInformationType::setE_mailAddress()
     * @uses ContactInformationType::setInternetAddress()
     * @uses ContactInformationType::setContactPerson()
     * @uses ContactInformationType::setTelephoneNumber()
     * @uses ContactInformationType::setTelefaxNumber()
     * @param int $languageCode
     * @param string $e_mailAddress
     * @param string $internetAddress
     * @param string $contactPerson
     * @param string $telephoneNumber
     * @param string $telefaxNumber
     */
    public function __construct(?int $languageCode = null, ?string $e_mailAddress = null, ?string $internetAddress = null, ?string $contactPerson = null, ?string $telephoneNumber = null, ?string $telefaxNumber = null)
    {
        $this
            ->setLanguageCode($languageCode)
            ->setE_mailAddress($e_mailAddress)
            ->setInternetAddress($internetAddress)
            ->setContactPerson($contactPerson)
            ->setTelephoneNumber($telephoneNumber)
            ->setTelefaxNumber($telefaxNumber);
    }
    /**
     * Get languageCode value
     * @return int|null
     */
    public function getLanguageCode(): ?int
    {
        return $this->languageCode;
    }
    /**
     * Set languageCode value
     * @param int $languageCode
     * @return \AppturePay\DSV\StructType\ContactInformationType
     */
    public function setLanguageCode(?int $languageCode = null): self
    {
        // validation for constraint: int
        if (!is_null($languageCode) && !(is_int($languageCode) || ctype_digit($languageCode))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($languageCode, true), gettype($languageCode)), __LINE__);
        }
        $this->languageCode = $languageCode;
        
        return $this;
    }
    /**
     * Get e_mailAddress value
     * @return string|null
     */
    public function getE_mailAddress(): ?string
    {
        return $this->{'e-mailAddress'};
    }
    /**
     * Set e_mailAddress value
     * @param string $e_mailAddress
     * @return \AppturePay\DSV\StructType\ContactInformationType
     */
    public function setE_mailAddress(?string $e_mailAddress = null): self
    {
        // validation for constraint: string
        if (!is_null($e_mailAddress) && !is_string($e_mailAddress)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($e_mailAddress, true), gettype($e_mailAddress)), __LINE__);
        }
        $this->e_mailAddress = $this->{'e-mailAddress'} = $e_mailAddress;
        
        return $this;
    }
    /**
     * Get internetAddress value
     * @return string|null
     */
    public function getInternetAddress(): ?string
    {
        return $this->internetAddress;
    }
    /**
     * Set internetAddress value
     * @param string $internetAddress
     * @return \AppturePay\DSV\StructType\ContactInformationType
     */
    public function setInternetAddress(?string $internetAddress = null): self
    {
        // validation for constraint: string
        if (!is_null($internetAddress) && !is_string($internetAddress)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($internetAddress, true), gettype($internetAddress)), __LINE__);
        }
        $this->internetAddress = $internetAddress;
        
        return $this;
    }
    /**
     * Get contactPerson value
     * @return string|null
     */
    public function getContactPerson(): ?string
    {
        return $this->contactPerson;
    }
    /**
     * Set contactPerson value
     * @param string $contactPerson
     * @return \AppturePay\DSV\StructType\ContactInformationType
     */
    public function setContactPerson(?string $contactPerson = null): self
    {
        // validation for constraint: string
        if (!is_null($contactPerson) && !is_string($contactPerson)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($contactPerson, true), gettype($contactPerson)), __LINE__);
        }
        $this->contactPerson = $contactPerson;
        
        return $this;
    }
    /**
     * Get telephoneNumber value
     * @return string|null
     */
    public function getTelephoneNumber(): ?string
    {
        return $this->telephoneNumber;
    }
    /**
     * Set telephoneNumber value
     * @param string $telephoneNumber
     * @return \AppturePay\DSV\StructType\ContactInformationType
     */
    public function setTelephoneNumber(?string $telephoneNumber = null): self
    {
        // validation for constraint: string
        if (!is_null($telephoneNumber) && !is_string($telephoneNumber)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($telephoneNumber, true), gettype($telephoneNumber)), __LINE__);
        }
        $this->telephoneNumber = $telephoneNumber;
        
        return $this;
    }
    /**
     * Get telefaxNumber value
     * @return string|null
     */
    public function getTelefaxNumber(): ?string
    {
        return $this->telefaxNumber;
    }
    /**
     * Set telefaxNumber value
     * @param string $telefaxNumber
     * @return \AppturePay\DSV\StructType\ContactInformationType
     */
    public function setTelefaxNumber(?string $telefaxNumber = null): self
    {
        // validation for constraint: string
        if (!is_null($telefaxNumber) && !is_string($telefaxNumber)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($telefaxNumber, true), gettype($telefaxNumber)), __LINE__);
        }
        $this->telefaxNumber = $telefaxNumber;
        
        return $this;
    }
}
