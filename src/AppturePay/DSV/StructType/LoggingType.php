<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for loggingType StructType
 * @subpackage Structs
 */
class LoggingType extends AbstractStructBase
{
    /**
     * The tableName
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $tableName = null;
    /**
     * The errorCode
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $errorCode = null;
    /**
     * The indicationNotOk
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $indicationNotOk = null;
    /**
     * The errorDescription
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $errorDescription = null;
    /**
     * The type
     * @var string|null
     */
    protected ?string $type = null;
    /**
     * Constructor method for loggingType
     * @uses LoggingType::setTableName()
     * @uses LoggingType::setErrorCode()
     * @uses LoggingType::setIndicationNotOk()
     * @uses LoggingType::setErrorDescription()
     * @uses LoggingType::setType()
     * @param string $tableName
     * @param int $errorCode
     * @param string $indicationNotOk
     * @param string $errorDescription
     * @param string $type
     */
    public function __construct(?string $tableName = null, ?int $errorCode = null, ?string $indicationNotOk = null, ?string $errorDescription = null, ?string $type = null)
    {
        $this
            ->setTableName($tableName)
            ->setErrorCode($errorCode)
            ->setIndicationNotOk($indicationNotOk)
            ->setErrorDescription($errorDescription)
            ->setType($type);
    }
    /**
     * Get tableName value
     * @return string|null
     */
    public function getTableName(): ?string
    {
        return $this->tableName;
    }
    /**
     * Set tableName value
     * @param string $tableName
     * @return \AppturePay\DSV\StructType\LoggingType
     */
    public function setTableName(?string $tableName = null): self
    {
        // validation for constraint: string
        if (!is_null($tableName) && !is_string($tableName)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($tableName, true), gettype($tableName)), __LINE__);
        }
        $this->tableName = $tableName;
        
        return $this;
    }
    /**
     * Get errorCode value
     * @return int|null
     */
    public function getErrorCode(): ?int
    {
        return $this->errorCode;
    }
    /**
     * Set errorCode value
     * @param int $errorCode
     * @return \AppturePay\DSV\StructType\LoggingType
     */
    public function setErrorCode(?int $errorCode = null): self
    {
        // validation for constraint: int
        if (!is_null($errorCode) && !(is_int($errorCode) || ctype_digit($errorCode))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($errorCode, true), gettype($errorCode)), __LINE__);
        }
        $this->errorCode = $errorCode;
        
        return $this;
    }
    /**
     * Get indicationNotOk value
     * @return string|null
     */
    public function getIndicationNotOk(): ?string
    {
        return $this->indicationNotOk;
    }
    /**
     * Set indicationNotOk value
     * @param string $indicationNotOk
     * @return \AppturePay\DSV\StructType\LoggingType
     */
    public function setIndicationNotOk(?string $indicationNotOk = null): self
    {
        // validation for constraint: string
        if (!is_null($indicationNotOk) && !is_string($indicationNotOk)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($indicationNotOk, true), gettype($indicationNotOk)), __LINE__);
        }
        $this->indicationNotOk = $indicationNotOk;
        
        return $this;
    }
    /**
     * Get errorDescription value
     * @return string|null
     */
    public function getErrorDescription(): ?string
    {
        return $this->errorDescription;
    }
    /**
     * Set errorDescription value
     * @param string $errorDescription
     * @return \AppturePay\DSV\StructType\LoggingType
     */
    public function setErrorDescription(?string $errorDescription = null): self
    {
        // validation for constraint: string
        if (!is_null($errorDescription) && !is_string($errorDescription)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($errorDescription, true), gettype($errorDescription)), __LINE__);
        }
        $this->errorDescription = $errorDescription;
        
        return $this;
    }
    /**
     * Get type value
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }
    /**
     * Set type value
     * @param string $type
     * @return \AppturePay\DSV\StructType\LoggingType
     */
    public function setType(?string $type = null): self
    {
        // validation for constraint: string
        if (!is_null($type) && !is_string($type)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($type, true), gettype($type)), __LINE__);
        }
        $this->type = $type;
        
        return $this;
    }
}
