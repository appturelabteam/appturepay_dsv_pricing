<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for goodsLineType StructType
 * @subpackage Structs
 */
class GoodsLineType extends AbstractStructBase
{
    /**
     * The articleCode
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $articleCode = null;
    /**
     * The primaryReference
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $primaryReference = null;
    /**
     * The quantity
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $quantity = null;
    /**
     * The packageCode
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $packageCode = null;
    /**
     * The commodityCode
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $commodityCode = null;
    /**
     * The grossWeight
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $grossWeight = null;
    /**
     * The netWeight
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $netWeight = null;
    /**
     * The volumeCode
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $volumeCode = null;
    /**
     * The dimension
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\DimensionType|null
     */
    protected ?\AppturePay\DSV\StructType\DimensionType $dimension = null;
    /**
     * The volume
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $volume = null;
    /**
     * The cubicMeters
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $cubicMeters = null;
    /**
     * The loadingMeters
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $loadingMeters = null;
    /**
     * The goodsValueCurrency
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $goodsValueCurrency = null;
    /**
     * The goodsValue
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $goodsValue = null;
    /**
     * The loadIndex
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $loadIndex = null;
    /**
     * The quantityOfShippingUnits
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $quantityOfShippingUnits = null;
    /**
     * The packageCodeOfShippingUnits
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $packageCodeOfShippingUnits = null;
    /**
     * The dangerousGoods
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\DangerousGoodsType|null
     */
    protected ?\AppturePay\DSV\StructType\DangerousGoodsType $dangerousGoods = null;
    /**
     * The type
     * @var string|null
     */
    protected ?string $type = null;
    /**
     * Constructor method for goodsLineType
     * @uses GoodsLineType::setArticleCode()
     * @uses GoodsLineType::setPrimaryReference()
     * @uses GoodsLineType::setQuantity()
     * @uses GoodsLineType::setPackageCode()
     * @uses GoodsLineType::setCommodityCode()
     * @uses GoodsLineType::setGrossWeight()
     * @uses GoodsLineType::setNetWeight()
     * @uses GoodsLineType::setVolumeCode()
     * @uses GoodsLineType::setDimension()
     * @uses GoodsLineType::setVolume()
     * @uses GoodsLineType::setCubicMeters()
     * @uses GoodsLineType::setLoadingMeters()
     * @uses GoodsLineType::setGoodsValueCurrency()
     * @uses GoodsLineType::setGoodsValue()
     * @uses GoodsLineType::setLoadIndex()
     * @uses GoodsLineType::setQuantityOfShippingUnits()
     * @uses GoodsLineType::setPackageCodeOfShippingUnits()
     * @uses GoodsLineType::setDangerousGoods()
     * @uses GoodsLineType::setType()
     * @param string $articleCode
     * @param string $primaryReference
     * @param float $quantity
     * @param string $packageCode
     * @param string $commodityCode
     * @param float $grossWeight
     * @param float $netWeight
     * @param string $volumeCode
     * @param \AppturePay\DSV\StructType\DimensionType $dimension
     * @param float $volume
     * @param float $cubicMeters
     * @param float $loadingMeters
     * @param string $goodsValueCurrency
     * @param float $goodsValue
     * @param float $loadIndex
     * @param int $quantityOfShippingUnits
     * @param string $packageCodeOfShippingUnits
     * @param \AppturePay\DSV\StructType\DangerousGoodsType $dangerousGoods
     * @param string $type
     */
    public function __construct(?string $articleCode = null, ?string $primaryReference = null, ?float $quantity = null, ?string $packageCode = null, ?string $commodityCode = null, ?float $grossWeight = null, ?float $netWeight = null, ?string $volumeCode = null, ?\AppturePay\DSV\StructType\DimensionType $dimension = null, ?float $volume = null, ?float $cubicMeters = null, ?float $loadingMeters = null, ?string $goodsValueCurrency = null, ?float $goodsValue = null, ?float $loadIndex = null, ?int $quantityOfShippingUnits = null, ?string $packageCodeOfShippingUnits = null, ?\AppturePay\DSV\StructType\DangerousGoodsType $dangerousGoods = null, ?string $type = null)
    {
        $this
            ->setArticleCode($articleCode)
            ->setPrimaryReference($primaryReference)
            ->setQuantity($quantity)
            ->setPackageCode($packageCode)
            ->setCommodityCode($commodityCode)
            ->setGrossWeight($grossWeight)
            ->setNetWeight($netWeight)
            ->setVolumeCode($volumeCode)
            ->setDimension($dimension)
            ->setVolume($volume)
            ->setCubicMeters($cubicMeters)
            ->setLoadingMeters($loadingMeters)
            ->setGoodsValueCurrency($goodsValueCurrency)
            ->setGoodsValue($goodsValue)
            ->setLoadIndex($loadIndex)
            ->setQuantityOfShippingUnits($quantityOfShippingUnits)
            ->setPackageCodeOfShippingUnits($packageCodeOfShippingUnits)
            ->setDangerousGoods($dangerousGoods)
            ->setType($type);
    }
    /**
     * Get articleCode value
     * @return string|null
     */
    public function getArticleCode(): ?string
    {
        return $this->articleCode;
    }
    /**
     * Set articleCode value
     * @param string $articleCode
     * @return \AppturePay\DSV\StructType\GoodsLineType
     */
    public function setArticleCode(?string $articleCode = null): self
    {
        // validation for constraint: string
        if (!is_null($articleCode) && !is_string($articleCode)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($articleCode, true), gettype($articleCode)), __LINE__);
        }
        $this->articleCode = $articleCode;
        
        return $this;
    }
    /**
     * Get primaryReference value
     * @return string|null
     */
    public function getPrimaryReference(): ?string
    {
        return $this->primaryReference;
    }
    /**
     * Set primaryReference value
     * @param string $primaryReference
     * @return \AppturePay\DSV\StructType\GoodsLineType
     */
    public function setPrimaryReference(?string $primaryReference = null): self
    {
        // validation for constraint: string
        if (!is_null($primaryReference) && !is_string($primaryReference)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($primaryReference, true), gettype($primaryReference)), __LINE__);
        }
        $this->primaryReference = $primaryReference;
        
        return $this;
    }
    /**
     * Get quantity value
     * @return float|null
     */
    public function getQuantity(): ?float
    {
        return $this->quantity;
    }
    /**
     * Set quantity value
     * @param float $quantity
     * @return \AppturePay\DSV\StructType\GoodsLineType
     */
    public function setQuantity(?float $quantity = null): self
    {
        // validation for constraint: float
        if (!is_null($quantity) && !(is_float($quantity) || is_numeric($quantity))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($quantity, true), gettype($quantity)), __LINE__);
        }
        $this->quantity = $quantity;
        
        return $this;
    }
    /**
     * Get packageCode value
     * @return string|null
     */
    public function getPackageCode(): ?string
    {
        return $this->packageCode;
    }
    /**
     * Set packageCode value
     * @param string $packageCode
     * @return \AppturePay\DSV\StructType\GoodsLineType
     */
    public function setPackageCode(?string $packageCode = null): self
    {
        // validation for constraint: string
        if (!is_null($packageCode) && !is_string($packageCode)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($packageCode, true), gettype($packageCode)), __LINE__);
        }
        $this->packageCode = $packageCode;
        
        return $this;
    }
    /**
     * Get commodityCode value
     * @return string|null
     */
    public function getCommodityCode(): ?string
    {
        return $this->commodityCode;
    }
    /**
     * Set commodityCode value
     * @param string $commodityCode
     * @return \AppturePay\DSV\StructType\GoodsLineType
     */
    public function setCommodityCode(?string $commodityCode = null): self
    {
        // validation for constraint: string
        if (!is_null($commodityCode) && !is_string($commodityCode)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($commodityCode, true), gettype($commodityCode)), __LINE__);
        }
        $this->commodityCode = $commodityCode;
        
        return $this;
    }
    /**
     * Get grossWeight value
     * @return float|null
     */
    public function getGrossWeight(): ?float
    {
        return $this->grossWeight;
    }
    /**
     * Set grossWeight value
     * @param float $grossWeight
     * @return \AppturePay\DSV\StructType\GoodsLineType
     */
    public function setGrossWeight(?float $grossWeight = null): self
    {
        // validation for constraint: float
        if (!is_null($grossWeight) && !(is_float($grossWeight) || is_numeric($grossWeight))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($grossWeight, true), gettype($grossWeight)), __LINE__);
        }
        $this->grossWeight = $grossWeight;
        
        return $this;
    }
    /**
     * Get netWeight value
     * @return float|null
     */
    public function getNetWeight(): ?float
    {
        return $this->netWeight;
    }
    /**
     * Set netWeight value
     * @param float $netWeight
     * @return \AppturePay\DSV\StructType\GoodsLineType
     */
    public function setNetWeight(?float $netWeight = null): self
    {
        // validation for constraint: float
        if (!is_null($netWeight) && !(is_float($netWeight) || is_numeric($netWeight))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($netWeight, true), gettype($netWeight)), __LINE__);
        }
        $this->netWeight = $netWeight;
        
        return $this;
    }
    /**
     * Get volumeCode value
     * @return string|null
     */
    public function getVolumeCode(): ?string
    {
        return $this->volumeCode;
    }
    /**
     * Set volumeCode value
     * @uses \AppturePay\DSV\EnumType\VolumeCodeType::valueIsValid()
     * @uses \AppturePay\DSV\EnumType\VolumeCodeType::getValidValues()
     * @throws InvalidArgumentException
     * @param string $volumeCode
     * @return \AppturePay\DSV\StructType\GoodsLineType
     */
    public function setVolumeCode(?string $volumeCode = null): self
    {
        // validation for constraint: enumeration
        if (!\AppturePay\DSV\EnumType\VolumeCodeType::valueIsValid($volumeCode)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \AppturePay\DSV\EnumType\VolumeCodeType', is_array($volumeCode) ? implode(', ', $volumeCode) : var_export($volumeCode, true), implode(', ', \AppturePay\DSV\EnumType\VolumeCodeType::getValidValues())), __LINE__);
        }
        $this->volumeCode = $volumeCode;
        
        return $this;
    }
    /**
     * Get dimension value
     * @return \AppturePay\DSV\StructType\DimensionType|null
     */
    public function getDimension(): ?\AppturePay\DSV\StructType\DimensionType
    {
        return $this->dimension;
    }
    /**
     * Set dimension value
     * @param \AppturePay\DSV\StructType\DimensionType $dimension
     * @return \AppturePay\DSV\StructType\GoodsLineType
     */
    public function setDimension(?\AppturePay\DSV\StructType\DimensionType $dimension = null): self
    {
        $this->dimension = $dimension;
        
        return $this;
    }
    /**
     * Get volume value
     * @return float|null
     */
    public function getVolume(): ?float
    {
        return $this->volume;
    }
    /**
     * Set volume value
     * @param float $volume
     * @return \AppturePay\DSV\StructType\GoodsLineType
     */
    public function setVolume(?float $volume = null): self
    {
        // validation for constraint: float
        if (!is_null($volume) && !(is_float($volume) || is_numeric($volume))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($volume, true), gettype($volume)), __LINE__);
        }
        $this->volume = $volume;
        
        return $this;
    }
    /**
     * Get cubicMeters value
     * @return float|null
     */
    public function getCubicMeters(): ?float
    {
        return $this->cubicMeters;
    }
    /**
     * Set cubicMeters value
     * @param float $cubicMeters
     * @return \AppturePay\DSV\StructType\GoodsLineType
     */
    public function setCubicMeters(?float $cubicMeters = null): self
    {
        // validation for constraint: float
        if (!is_null($cubicMeters) && !(is_float($cubicMeters) || is_numeric($cubicMeters))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($cubicMeters, true), gettype($cubicMeters)), __LINE__);
        }
        $this->cubicMeters = $cubicMeters;
        
        return $this;
    }
    /**
     * Get loadingMeters value
     * @return float|null
     */
    public function getLoadingMeters(): ?float
    {
        return $this->loadingMeters;
    }
    /**
     * Set loadingMeters value
     * @param float $loadingMeters
     * @return \AppturePay\DSV\StructType\GoodsLineType
     */
    public function setLoadingMeters(?float $loadingMeters = null): self
    {
        // validation for constraint: float
        if (!is_null($loadingMeters) && !(is_float($loadingMeters) || is_numeric($loadingMeters))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($loadingMeters, true), gettype($loadingMeters)), __LINE__);
        }
        $this->loadingMeters = $loadingMeters;
        
        return $this;
    }
    /**
     * Get goodsValueCurrency value
     * @return string|null
     */
    public function getGoodsValueCurrency(): ?string
    {
        return $this->goodsValueCurrency;
    }
    /**
     * Set goodsValueCurrency value
     * @param string $goodsValueCurrency
     * @return \AppturePay\DSV\StructType\GoodsLineType
     */
    public function setGoodsValueCurrency(?string $goodsValueCurrency = null): self
    {
        // validation for constraint: string
        if (!is_null($goodsValueCurrency) && !is_string($goodsValueCurrency)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($goodsValueCurrency, true), gettype($goodsValueCurrency)), __LINE__);
        }
        $this->goodsValueCurrency = $goodsValueCurrency;
        
        return $this;
    }
    /**
     * Get goodsValue value
     * @return float|null
     */
    public function getGoodsValue(): ?float
    {
        return $this->goodsValue;
    }
    /**
     * Set goodsValue value
     * @param float $goodsValue
     * @return \AppturePay\DSV\StructType\GoodsLineType
     */
    public function setGoodsValue(?float $goodsValue = null): self
    {
        // validation for constraint: float
        if (!is_null($goodsValue) && !(is_float($goodsValue) || is_numeric($goodsValue))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($goodsValue, true), gettype($goodsValue)), __LINE__);
        }
        $this->goodsValue = $goodsValue;
        
        return $this;
    }
    /**
     * Get loadIndex value
     * @return float|null
     */
    public function getLoadIndex(): ?float
    {
        return $this->loadIndex;
    }
    /**
     * Set loadIndex value
     * @param float $loadIndex
     * @return \AppturePay\DSV\StructType\GoodsLineType
     */
    public function setLoadIndex(?float $loadIndex = null): self
    {
        // validation for constraint: float
        if (!is_null($loadIndex) && !(is_float($loadIndex) || is_numeric($loadIndex))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($loadIndex, true), gettype($loadIndex)), __LINE__);
        }
        $this->loadIndex = $loadIndex;
        
        return $this;
    }
    /**
     * Get quantityOfShippingUnits value
     * @return int|null
     */
    public function getQuantityOfShippingUnits(): ?int
    {
        return $this->quantityOfShippingUnits;
    }
    /**
     * Set quantityOfShippingUnits value
     * @param int $quantityOfShippingUnits
     * @return \AppturePay\DSV\StructType\GoodsLineType
     */
    public function setQuantityOfShippingUnits(?int $quantityOfShippingUnits = null): self
    {
        // validation for constraint: int
        if (!is_null($quantityOfShippingUnits) && !(is_int($quantityOfShippingUnits) || ctype_digit($quantityOfShippingUnits))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($quantityOfShippingUnits, true), gettype($quantityOfShippingUnits)), __LINE__);
        }
        $this->quantityOfShippingUnits = $quantityOfShippingUnits;
        
        return $this;
    }
    /**
     * Get packageCodeOfShippingUnits value
     * @return string|null
     */
    public function getPackageCodeOfShippingUnits(): ?string
    {
        return $this->packageCodeOfShippingUnits;
    }
    /**
     * Set packageCodeOfShippingUnits value
     * @param string $packageCodeOfShippingUnits
     * @return \AppturePay\DSV\StructType\GoodsLineType
     */
    public function setPackageCodeOfShippingUnits(?string $packageCodeOfShippingUnits = null): self
    {
        // validation for constraint: string
        if (!is_null($packageCodeOfShippingUnits) && !is_string($packageCodeOfShippingUnits)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($packageCodeOfShippingUnits, true), gettype($packageCodeOfShippingUnits)), __LINE__);
        }
        $this->packageCodeOfShippingUnits = $packageCodeOfShippingUnits;
        
        return $this;
    }
    /**
     * Get dangerousGoods value
     * @return \AppturePay\DSV\StructType\DangerousGoodsType|null
     */
    public function getDangerousGoods(): ?\AppturePay\DSV\StructType\DangerousGoodsType
    {
        return $this->dangerousGoods;
    }
    /**
     * Set dangerousGoods value
     * @param \AppturePay\DSV\StructType\DangerousGoodsType $dangerousGoods
     * @return \AppturePay\DSV\StructType\GoodsLineType
     */
    public function setDangerousGoods(?\AppturePay\DSV\StructType\DangerousGoodsType $dangerousGoods = null): self
    {
        $this->dangerousGoods = $dangerousGoods;
        
        return $this;
    }
    /**
     * Get type value
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }
    /**
     * Set type value
     * @param string $type
     * @return \AppturePay\DSV\StructType\GoodsLineType
     */
    public function setType(?string $type = null): self
    {
        // validation for constraint: string
        if (!is_null($type) && !is_string($type)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($type, true), gettype($type)), __LINE__);
        }
        $this->type = $type;
        
        return $this;
    }
}
