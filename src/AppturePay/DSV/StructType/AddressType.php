<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for addressType StructType
 * @subpackage Structs
 */
class AddressType extends AbstractStructBase
{
    /**
     * The addressType
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $addressType = null;
    /**
     * The searchName
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $searchName = null;
    /**
     * The relationNumber
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $relationNumber = null;
    /**
     * The searchString
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $searchString = null;
    /**
     * The additional
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\AdditionalType|null
     */
    protected ?\AppturePay\DSV\StructType\AdditionalType $additional = null;
    /**
     * The addressDetails
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\AddressDetailsType|null
     */
    protected ?\AppturePay\DSV\StructType\AddressDetailsType $addressDetails = null;
    /**
     * The contactInformation
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\ContactInformationType|null
     */
    protected ?\AppturePay\DSV\StructType\ContactInformationType $contactInformation = null;
    /**
     * The type
     * @var string|null
     */
    protected ?string $type = null;
    /**
     * Constructor method for addressType
     * @uses AddressType::setAddressType()
     * @uses AddressType::setSearchName()
     * @uses AddressType::setRelationNumber()
     * @uses AddressType::setSearchString()
     * @uses AddressType::setAdditional()
     * @uses AddressType::setAddressDetails()
     * @uses AddressType::setContactInformation()
     * @uses AddressType::setType()
     * @param int $addressType
     * @param string $searchName
     * @param int $relationNumber
     * @param string $searchString
     * @param \AppturePay\DSV\StructType\AdditionalType $additional
     * @param \AppturePay\DSV\StructType\AddressDetailsType $addressDetails
     * @param \AppturePay\DSV\StructType\ContactInformationType $contactInformation
     * @param string $type
     */
    public function __construct(?int $addressType = null, ?string $searchName = null, ?int $relationNumber = null, ?string $searchString = null, ?\AppturePay\DSV\StructType\AdditionalType $additional = null, ?\AppturePay\DSV\StructType\AddressDetailsType $addressDetails = null, ?\AppturePay\DSV\StructType\ContactInformationType $contactInformation = null, ?string $type = null)
    {
        $this
            ->setAddressType($addressType)
            ->setSearchName($searchName)
            ->setRelationNumber($relationNumber)
            ->setSearchString($searchString)
            ->setAdditional($additional)
            ->setAddressDetails($addressDetails)
            ->setContactInformation($contactInformation)
            ->setType($type);
    }
    /**
     * Get addressType value
     * @return int|null
     */
    public function getAddressType(): ?int
    {
        return $this->addressType;
    }
    /**
     * Set addressType value
     * @param int $addressType
     * @return \AppturePay\DSV\StructType\AddressType
     */
    public function setAddressType(?int $addressType = null): self
    {
        // validation for constraint: int
        if (!is_null($addressType) && !(is_int($addressType) || ctype_digit($addressType))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($addressType, true), gettype($addressType)), __LINE__);
        }
        $this->addressType = $addressType;
        
        return $this;
    }
    /**
     * Get searchName value
     * @return string|null
     */
    public function getSearchName(): ?string
    {
        return $this->searchName;
    }
    /**
     * Set searchName value
     * @param string $searchName
     * @return \AppturePay\DSV\StructType\AddressType
     */
    public function setSearchName(?string $searchName = null): self
    {
        // validation for constraint: string
        if (!is_null($searchName) && !is_string($searchName)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($searchName, true), gettype($searchName)), __LINE__);
        }
        $this->searchName = $searchName;
        
        return $this;
    }
    /**
     * Get relationNumber value
     * @return int|null
     */
    public function getRelationNumber(): ?int
    {
        return $this->relationNumber;
    }
    /**
     * Set relationNumber value
     * @param int $relationNumber
     * @return \AppturePay\DSV\StructType\AddressType
     */
    public function setRelationNumber(?int $relationNumber = null): self
    {
        // validation for constraint: int
        if (!is_null($relationNumber) && !(is_int($relationNumber) || ctype_digit($relationNumber))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($relationNumber, true), gettype($relationNumber)), __LINE__);
        }
        $this->relationNumber = $relationNumber;
        
        return $this;
    }
    /**
     * Get searchString value
     * @return string|null
     */
    public function getSearchString(): ?string
    {
        return $this->searchString;
    }
    /**
     * Set searchString value
     * @param string $searchString
     * @return \AppturePay\DSV\StructType\AddressType
     */
    public function setSearchString(?string $searchString = null): self
    {
        // validation for constraint: string
        if (!is_null($searchString) && !is_string($searchString)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($searchString, true), gettype($searchString)), __LINE__);
        }
        $this->searchString = $searchString;
        
        return $this;
    }
    /**
     * Get additional value
     * @return \AppturePay\DSV\StructType\AdditionalType|null
     */
    public function getAdditional(): ?\AppturePay\DSV\StructType\AdditionalType
    {
        return $this->additional;
    }
    /**
     * Set additional value
     * @param \AppturePay\DSV\StructType\AdditionalType $additional
     * @return \AppturePay\DSV\StructType\AddressType
     */
    public function setAdditional(?\AppturePay\DSV\StructType\AdditionalType $additional = null): self
    {
        $this->additional = $additional;
        
        return $this;
    }
    /**
     * Get addressDetails value
     * @return \AppturePay\DSV\StructType\AddressDetailsType|null
     */
    public function getAddressDetails(): ?\AppturePay\DSV\StructType\AddressDetailsType
    {
        return $this->addressDetails;
    }
    /**
     * Set addressDetails value
     * @param \AppturePay\DSV\StructType\AddressDetailsType $addressDetails
     * @return \AppturePay\DSV\StructType\AddressType
     */
    public function setAddressDetails(?\AppturePay\DSV\StructType\AddressDetailsType $addressDetails = null): self
    {
        $this->addressDetails = $addressDetails;
        
        return $this;
    }
    /**
     * Get contactInformation value
     * @return \AppturePay\DSV\StructType\ContactInformationType|null
     */
    public function getContactInformation(): ?\AppturePay\DSV\StructType\ContactInformationType
    {
        return $this->contactInformation;
    }
    /**
     * Set contactInformation value
     * @param \AppturePay\DSV\StructType\ContactInformationType $contactInformation
     * @return \AppturePay\DSV\StructType\AddressType
     */
    public function setContactInformation(?\AppturePay\DSV\StructType\ContactInformationType $contactInformation = null): self
    {
        $this->contactInformation = $contactInformation;
        
        return $this;
    }
    /**
     * Get type value
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }
    /**
     * Set type value
     * @param string $type
     * @return \AppturePay\DSV\StructType\AddressType
     */
    public function setType(?string $type = null): self
    {
        // validation for constraint: string
        if (!is_null($type) && !is_string($type)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($type, true), gettype($type)), __LINE__);
        }
        $this->type = $type;
        
        return $this;
    }
}
