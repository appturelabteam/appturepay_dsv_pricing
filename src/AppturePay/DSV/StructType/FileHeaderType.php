<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for fileHeaderType StructType
 * @subpackage Structs
 */
class FileHeaderType extends AbstractStructBase
{
    /**
     * The extraAmount
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\ExtraAmountType[]
     */
    protected ?array $extraAmount = null;
    /**
     * The invoiceLine
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\InvoiceLineType[]
     */
    protected ?array $invoiceLine = null;
    /**
     * The type
     * @var string|null
     */
    protected ?string $type = null;
    /**
     * Constructor method for fileHeaderType
     * @uses FileHeaderType::setExtraAmount()
     * @uses FileHeaderType::setInvoiceLine()
     * @uses FileHeaderType::setType()
     * @param \AppturePay\DSV\StructType\ExtraAmountType[] $extraAmount
     * @param \AppturePay\DSV\StructType\InvoiceLineType[] $invoiceLine
     * @param string $type
     */
    public function __construct(?array $extraAmount = null, ?array $invoiceLine = null, ?string $type = null)
    {
        $this
            ->setExtraAmount($extraAmount)
            ->setInvoiceLine($invoiceLine)
            ->setType($type);
    }
    /**
     * Get extraAmount value
     * @return \AppturePay\DSV\StructType\ExtraAmountType[]
     */
    public function getExtraAmount(): ?array
    {
        return $this->extraAmount;
    }
    /**
     * This method is responsible for validating the values passed to the setExtraAmount method
     * This method is willingly generated in order to preserve the one-line inline validation within the setExtraAmount method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateExtraAmountForArrayConstraintsFromSetExtraAmount(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $fileHeaderTypeExtraAmountItem) {
            // validation for constraint: itemType
            if (!$fileHeaderTypeExtraAmountItem instanceof \AppturePay\DSV\StructType\ExtraAmountType) {
                $invalidValues[] = is_object($fileHeaderTypeExtraAmountItem) ? get_class($fileHeaderTypeExtraAmountItem) : sprintf('%s(%s)', gettype($fileHeaderTypeExtraAmountItem), var_export($fileHeaderTypeExtraAmountItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The extraAmount property can only contain items of type \AppturePay\DSV\StructType\ExtraAmountType, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set extraAmount value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\ExtraAmountType[] $extraAmount
     * @return \AppturePay\DSV\StructType\FileHeaderType
     */
    public function setExtraAmount(?array $extraAmount = null): self
    {
        // validation for constraint: array
        if ('' !== ($extraAmountArrayErrorMessage = self::validateExtraAmountForArrayConstraintsFromSetExtraAmount($extraAmount))) {
            throw new InvalidArgumentException($extraAmountArrayErrorMessage, __LINE__);
        }
        $this->extraAmount = $extraAmount;
        
        return $this;
    }
    /**
     * Add item to extraAmount value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\ExtraAmountType $item
     * @return \AppturePay\DSV\StructType\FileHeaderType
     */
    public function addToExtraAmount(\AppturePay\DSV\StructType\ExtraAmountType $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \AppturePay\DSV\StructType\ExtraAmountType) {
            throw new InvalidArgumentException(sprintf('The extraAmount property can only contain items of type \AppturePay\DSV\StructType\ExtraAmountType, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->extraAmount[] = $item;
        
        return $this;
    }
    /**
     * Get invoiceLine value
     * @return \AppturePay\DSV\StructType\InvoiceLineType[]
     */
    public function getInvoiceLine(): ?array
    {
        return $this->invoiceLine;
    }
    /**
     * This method is responsible for validating the values passed to the setInvoiceLine method
     * This method is willingly generated in order to preserve the one-line inline validation within the setInvoiceLine method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateInvoiceLineForArrayConstraintsFromSetInvoiceLine(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $fileHeaderTypeInvoiceLineItem) {
            // validation for constraint: itemType
            if (!$fileHeaderTypeInvoiceLineItem instanceof \AppturePay\DSV\StructType\InvoiceLineType) {
                $invalidValues[] = is_object($fileHeaderTypeInvoiceLineItem) ? get_class($fileHeaderTypeInvoiceLineItem) : sprintf('%s(%s)', gettype($fileHeaderTypeInvoiceLineItem), var_export($fileHeaderTypeInvoiceLineItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The invoiceLine property can only contain items of type \AppturePay\DSV\StructType\InvoiceLineType, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set invoiceLine value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\InvoiceLineType[] $invoiceLine
     * @return \AppturePay\DSV\StructType\FileHeaderType
     */
    public function setInvoiceLine(?array $invoiceLine = null): self
    {
        // validation for constraint: array
        if ('' !== ($invoiceLineArrayErrorMessage = self::validateInvoiceLineForArrayConstraintsFromSetInvoiceLine($invoiceLine))) {
            throw new InvalidArgumentException($invoiceLineArrayErrorMessage, __LINE__);
        }
        $this->invoiceLine = $invoiceLine;
        
        return $this;
    }
    /**
     * Add item to invoiceLine value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\InvoiceLineType $item
     * @return \AppturePay\DSV\StructType\FileHeaderType
     */
    public function addToInvoiceLine(\AppturePay\DSV\StructType\InvoiceLineType $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \AppturePay\DSV\StructType\InvoiceLineType) {
            throw new InvalidArgumentException(sprintf('The invoiceLine property can only contain items of type \AppturePay\DSV\StructType\InvoiceLineType, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->invoiceLine[] = $item;
        
        return $this;
    }
    /**
     * Get type value
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }
    /**
     * Set type value
     * @param string $type
     * @return \AppturePay\DSV\StructType\FileHeaderType
     */
    public function setType(?string $type = null): self
    {
        // validation for constraint: string
        if (!is_null($type) && !is_string($type)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($type, true), gettype($type)), __LINE__);
        }
        $this->type = $type;
        
        return $this;
    }
}
