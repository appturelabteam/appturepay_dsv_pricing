<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for tariffSpecificationType StructType
 * @subpackage Structs
 */
class TariffSpecificationType extends AbstractStructBase
{
    /**
     * The tariffNumber
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $tariffNumber = null;
    /**
     * The validFrom
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $validFrom = null;
    /**
     * The validThru
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $validThru = null;
    /**
     * The relationNumber
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $relationNumber = null;
    /**
     * The type
     * @var string|null
     */
    protected ?string $type = null;
    /**
     * Constructor method for tariffSpecificationType
     * @uses TariffSpecificationType::setTariffNumber()
     * @uses TariffSpecificationType::setValidFrom()
     * @uses TariffSpecificationType::setValidThru()
     * @uses TariffSpecificationType::setRelationNumber()
     * @uses TariffSpecificationType::setType()
     * @param int $tariffNumber
     * @param string $validFrom
     * @param string $validThru
     * @param int $relationNumber
     * @param string $type
     */
    public function __construct(?int $tariffNumber = null, ?string $validFrom = null, ?string $validThru = null, ?int $relationNumber = null, ?string $type = null)
    {
        $this
            ->setTariffNumber($tariffNumber)
            ->setValidFrom($validFrom)
            ->setValidThru($validThru)
            ->setRelationNumber($relationNumber)
            ->setType($type);
    }
    /**
     * Get tariffNumber value
     * @return int|null
     */
    public function getTariffNumber(): ?int
    {
        return $this->tariffNumber;
    }
    /**
     * Set tariffNumber value
     * @param int $tariffNumber
     * @return \AppturePay\DSV\StructType\TariffSpecificationType
     */
    public function setTariffNumber(?int $tariffNumber = null): self
    {
        // validation for constraint: int
        if (!is_null($tariffNumber) && !(is_int($tariffNumber) || ctype_digit($tariffNumber))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($tariffNumber, true), gettype($tariffNumber)), __LINE__);
        }
        $this->tariffNumber = $tariffNumber;
        
        return $this;
    }
    /**
     * Get validFrom value
     * @return string|null
     */
    public function getValidFrom(): ?string
    {
        return $this->validFrom;
    }
    /**
     * Set validFrom value
     * @param string $validFrom
     * @return \AppturePay\DSV\StructType\TariffSpecificationType
     */
    public function setValidFrom(?string $validFrom = null): self
    {
        // validation for constraint: string
        if (!is_null($validFrom) && !is_string($validFrom)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($validFrom, true), gettype($validFrom)), __LINE__);
        }
        $this->validFrom = $validFrom;
        
        return $this;
    }
    /**
     * Get validThru value
     * @return string|null
     */
    public function getValidThru(): ?string
    {
        return $this->validThru;
    }
    /**
     * Set validThru value
     * @param string $validThru
     * @return \AppturePay\DSV\StructType\TariffSpecificationType
     */
    public function setValidThru(?string $validThru = null): self
    {
        // validation for constraint: string
        if (!is_null($validThru) && !is_string($validThru)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($validThru, true), gettype($validThru)), __LINE__);
        }
        $this->validThru = $validThru;
        
        return $this;
    }
    /**
     * Get relationNumber value
     * @return int|null
     */
    public function getRelationNumber(): ?int
    {
        return $this->relationNumber;
    }
    /**
     * Set relationNumber value
     * @param int $relationNumber
     * @return \AppturePay\DSV\StructType\TariffSpecificationType
     */
    public function setRelationNumber(?int $relationNumber = null): self
    {
        // validation for constraint: int
        if (!is_null($relationNumber) && !(is_int($relationNumber) || ctype_digit($relationNumber))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($relationNumber, true), gettype($relationNumber)), __LINE__);
        }
        $this->relationNumber = $relationNumber;
        
        return $this;
    }
    /**
     * Get type value
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }
    /**
     * Set type value
     * @param string $type
     * @return \AppturePay\DSV\StructType\TariffSpecificationType
     */
    public function setType(?string $type = null): self
    {
        // validation for constraint: string
        if (!is_null($type) && !is_string($type)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($type, true), gettype($type)), __LINE__);
        }
        $this->type = $type;
        
        return $this;
    }
}
