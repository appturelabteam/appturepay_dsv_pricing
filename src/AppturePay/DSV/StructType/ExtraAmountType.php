<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for extraAmountType StructType
 * @subpackage Structs
 */
class ExtraAmountType extends AbstractStructBase
{
    /**
     * The amountType
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $amountType = null;
    /**
     * The currencyCode
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $currencyCode = null;
    /**
     * The amount
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $amount = null;
    /**
     * The type
     * @var string|null
     */
    protected ?string $type = null;
    /**
     * Constructor method for extraAmountType
     * @uses ExtraAmountType::setAmountType()
     * @uses ExtraAmountType::setCurrencyCode()
     * @uses ExtraAmountType::setAmount()
     * @uses ExtraAmountType::setType()
     * @param int $amountType
     * @param string $currencyCode
     * @param float $amount
     * @param string $type
     */
    public function __construct(?int $amountType = null, ?string $currencyCode = null, ?float $amount = null, ?string $type = null)
    {
        $this
            ->setAmountType($amountType)
            ->setCurrencyCode($currencyCode)
            ->setAmount($amount)
            ->setType($type);
    }
    /**
     * Get amountType value
     * @return int|null
     */
    public function getAmountType(): ?int
    {
        return $this->amountType;
    }
    /**
     * Set amountType value
     * @param int $amountType
     * @return \AppturePay\DSV\StructType\ExtraAmountType
     */
    public function setAmountType(?int $amountType = null): self
    {
        // validation for constraint: int
        if (!is_null($amountType) && !(is_int($amountType) || ctype_digit($amountType))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($amountType, true), gettype($amountType)), __LINE__);
        }
        $this->amountType = $amountType;
        
        return $this;
    }
    /**
     * Get currencyCode value
     * @return string|null
     */
    public function getCurrencyCode(): ?string
    {
        return $this->currencyCode;
    }
    /**
     * Set currencyCode value
     * @param string $currencyCode
     * @return \AppturePay\DSV\StructType\ExtraAmountType
     */
    public function setCurrencyCode(?string $currencyCode = null): self
    {
        // validation for constraint: string
        if (!is_null($currencyCode) && !is_string($currencyCode)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($currencyCode, true), gettype($currencyCode)), __LINE__);
        }
        $this->currencyCode = $currencyCode;
        
        return $this;
    }
    /**
     * Get amount value
     * @return float|null
     */
    public function getAmount(): ?float
    {
        return $this->amount;
    }
    /**
     * Set amount value
     * @param float $amount
     * @return \AppturePay\DSV\StructType\ExtraAmountType
     */
    public function setAmount(?float $amount = null): self
    {
        // validation for constraint: float
        if (!is_null($amount) && !(is_float($amount) || is_numeric($amount))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($amount, true), gettype($amount)), __LINE__);
        }
        $this->amount = $amount;
        
        return $this;
    }
    /**
     * Get type value
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }
    /**
     * Set type value
     * @param string $type
     * @return \AppturePay\DSV\StructType\ExtraAmountType
     */
    public function setType(?string $type = null): self
    {
        // validation for constraint: string
        if (!is_null($type) && !is_string($type)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($type, true), gettype($type)), __LINE__);
        }
        $this->type = $type;
        
        return $this;
    }
}
