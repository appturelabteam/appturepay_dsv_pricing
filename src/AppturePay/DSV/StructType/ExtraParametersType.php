<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for extraParametersType StructType
 * @subpackage Structs
 */
class ExtraParametersType extends AbstractStructBase
{
    /**
     * The parameterYn11
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $parameterYn11 = null;
    /**
     * The parameterYn12
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $parameterYn12 = null;
    /**
     * The parameterYn13
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $parameterYn13 = null;
    /**
     * The parameterYn14
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $parameterYn14 = null;
    /**
     * The parameterYn15
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $parameterYn15 = null;
    /**
     * The parameterYn16
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $parameterYn16 = null;
    /**
     * The parameterYn17
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $parameterYn17 = null;
    /**
     * The parameterYn18
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $parameterYn18 = null;
    /**
     * The parameterYn19
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $parameterYn19 = null;
    /**
     * The parameterYn20
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $parameterYn20 = null;
    /**
     * Constructor method for extraParametersType
     * @uses ExtraParametersType::setParameterYn11()
     * @uses ExtraParametersType::setParameterYn12()
     * @uses ExtraParametersType::setParameterYn13()
     * @uses ExtraParametersType::setParameterYn14()
     * @uses ExtraParametersType::setParameterYn15()
     * @uses ExtraParametersType::setParameterYn16()
     * @uses ExtraParametersType::setParameterYn17()
     * @uses ExtraParametersType::setParameterYn18()
     * @uses ExtraParametersType::setParameterYn19()
     * @uses ExtraParametersType::setParameterYn20()
     * @param string $parameterYn11
     * @param string $parameterYn12
     * @param string $parameterYn13
     * @param string $parameterYn14
     * @param string $parameterYn15
     * @param string $parameterYn16
     * @param string $parameterYn17
     * @param string $parameterYn18
     * @param string $parameterYn19
     * @param string $parameterYn20
     */
    public function __construct(?string $parameterYn11 = null, ?string $parameterYn12 = null, ?string $parameterYn13 = null, ?string $parameterYn14 = null, ?string $parameterYn15 = null, ?string $parameterYn16 = null, ?string $parameterYn17 = null, ?string $parameterYn18 = null, ?string $parameterYn19 = null, ?string $parameterYn20 = null)
    {
        $this
            ->setParameterYn11($parameterYn11)
            ->setParameterYn12($parameterYn12)
            ->setParameterYn13($parameterYn13)
            ->setParameterYn14($parameterYn14)
            ->setParameterYn15($parameterYn15)
            ->setParameterYn16($parameterYn16)
            ->setParameterYn17($parameterYn17)
            ->setParameterYn18($parameterYn18)
            ->setParameterYn19($parameterYn19)
            ->setParameterYn20($parameterYn20);
    }
    /**
     * Get parameterYn11 value
     * @return string|null
     */
    public function getParameterYn11(): ?string
    {
        return $this->parameterYn11;
    }
    /**
     * Set parameterYn11 value
     * @param string $parameterYn11
     * @return \AppturePay\DSV\StructType\ExtraParametersType
     */
    public function setParameterYn11(?string $parameterYn11 = null): self
    {
        // validation for constraint: string
        if (!is_null($parameterYn11) && !is_string($parameterYn11)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($parameterYn11, true), gettype($parameterYn11)), __LINE__);
        }
        $this->parameterYn11 = $parameterYn11;
        
        return $this;
    }
    /**
     * Get parameterYn12 value
     * @return string|null
     */
    public function getParameterYn12(): ?string
    {
        return $this->parameterYn12;
    }
    /**
     * Set parameterYn12 value
     * @param string $parameterYn12
     * @return \AppturePay\DSV\StructType\ExtraParametersType
     */
    public function setParameterYn12(?string $parameterYn12 = null): self
    {
        // validation for constraint: string
        if (!is_null($parameterYn12) && !is_string($parameterYn12)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($parameterYn12, true), gettype($parameterYn12)), __LINE__);
        }
        $this->parameterYn12 = $parameterYn12;
        
        return $this;
    }
    /**
     * Get parameterYn13 value
     * @return string|null
     */
    public function getParameterYn13(): ?string
    {
        return $this->parameterYn13;
    }
    /**
     * Set parameterYn13 value
     * @param string $parameterYn13
     * @return \AppturePay\DSV\StructType\ExtraParametersType
     */
    public function setParameterYn13(?string $parameterYn13 = null): self
    {
        // validation for constraint: string
        if (!is_null($parameterYn13) && !is_string($parameterYn13)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($parameterYn13, true), gettype($parameterYn13)), __LINE__);
        }
        $this->parameterYn13 = $parameterYn13;
        
        return $this;
    }
    /**
     * Get parameterYn14 value
     * @return string|null
     */
    public function getParameterYn14(): ?string
    {
        return $this->parameterYn14;
    }
    /**
     * Set parameterYn14 value
     * @param string $parameterYn14
     * @return \AppturePay\DSV\StructType\ExtraParametersType
     */
    public function setParameterYn14(?string $parameterYn14 = null): self
    {
        // validation for constraint: string
        if (!is_null($parameterYn14) && !is_string($parameterYn14)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($parameterYn14, true), gettype($parameterYn14)), __LINE__);
        }
        $this->parameterYn14 = $parameterYn14;
        
        return $this;
    }
    /**
     * Get parameterYn15 value
     * @return string|null
     */
    public function getParameterYn15(): ?string
    {
        return $this->parameterYn15;
    }
    /**
     * Set parameterYn15 value
     * @param string $parameterYn15
     * @return \AppturePay\DSV\StructType\ExtraParametersType
     */
    public function setParameterYn15(?string $parameterYn15 = null): self
    {
        // validation for constraint: string
        if (!is_null($parameterYn15) && !is_string($parameterYn15)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($parameterYn15, true), gettype($parameterYn15)), __LINE__);
        }
        $this->parameterYn15 = $parameterYn15;
        
        return $this;
    }
    /**
     * Get parameterYn16 value
     * @return string|null
     */
    public function getParameterYn16(): ?string
    {
        return $this->parameterYn16;
    }
    /**
     * Set parameterYn16 value
     * @param string $parameterYn16
     * @return \AppturePay\DSV\StructType\ExtraParametersType
     */
    public function setParameterYn16(?string $parameterYn16 = null): self
    {
        // validation for constraint: string
        if (!is_null($parameterYn16) && !is_string($parameterYn16)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($parameterYn16, true), gettype($parameterYn16)), __LINE__);
        }
        $this->parameterYn16 = $parameterYn16;
        
        return $this;
    }
    /**
     * Get parameterYn17 value
     * @return string|null
     */
    public function getParameterYn17(): ?string
    {
        return $this->parameterYn17;
    }
    /**
     * Set parameterYn17 value
     * @param string $parameterYn17
     * @return \AppturePay\DSV\StructType\ExtraParametersType
     */
    public function setParameterYn17(?string $parameterYn17 = null): self
    {
        // validation for constraint: string
        if (!is_null($parameterYn17) && !is_string($parameterYn17)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($parameterYn17, true), gettype($parameterYn17)), __LINE__);
        }
        $this->parameterYn17 = $parameterYn17;
        
        return $this;
    }
    /**
     * Get parameterYn18 value
     * @return string|null
     */
    public function getParameterYn18(): ?string
    {
        return $this->parameterYn18;
    }
    /**
     * Set parameterYn18 value
     * @param string $parameterYn18
     * @return \AppturePay\DSV\StructType\ExtraParametersType
     */
    public function setParameterYn18(?string $parameterYn18 = null): self
    {
        // validation for constraint: string
        if (!is_null($parameterYn18) && !is_string($parameterYn18)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($parameterYn18, true), gettype($parameterYn18)), __LINE__);
        }
        $this->parameterYn18 = $parameterYn18;
        
        return $this;
    }
    /**
     * Get parameterYn19 value
     * @return string|null
     */
    public function getParameterYn19(): ?string
    {
        return $this->parameterYn19;
    }
    /**
     * Set parameterYn19 value
     * @param string $parameterYn19
     * @return \AppturePay\DSV\StructType\ExtraParametersType
     */
    public function setParameterYn19(?string $parameterYn19 = null): self
    {
        // validation for constraint: string
        if (!is_null($parameterYn19) && !is_string($parameterYn19)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($parameterYn19, true), gettype($parameterYn19)), __LINE__);
        }
        $this->parameterYn19 = $parameterYn19;
        
        return $this;
    }
    /**
     * Get parameterYn20 value
     * @return string|null
     */
    public function getParameterYn20(): ?string
    {
        return $this->parameterYn20;
    }
    /**
     * Set parameterYn20 value
     * @param string $parameterYn20
     * @return \AppturePay\DSV\StructType\ExtraParametersType
     */
    public function setParameterYn20(?string $parameterYn20 = null): self
    {
        // validation for constraint: string
        if (!is_null($parameterYn20) && !is_string($parameterYn20)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($parameterYn20, true), gettype($parameterYn20)), __LINE__);
        }
        $this->parameterYn20 = $parameterYn20;
        
        return $this;
    }
}
