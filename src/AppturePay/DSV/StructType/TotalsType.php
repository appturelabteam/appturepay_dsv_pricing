<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for totalsType StructType
 * @subpackage Structs
 */
class TotalsType extends AbstractStructBase
{
    /**
     * The quantity
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $quantity = null;
    /**
     * The packageCode
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $packageCode = null;
    /**
     * The commodityCode
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $commodityCode = null;
    /**
     * The commodityDescription
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $commodityDescription = null;
    /**
     * The grossWeight
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $grossWeight = null;
    /**
     * The netWeight
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $netWeight = null;
    /**
     * The volumeCode
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $volumeCode = null;
    /**
     * The volume
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $volume = null;
    /**
     * The cubicMeters
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $cubicMeters = null;
    /**
     * The loadingMeters
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $loadingMeters = null;
    /**
     * The goodsValueCurrency
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $goodsValueCurrency = null;
    /**
     * The goodsValue
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $goodsValue = null;
    /**
     * The loadIndex
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $loadIndex = null;
    /**
     * Constructor method for totalsType
     * @uses TotalsType::setQuantity()
     * @uses TotalsType::setPackageCode()
     * @uses TotalsType::setCommodityCode()
     * @uses TotalsType::setCommodityDescription()
     * @uses TotalsType::setGrossWeight()
     * @uses TotalsType::setNetWeight()
     * @uses TotalsType::setVolumeCode()
     * @uses TotalsType::setVolume()
     * @uses TotalsType::setCubicMeters()
     * @uses TotalsType::setLoadingMeters()
     * @uses TotalsType::setGoodsValueCurrency()
     * @uses TotalsType::setGoodsValue()
     * @uses TotalsType::setLoadIndex()
     * @param float $quantity
     * @param string $packageCode
     * @param string $commodityCode
     * @param string $commodityDescription
     * @param float $grossWeight
     * @param float $netWeight
     * @param string $volumeCode
     * @param float $volume
     * @param float $cubicMeters
     * @param float $loadingMeters
     * @param string $goodsValueCurrency
     * @param float $goodsValue
     * @param float $loadIndex
     */
    public function __construct(?float $quantity = null, ?string $packageCode = null, ?string $commodityCode = null, ?string $commodityDescription = null, ?float $grossWeight = null, ?float $netWeight = null, ?string $volumeCode = null, ?float $volume = null, ?float $cubicMeters = null, ?float $loadingMeters = null, ?string $goodsValueCurrency = null, ?float $goodsValue = null, ?float $loadIndex = null)
    {
        $this
            ->setQuantity($quantity)
            ->setPackageCode($packageCode)
            ->setCommodityCode($commodityCode)
            ->setCommodityDescription($commodityDescription)
            ->setGrossWeight($grossWeight)
            ->setNetWeight($netWeight)
            ->setVolumeCode($volumeCode)
            ->setVolume($volume)
            ->setCubicMeters($cubicMeters)
            ->setLoadingMeters($loadingMeters)
            ->setGoodsValueCurrency($goodsValueCurrency)
            ->setGoodsValue($goodsValue)
            ->setLoadIndex($loadIndex);
    }
    /**
     * Get quantity value
     * @return float|null
     */
    public function getQuantity(): ?float
    {
        return $this->quantity;
    }
    /**
     * Set quantity value
     * @param float $quantity
     * @return \AppturePay\DSV\StructType\TotalsType
     */
    public function setQuantity(?float $quantity = null): self
    {
        // validation for constraint: float
        if (!is_null($quantity) && !(is_float($quantity) || is_numeric($quantity))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($quantity, true), gettype($quantity)), __LINE__);
        }
        $this->quantity = $quantity;
        
        return $this;
    }
    /**
     * Get packageCode value
     * @return string|null
     */
    public function getPackageCode(): ?string
    {
        return $this->packageCode;
    }
    /**
     * Set packageCode value
     * @param string $packageCode
     * @return \AppturePay\DSV\StructType\TotalsType
     */
    public function setPackageCode(?string $packageCode = null): self
    {
        // validation for constraint: string
        if (!is_null($packageCode) && !is_string($packageCode)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($packageCode, true), gettype($packageCode)), __LINE__);
        }
        $this->packageCode = $packageCode;
        
        return $this;
    }
    /**
     * Get commodityCode value
     * @return string|null
     */
    public function getCommodityCode(): ?string
    {
        return $this->commodityCode;
    }
    /**
     * Set commodityCode value
     * @param string $commodityCode
     * @return \AppturePay\DSV\StructType\TotalsType
     */
    public function setCommodityCode(?string $commodityCode = null): self
    {
        // validation for constraint: string
        if (!is_null($commodityCode) && !is_string($commodityCode)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($commodityCode, true), gettype($commodityCode)), __LINE__);
        }
        $this->commodityCode = $commodityCode;
        
        return $this;
    }
    /**
     * Get commodityDescription value
     * @return string|null
     */
    public function getCommodityDescription(): ?string
    {
        return $this->commodityDescription;
    }
    /**
     * Set commodityDescription value
     * @param string $commodityDescription
     * @return \AppturePay\DSV\StructType\TotalsType
     */
    public function setCommodityDescription(?string $commodityDescription = null): self
    {
        // validation for constraint: string
        if (!is_null($commodityDescription) && !is_string($commodityDescription)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($commodityDescription, true), gettype($commodityDescription)), __LINE__);
        }
        $this->commodityDescription = $commodityDescription;
        
        return $this;
    }
    /**
     * Get grossWeight value
     * @return float|null
     */
    public function getGrossWeight(): ?float
    {
        return $this->grossWeight;
    }
    /**
     * Set grossWeight value
     * @param float $grossWeight
     * @return \AppturePay\DSV\StructType\TotalsType
     */
    public function setGrossWeight(?float $grossWeight = null): self
    {
        // validation for constraint: float
        if (!is_null($grossWeight) && !(is_float($grossWeight) || is_numeric($grossWeight))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($grossWeight, true), gettype($grossWeight)), __LINE__);
        }
        $this->grossWeight = $grossWeight;
        
        return $this;
    }
    /**
     * Get netWeight value
     * @return float|null
     */
    public function getNetWeight(): ?float
    {
        return $this->netWeight;
    }
    /**
     * Set netWeight value
     * @param float $netWeight
     * @return \AppturePay\DSV\StructType\TotalsType
     */
    public function setNetWeight(?float $netWeight = null): self
    {
        // validation for constraint: float
        if (!is_null($netWeight) && !(is_float($netWeight) || is_numeric($netWeight))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($netWeight, true), gettype($netWeight)), __LINE__);
        }
        $this->netWeight = $netWeight;
        
        return $this;
    }
    /**
     * Get volumeCode value
     * @return string|null
     */
    public function getVolumeCode(): ?string
    {
        return $this->volumeCode;
    }
    /**
     * Set volumeCode value
     * @uses \AppturePay\DSV\EnumType\VolumeCodeType::valueIsValid()
     * @uses \AppturePay\DSV\EnumType\VolumeCodeType::getValidValues()
     * @throws InvalidArgumentException
     * @param string $volumeCode
     * @return \AppturePay\DSV\StructType\TotalsType
     */
    public function setVolumeCode(?string $volumeCode = null): self
    {
        // validation for constraint: enumeration
        if (!\AppturePay\DSV\EnumType\VolumeCodeType::valueIsValid($volumeCode)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \AppturePay\DSV\EnumType\VolumeCodeType', is_array($volumeCode) ? implode(', ', $volumeCode) : var_export($volumeCode, true), implode(', ', \AppturePay\DSV\EnumType\VolumeCodeType::getValidValues())), __LINE__);
        }
        $this->volumeCode = $volumeCode;
        
        return $this;
    }
    /**
     * Get volume value
     * @return float|null
     */
    public function getVolume(): ?float
    {
        return $this->volume;
    }
    /**
     * Set volume value
     * @param float $volume
     * @return \AppturePay\DSV\StructType\TotalsType
     */
    public function setVolume(?float $volume = null): self
    {
        // validation for constraint: float
        if (!is_null($volume) && !(is_float($volume) || is_numeric($volume))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($volume, true), gettype($volume)), __LINE__);
        }
        $this->volume = $volume;
        
        return $this;
    }
    /**
     * Get cubicMeters value
     * @return float|null
     */
    public function getCubicMeters(): ?float
    {
        return $this->cubicMeters;
    }
    /**
     * Set cubicMeters value
     * @param float $cubicMeters
     * @return \AppturePay\DSV\StructType\TotalsType
     */
    public function setCubicMeters(?float $cubicMeters = null): self
    {
        // validation for constraint: float
        if (!is_null($cubicMeters) && !(is_float($cubicMeters) || is_numeric($cubicMeters))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($cubicMeters, true), gettype($cubicMeters)), __LINE__);
        }
        $this->cubicMeters = $cubicMeters;
        
        return $this;
    }
    /**
     * Get loadingMeters value
     * @return float|null
     */
    public function getLoadingMeters(): ?float
    {
        return $this->loadingMeters;
    }
    /**
     * Set loadingMeters value
     * @param float $loadingMeters
     * @return \AppturePay\DSV\StructType\TotalsType
     */
    public function setLoadingMeters(?float $loadingMeters = null): self
    {
        // validation for constraint: float
        if (!is_null($loadingMeters) && !(is_float($loadingMeters) || is_numeric($loadingMeters))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($loadingMeters, true), gettype($loadingMeters)), __LINE__);
        }
        $this->loadingMeters = $loadingMeters;
        
        return $this;
    }
    /**
     * Get goodsValueCurrency value
     * @return string|null
     */
    public function getGoodsValueCurrency(): ?string
    {
        return $this->goodsValueCurrency;
    }
    /**
     * Set goodsValueCurrency value
     * @param string $goodsValueCurrency
     * @return \AppturePay\DSV\StructType\TotalsType
     */
    public function setGoodsValueCurrency(?string $goodsValueCurrency = null): self
    {
        // validation for constraint: string
        if (!is_null($goodsValueCurrency) && !is_string($goodsValueCurrency)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($goodsValueCurrency, true), gettype($goodsValueCurrency)), __LINE__);
        }
        $this->goodsValueCurrency = $goodsValueCurrency;
        
        return $this;
    }
    /**
     * Get goodsValue value
     * @return float|null
     */
    public function getGoodsValue(): ?float
    {
        return $this->goodsValue;
    }
    /**
     * Set goodsValue value
     * @param float $goodsValue
     * @return \AppturePay\DSV\StructType\TotalsType
     */
    public function setGoodsValue(?float $goodsValue = null): self
    {
        // validation for constraint: float
        if (!is_null($goodsValue) && !(is_float($goodsValue) || is_numeric($goodsValue))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($goodsValue, true), gettype($goodsValue)), __LINE__);
        }
        $this->goodsValue = $goodsValue;
        
        return $this;
    }
    /**
     * Get loadIndex value
     * @return float|null
     */
    public function getLoadIndex(): ?float
    {
        return $this->loadIndex;
    }
    /**
     * Set loadIndex value
     * @param float $loadIndex
     * @return \AppturePay\DSV\StructType\TotalsType
     */
    public function setLoadIndex(?float $loadIndex = null): self
    {
        // validation for constraint: float
        if (!is_null($loadIndex) && !(is_float($loadIndex) || is_numeric($loadIndex))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($loadIndex, true), gettype($loadIndex)), __LINE__);
        }
        $this->loadIndex = $loadIndex;
        
        return $this;
    }
}
