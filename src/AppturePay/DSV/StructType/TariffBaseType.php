<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for tariffBaseType StructType
 * @subpackage Structs
 */
class TariffBaseType extends AbstractStructBase
{
    /**
     * The base
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $base = null;
    /**
     * The round
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $round = null;
    /**
     * The rate
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $rate = null;
    /**
     * The unit
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $unit = null;
    /**
     * The value
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $value = null;
    /**
     * The currencyCode
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $currencyCode = null;
    /**
     * The percentage
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $percentage = null;
    /**
     * The type
     * @var string|null
     */
    protected ?string $type = null;
    /**
     * Constructor method for tariffBaseType
     * @uses TariffBaseType::setBase()
     * @uses TariffBaseType::setRound()
     * @uses TariffBaseType::setRate()
     * @uses TariffBaseType::setUnit()
     * @uses TariffBaseType::setValue()
     * @uses TariffBaseType::setCurrencyCode()
     * @uses TariffBaseType::setPercentage()
     * @uses TariffBaseType::setType()
     * @param float $base
     * @param float $round
     * @param float $rate
     * @param int $unit
     * @param float $value
     * @param string $currencyCode
     * @param float $percentage
     * @param string $type
     */
    public function __construct(?float $base = null, ?float $round = null, ?float $rate = null, ?int $unit = null, ?float $value = null, ?string $currencyCode = null, ?float $percentage = null, ?string $type = null)
    {
        $this
            ->setBase($base)
            ->setRound($round)
            ->setRate($rate)
            ->setUnit($unit)
            ->setValue($value)
            ->setCurrencyCode($currencyCode)
            ->setPercentage($percentage)
            ->setType($type);
    }
    /**
     * Get base value
     * @return float|null
     */
    public function getBase(): ?float
    {
        return $this->base;
    }
    /**
     * Set base value
     * @param float $base
     * @return \AppturePay\DSV\StructType\TariffBaseType
     */
    public function setBase(?float $base = null): self
    {
        // validation for constraint: float
        if (!is_null($base) && !(is_float($base) || is_numeric($base))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($base, true), gettype($base)), __LINE__);
        }
        $this->base = $base;
        
        return $this;
    }
    /**
     * Get round value
     * @return float|null
     */
    public function getRound(): ?float
    {
        return $this->round;
    }
    /**
     * Set round value
     * @param float $round
     * @return \AppturePay\DSV\StructType\TariffBaseType
     */
    public function setRound(?float $round = null): self
    {
        // validation for constraint: float
        if (!is_null($round) && !(is_float($round) || is_numeric($round))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($round, true), gettype($round)), __LINE__);
        }
        $this->round = $round;
        
        return $this;
    }
    /**
     * Get rate value
     * @return float|null
     */
    public function getRate(): ?float
    {
        return $this->rate;
    }
    /**
     * Set rate value
     * @param float $rate
     * @return \AppturePay\DSV\StructType\TariffBaseType
     */
    public function setRate(?float $rate = null): self
    {
        // validation for constraint: float
        if (!is_null($rate) && !(is_float($rate) || is_numeric($rate))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($rate, true), gettype($rate)), __LINE__);
        }
        $this->rate = $rate;
        
        return $this;
    }
    /**
     * Get unit value
     * @return int|null
     */
    public function getUnit(): ?int
    {
        return $this->unit;
    }
    /**
     * Set unit value
     * @param int $unit
     * @return \AppturePay\DSV\StructType\TariffBaseType
     */
    public function setUnit(?int $unit = null): self
    {
        // validation for constraint: int
        if (!is_null($unit) && !(is_int($unit) || ctype_digit($unit))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($unit, true), gettype($unit)), __LINE__);
        }
        $this->unit = $unit;
        
        return $this;
    }
    /**
     * Get value value
     * @return float|null
     */
    public function getValue(): ?float
    {
        return $this->value;
    }
    /**
     * Set value value
     * @param float $value
     * @return \AppturePay\DSV\StructType\TariffBaseType
     */
    public function setValue(?float $value = null): self
    {
        // validation for constraint: float
        if (!is_null($value) && !(is_float($value) || is_numeric($value))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($value, true), gettype($value)), __LINE__);
        }
        $this->value = $value;
        
        return $this;
    }
    /**
     * Get currencyCode value
     * @return string|null
     */
    public function getCurrencyCode(): ?string
    {
        return $this->currencyCode;
    }
    /**
     * Set currencyCode value
     * @param string $currencyCode
     * @return \AppturePay\DSV\StructType\TariffBaseType
     */
    public function setCurrencyCode(?string $currencyCode = null): self
    {
        // validation for constraint: string
        if (!is_null($currencyCode) && !is_string($currencyCode)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($currencyCode, true), gettype($currencyCode)), __LINE__);
        }
        $this->currencyCode = $currencyCode;
        
        return $this;
    }
    /**
     * Get percentage value
     * @return float|null
     */
    public function getPercentage(): ?float
    {
        return $this->percentage;
    }
    /**
     * Set percentage value
     * @param float $percentage
     * @return \AppturePay\DSV\StructType\TariffBaseType
     */
    public function setPercentage(?float $percentage = null): self
    {
        // validation for constraint: float
        if (!is_null($percentage) && !(is_float($percentage) || is_numeric($percentage))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($percentage, true), gettype($percentage)), __LINE__);
        }
        $this->percentage = $percentage;
        
        return $this;
    }
    /**
     * Get type value
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }
    /**
     * Set type value
     * @param string $type
     * @return \AppturePay\DSV\StructType\TariffBaseType
     */
    public function setType(?string $type = null): self
    {
        // validation for constraint: string
        if (!is_null($type) && !is_string($type)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($type, true), gettype($type)), __LINE__);
        }
        $this->type = $type;
        
        return $this;
    }
}
