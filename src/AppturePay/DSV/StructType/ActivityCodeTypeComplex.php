<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for activityCodeTypeComplex StructType
 * @subpackage Structs
 */
class ActivityCodeTypeComplex extends AbstractStructBase
{
    /**
     * The _
     * @var int|null
     */
    protected ?int $_ = null;
    /**
     * The group
     * @var string|null
     */
    protected ?string $group = null;
    /**
     * Constructor method for activityCodeTypeComplex
     * @uses ActivityCodeTypeComplex::set_()
     * @uses ActivityCodeTypeComplex::setGroup()
     * @param int $_
     * @param string $group
     */
    public function __construct(?int $_ = null, ?string $group = null)
    {
        $this
            ->set_($_)
            ->setGroup($group);
    }
    /**
     * Get _ value
     * @return int|null
     */
    public function get_(): ?int
    {
        return $this->_;
    }
    /**
     * Set _ value
     * @param int $_
     * @return \AppturePay\DSV\StructType\ActivityCodeTypeComplex
     */
    public function set_(?int $_ = null): self
    {
        // validation for constraint: int
        if (!is_null($_) && !(is_int($_) || ctype_digit($_))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($_, true), gettype($_)), __LINE__);
        }
        $this->_ = $_;
        
        return $this;
    }
    /**
     * Get group value
     * @return string|null
     */
    public function getGroup(): ?string
    {
        return $this->group;
    }
    /**
     * Set group value
     * @param string $group
     * @return \AppturePay\DSV\StructType\ActivityCodeTypeComplex
     */
    public function setGroup(?string $group = null): self
    {
        // validation for constraint: string
        if (!is_null($group) && !is_string($group)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($group, true), gettype($group)), __LINE__);
        }
        $this->group = $group;
        
        return $this;
    }
}
