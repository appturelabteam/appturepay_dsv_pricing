<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ediParametersType StructType
 * @subpackage Structs
 */
class EdiParametersType extends AbstractStructBase
{
    /**
     * The transmitter
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $transmitter = null;
    /**
     * The receiver
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $receiver = null;
    /**
     * The ediReference
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $ediReference = null;
    /**
     * The referenceIndication
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $referenceIndication = null;
    /**
     * The internalNumber
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $internalNumber = null;
    /**
     * The ediFunction1
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $ediFunction1 = null;
    /**
     * The dateTimeZone
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $dateTimeZone = null;
    /**
     * The messageReferenceNumber
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $messageReferenceNumber = null;
    /**
     * The logging
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\LoggingType[]
     */
    protected ?array $logging = null;
    /**
     * The fileHeader
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\FileHeaderType|null
     */
    protected ?\AppturePay\DSV\StructType\FileHeaderType $fileHeader = null;
    /**
     * The child
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\ChildType[]
     */
    protected ?array $child = null;
    /**
     * The status
     * @var string|null
     */
    protected ?string $status = null;
    /**
     * The type
     * @var string|null
     */
    protected ?string $type = null;
    /**
     * Constructor method for ediParametersType
     * @uses EdiParametersType::setTransmitter()
     * @uses EdiParametersType::setReceiver()
     * @uses EdiParametersType::setEdiReference()
     * @uses EdiParametersType::setReferenceIndication()
     * @uses EdiParametersType::setInternalNumber()
     * @uses EdiParametersType::setEdiFunction1()
     * @uses EdiParametersType::setDateTimeZone()
     * @uses EdiParametersType::setMessageReferenceNumber()
     * @uses EdiParametersType::setLogging()
     * @uses EdiParametersType::setFileHeader()
     * @uses EdiParametersType::setChild()
     * @uses EdiParametersType::setStatus()
     * @uses EdiParametersType::setType()
     * @param string $transmitter
     * @param string $receiver
     * @param string $ediReference
     * @param string $referenceIndication
     * @param int $internalNumber
     * @param string $ediFunction1
     * @param string $dateTimeZone
     * @param string $messageReferenceNumber
     * @param \AppturePay\DSV\StructType\LoggingType[] $logging
     * @param \AppturePay\DSV\StructType\FileHeaderType $fileHeader
     * @param \AppturePay\DSV\StructType\ChildType[] $child
     * @param string $status
     * @param string $type
     */
    public function __construct(?string $transmitter = null, ?string $receiver = null, ?string $ediReference = null, ?string $referenceIndication = null, ?int $internalNumber = null, ?string $ediFunction1 = null, ?string $dateTimeZone = null, ?string $messageReferenceNumber = null, ?array $logging = null, ?\AppturePay\DSV\StructType\FileHeaderType $fileHeader = null, ?array $child = null, ?string $status = null, ?string $type = null)
    {
        $this
            ->setTransmitter($transmitter)
            ->setReceiver($receiver)
            ->setEdiReference($ediReference)
            ->setReferenceIndication($referenceIndication)
            ->setInternalNumber($internalNumber)
            ->setEdiFunction1($ediFunction1)
            ->setDateTimeZone($dateTimeZone)
            ->setMessageReferenceNumber($messageReferenceNumber)
            ->setLogging($logging)
            ->setFileHeader($fileHeader)
            ->setChild($child)
            ->setStatus($status)
            ->setType($type);
    }
    /**
     * Get transmitter value
     * @return string|null
     */
    public function getTransmitter(): ?string
    {
        return $this->transmitter;
    }
    /**
     * Set transmitter value
     * @param string $transmitter
     * @return \AppturePay\DSV\StructType\EdiParametersType
     */
    public function setTransmitter(?string $transmitter = null): self
    {
        // validation for constraint: string
        if (!is_null($transmitter) && !is_string($transmitter)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($transmitter, true), gettype($transmitter)), __LINE__);
        }
        $this->transmitter = $transmitter;
        
        return $this;
    }
    /**
     * Get receiver value
     * @return string|null
     */
    public function getReceiver(): ?string
    {
        return $this->receiver;
    }
    /**
     * Set receiver value
     * @param string $receiver
     * @return \AppturePay\DSV\StructType\EdiParametersType
     */
    public function setReceiver(?string $receiver = null): self
    {
        // validation for constraint: string
        if (!is_null($receiver) && !is_string($receiver)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($receiver, true), gettype($receiver)), __LINE__);
        }
        $this->receiver = $receiver;
        
        return $this;
    }
    /**
     * Get ediReference value
     * @return string|null
     */
    public function getEdiReference(): ?string
    {
        return $this->ediReference;
    }
    /**
     * Set ediReference value
     * @param string $ediReference
     * @return \AppturePay\DSV\StructType\EdiParametersType
     */
    public function setEdiReference(?string $ediReference = null): self
    {
        // validation for constraint: string
        if (!is_null($ediReference) && !is_string($ediReference)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($ediReference, true), gettype($ediReference)), __LINE__);
        }
        $this->ediReference = $ediReference;
        
        return $this;
    }
    /**
     * Get referenceIndication value
     * @return string|null
     */
    public function getReferenceIndication(): ?string
    {
        return $this->referenceIndication;
    }
    /**
     * Set referenceIndication value
     * @uses \AppturePay\DSV\EnumType\ReferenceIndicationType::valueIsValid()
     * @uses \AppturePay\DSV\EnumType\ReferenceIndicationType::getValidValues()
     * @throws InvalidArgumentException
     * @param string $referenceIndication
     * @return \AppturePay\DSV\StructType\EdiParametersType
     */
    public function setReferenceIndication(?string $referenceIndication = null): self
    {
        // validation for constraint: enumeration
        if (!\AppturePay\DSV\EnumType\ReferenceIndicationType::valueIsValid($referenceIndication)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \AppturePay\DSV\EnumType\ReferenceIndicationType', is_array($referenceIndication) ? implode(', ', $referenceIndication) : var_export($referenceIndication, true), implode(', ', \AppturePay\DSV\EnumType\ReferenceIndicationType::getValidValues())), __LINE__);
        }
        $this->referenceIndication = $referenceIndication;
        
        return $this;
    }
    /**
     * Get internalNumber value
     * @return int|null
     */
    public function getInternalNumber(): ?int
    {
        return $this->internalNumber;
    }
    /**
     * Set internalNumber value
     * @param int $internalNumber
     * @return \AppturePay\DSV\StructType\EdiParametersType
     */
    public function setInternalNumber(?int $internalNumber = null): self
    {
        // validation for constraint: int
        if (!is_null($internalNumber) && !(is_int($internalNumber) || ctype_digit($internalNumber))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($internalNumber, true), gettype($internalNumber)), __LINE__);
        }
        $this->internalNumber = $internalNumber;
        
        return $this;
    }
    /**
     * Get ediFunction1 value
     * @return string|null
     */
    public function getEdiFunction1(): ?string
    {
        return $this->ediFunction1;
    }
    /**
     * Set ediFunction1 value
     * @param string $ediFunction1
     * @return \AppturePay\DSV\StructType\EdiParametersType
     */
    public function setEdiFunction1(?string $ediFunction1 = null): self
    {
        // validation for constraint: string
        if (!is_null($ediFunction1) && !is_string($ediFunction1)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($ediFunction1, true), gettype($ediFunction1)), __LINE__);
        }
        $this->ediFunction1 = $ediFunction1;
        
        return $this;
    }
    /**
     * Get dateTimeZone value
     * @return string|null
     */
    public function getDateTimeZone(): ?string
    {
        return $this->dateTimeZone;
    }
    /**
     * Set dateTimeZone value
     * @param string $dateTimeZone
     * @return \AppturePay\DSV\StructType\EdiParametersType
     */
    public function setDateTimeZone(?string $dateTimeZone = null): self
    {
        // validation for constraint: string
        if (!is_null($dateTimeZone) && !is_string($dateTimeZone)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($dateTimeZone, true), gettype($dateTimeZone)), __LINE__);
        }
        $this->dateTimeZone = $dateTimeZone;
        
        return $this;
    }
    /**
     * Get messageReferenceNumber value
     * @return string|null
     */
    public function getMessageReferenceNumber(): ?string
    {
        return $this->messageReferenceNumber;
    }
    /**
     * Set messageReferenceNumber value
     * @param string $messageReferenceNumber
     * @return \AppturePay\DSV\StructType\EdiParametersType
     */
    public function setMessageReferenceNumber(?string $messageReferenceNumber = null): self
    {
        // validation for constraint: string
        if (!is_null($messageReferenceNumber) && !is_string($messageReferenceNumber)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($messageReferenceNumber, true), gettype($messageReferenceNumber)), __LINE__);
        }
        $this->messageReferenceNumber = $messageReferenceNumber;
        
        return $this;
    }
    /**
     * Get logging value
     * @return \AppturePay\DSV\StructType\LoggingType[]
     */
    public function getLogging(): ?array
    {
        return $this->logging;
    }
    /**
     * This method is responsible for validating the values passed to the setLogging method
     * This method is willingly generated in order to preserve the one-line inline validation within the setLogging method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateLoggingForArrayConstraintsFromSetLogging(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $ediParametersTypeLoggingItem) {
            // validation for constraint: itemType
            if (!$ediParametersTypeLoggingItem instanceof \AppturePay\DSV\StructType\LoggingType) {
                $invalidValues[] = is_object($ediParametersTypeLoggingItem) ? get_class($ediParametersTypeLoggingItem) : sprintf('%s(%s)', gettype($ediParametersTypeLoggingItem), var_export($ediParametersTypeLoggingItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The logging property can only contain items of type \AppturePay\DSV\StructType\LoggingType, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set logging value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\LoggingType[] $logging
     * @return \AppturePay\DSV\StructType\EdiParametersType
     */
    public function setLogging(?array $logging = null): self
    {
        // validation for constraint: array
        if ('' !== ($loggingArrayErrorMessage = self::validateLoggingForArrayConstraintsFromSetLogging($logging))) {
            throw new InvalidArgumentException($loggingArrayErrorMessage, __LINE__);
        }
        $this->logging = $logging;
        
        return $this;
    }
    /**
     * Add item to logging value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\LoggingType $item
     * @return \AppturePay\DSV\StructType\EdiParametersType
     */
    public function addToLogging(\AppturePay\DSV\StructType\LoggingType $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \AppturePay\DSV\StructType\LoggingType) {
            throw new InvalidArgumentException(sprintf('The logging property can only contain items of type \AppturePay\DSV\StructType\LoggingType, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->logging[] = $item;
        
        return $this;
    }
    /**
     * Get fileHeader value
     * @return \AppturePay\DSV\StructType\FileHeaderType|null
     */
    public function getFileHeader(): ?\AppturePay\DSV\StructType\FileHeaderType
    {
        return $this->fileHeader;
    }
    /**
     * Set fileHeader value
     * @param \AppturePay\DSV\StructType\FileHeaderType $fileHeader
     * @return \AppturePay\DSV\StructType\EdiParametersType
     */
    public function setFileHeader(?\AppturePay\DSV\StructType\FileHeaderType $fileHeader = null): self
    {
        $this->fileHeader = $fileHeader;
        
        return $this;
    }
    /**
     * Get child value
     * @return \AppturePay\DSV\StructType\ChildType[]
     */
    public function getChild(): ?array
    {
        return $this->child;
    }
    /**
     * This method is responsible for validating the values passed to the setChild method
     * This method is willingly generated in order to preserve the one-line inline validation within the setChild method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateChildForArrayConstraintsFromSetChild(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $ediParametersTypeChildItem) {
            // validation for constraint: itemType
            if (!$ediParametersTypeChildItem instanceof \AppturePay\DSV\StructType\ChildType) {
                $invalidValues[] = is_object($ediParametersTypeChildItem) ? get_class($ediParametersTypeChildItem) : sprintf('%s(%s)', gettype($ediParametersTypeChildItem), var_export($ediParametersTypeChildItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The child property can only contain items of type \AppturePay\DSV\StructType\ChildType, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set child value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\ChildType[] $child
     * @return \AppturePay\DSV\StructType\EdiParametersType
     */
    public function setChild(?array $child = null): self
    {
        // validation for constraint: array
        if ('' !== ($childArrayErrorMessage = self::validateChildForArrayConstraintsFromSetChild($child))) {
            throw new InvalidArgumentException($childArrayErrorMessage, __LINE__);
        }
        $this->child = $child;
        
        return $this;
    }
    /**
     * Add item to child value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\ChildType $item
     * @return \AppturePay\DSV\StructType\EdiParametersType
     */
    public function addToChild(\AppturePay\DSV\StructType\ChildType $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \AppturePay\DSV\StructType\ChildType) {
            throw new InvalidArgumentException(sprintf('The child property can only contain items of type \AppturePay\DSV\StructType\ChildType, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->child[] = $item;
        
        return $this;
    }
    /**
     * Get status value
     * @return string|null
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }
    /**
     * Set status value
     * @param string $status
     * @return \AppturePay\DSV\StructType\EdiParametersType
     */
    public function setStatus(?string $status = null): self
    {
        // validation for constraint: string
        if (!is_null($status) && !is_string($status)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($status, true), gettype($status)), __LINE__);
        }
        $this->status = $status;
        
        return $this;
    }
    /**
     * Get type value
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }
    /**
     * Set type value
     * @param string $type
     * @return \AppturePay\DSV\StructType\EdiParametersType
     */
    public function setType(?string $type = null): self
    {
        // validation for constraint: string
        if (!is_null($type) && !is_string($type)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($type, true), gettype($type)), __LINE__);
        }
        $this->type = $type;
        
        return $this;
    }
}
