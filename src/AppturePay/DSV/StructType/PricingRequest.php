<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for pricingRequest StructType
 * @subpackage Structs
 */
class PricingRequest extends AbstractStructBase
{
    /**
     * The pricing
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\PricingType|null
     */
    protected ?\AppturePay\DSV\StructType\PricingType $pricing = null;
    /**
     * Constructor method for pricingRequest
     * @uses PricingRequest::setPricing()
     * @param \AppturePay\DSV\StructType\PricingType $pricing
     */
    public function __construct(?\AppturePay\DSV\StructType\PricingType $pricing = null)
    {
        $this
            ->setPricing($pricing);
    }
    /**
     * Get pricing value
     * @return \AppturePay\DSV\StructType\PricingType|null
     */
    public function getPricing(): ?\AppturePay\DSV\StructType\PricingType
    {
        return $this->pricing;
    }
    /**
     * Set pricing value
     * @param \AppturePay\DSV\StructType\PricingType $pricing
     * @return \AppturePay\DSV\StructType\PricingRequest
     */
    public function setPricing(?\AppturePay\DSV\StructType\PricingType $pricing = null): self
    {
        $this->pricing = $pricing;
        
        return $this;
    }
}
