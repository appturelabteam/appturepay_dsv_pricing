<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for dimensionType StructType
 * @subpackage Structs
 */
class DimensionType extends AbstractStructBase
{
    /**
     * The length
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $length = null;
    /**
     * The width
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $width = null;
    /**
     * The height
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $height = null;
    /**
     * Constructor method for dimensionType
     * @uses DimensionType::setLength()
     * @uses DimensionType::setWidth()
     * @uses DimensionType::setHeight()
     * @param float $length
     * @param float $width
     * @param float $height
     */
    public function __construct(?float $length = null, ?float $width = null, ?float $height = null)
    {
        $this
            ->setLength($length)
            ->setWidth($width)
            ->setHeight($height);
    }
    /**
     * Get length value
     * @return float|null
     */
    public function getLength(): ?float
    {
        return $this->length;
    }
    /**
     * Set length value
     * @param float $length
     * @return \AppturePay\DSV\StructType\DimensionType
     */
    public function setLength(?float $length = null): self
    {
        // validation for constraint: float
        if (!is_null($length) && !(is_float($length) || is_numeric($length))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($length, true), gettype($length)), __LINE__);
        }
        $this->length = $length;
        
        return $this;
    }
    /**
     * Get width value
     * @return float|null
     */
    public function getWidth(): ?float
    {
        return $this->width;
    }
    /**
     * Set width value
     * @param float $width
     * @return \AppturePay\DSV\StructType\DimensionType
     */
    public function setWidth(?float $width = null): self
    {
        // validation for constraint: float
        if (!is_null($width) && !(is_float($width) || is_numeric($width))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($width, true), gettype($width)), __LINE__);
        }
        $this->width = $width;
        
        return $this;
    }
    /**
     * Get height value
     * @return float|null
     */
    public function getHeight(): ?float
    {
        return $this->height;
    }
    /**
     * Set height value
     * @param float $height
     * @return \AppturePay\DSV\StructType\DimensionType
     */
    public function setHeight(?float $height = null): self
    {
        // validation for constraint: float
        if (!is_null($height) && !(is_float($height) || is_numeric($height))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($height, true), gettype($height)), __LINE__);
        }
        $this->height = $height;
        
        return $this;
    }
}
