<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for additionalType StructType
 * @subpackage Structs
 */
class AdditionalType extends AbstractStructBase
{
    /**
     * The vatIdNumber
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $vatIdNumber = null;
    /**
     * The ILNNumber
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $ILNNumber = null;
    /**
     * Constructor method for additionalType
     * @uses AdditionalType::setVatIdNumber()
     * @uses AdditionalType::setILNNumber()
     * @param string $vatIdNumber
     * @param string $iLNNumber
     */
    public function __construct(?string $vatIdNumber = null, ?string $iLNNumber = null)
    {
        $this
            ->setVatIdNumber($vatIdNumber)
            ->setILNNumber($iLNNumber);
    }
    /**
     * Get vatIdNumber value
     * @return string|null
     */
    public function getVatIdNumber(): ?string
    {
        return $this->vatIdNumber;
    }
    /**
     * Set vatIdNumber value
     * @param string $vatIdNumber
     * @return \AppturePay\DSV\StructType\AdditionalType
     */
    public function setVatIdNumber(?string $vatIdNumber = null): self
    {
        // validation for constraint: string
        if (!is_null($vatIdNumber) && !is_string($vatIdNumber)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($vatIdNumber, true), gettype($vatIdNumber)), __LINE__);
        }
        $this->vatIdNumber = $vatIdNumber;
        
        return $this;
    }
    /**
     * Get ILNNumber value
     * @return string|null
     */
    public function getILNNumber(): ?string
    {
        return $this->ILNNumber;
    }
    /**
     * Set ILNNumber value
     * @param string $iLNNumber
     * @return \AppturePay\DSV\StructType\AdditionalType
     */
    public function setILNNumber(?string $iLNNumber = null): self
    {
        // validation for constraint: string
        if (!is_null($iLNNumber) && !is_string($iLNNumber)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($iLNNumber, true), gettype($iLNNumber)), __LINE__);
        }
        $this->ILNNumber = $iLNNumber;
        
        return $this;
    }
}
