<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for dangerousGoodsType StructType
 * @subpackage Structs
 */
class DangerousGoodsType extends AbstractStructBase
{
    /**
     * The dangerousGoodsUNNumber
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $dangerousGoodsUNNumber = null;
    /**
     * The dangerousGoodsUNSuffix
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $dangerousGoodsUNSuffix = null;
    /**
     * Constructor method for dangerousGoodsType
     * @uses DangerousGoodsType::setDangerousGoodsUNNumber()
     * @uses DangerousGoodsType::setDangerousGoodsUNSuffix()
     * @param int $dangerousGoodsUNNumber
     * @param int $dangerousGoodsUNSuffix
     */
    public function __construct(?int $dangerousGoodsUNNumber = null, ?int $dangerousGoodsUNSuffix = null)
    {
        $this
            ->setDangerousGoodsUNNumber($dangerousGoodsUNNumber)
            ->setDangerousGoodsUNSuffix($dangerousGoodsUNSuffix);
    }
    /**
     * Get dangerousGoodsUNNumber value
     * @return int|null
     */
    public function getDangerousGoodsUNNumber(): ?int
    {
        return $this->dangerousGoodsUNNumber;
    }
    /**
     * Set dangerousGoodsUNNumber value
     * @param int $dangerousGoodsUNNumber
     * @return \AppturePay\DSV\StructType\DangerousGoodsType
     */
    public function setDangerousGoodsUNNumber(?int $dangerousGoodsUNNumber = null): self
    {
        // validation for constraint: int
        if (!is_null($dangerousGoodsUNNumber) && !(is_int($dangerousGoodsUNNumber) || ctype_digit($dangerousGoodsUNNumber))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($dangerousGoodsUNNumber, true), gettype($dangerousGoodsUNNumber)), __LINE__);
        }
        $this->dangerousGoodsUNNumber = $dangerousGoodsUNNumber;
        
        return $this;
    }
    /**
     * Get dangerousGoodsUNSuffix value
     * @return int|null
     */
    public function getDangerousGoodsUNSuffix(): ?int
    {
        return $this->dangerousGoodsUNSuffix;
    }
    /**
     * Set dangerousGoodsUNSuffix value
     * @param int $dangerousGoodsUNSuffix
     * @return \AppturePay\DSV\StructType\DangerousGoodsType
     */
    public function setDangerousGoodsUNSuffix(?int $dangerousGoodsUNSuffix = null): self
    {
        // validation for constraint: int
        if (!is_null($dangerousGoodsUNSuffix) && !(is_int($dangerousGoodsUNSuffix) || ctype_digit($dangerousGoodsUNSuffix))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($dangerousGoodsUNSuffix, true), gettype($dangerousGoodsUNSuffix)), __LINE__);
        }
        $this->dangerousGoodsUNSuffix = $dangerousGoodsUNSuffix;
        
        return $this;
    }
}
