<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for invoiceLineType StructType
 * @subpackage Structs
 */
class InvoiceLineType extends AbstractStructBase
{
    /**
     * The activityCode
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\ActivityCodeTypeComplex|null
     */
    protected ?\AppturePay\DSV\StructType\ActivityCodeTypeComplex $activityCode = null;
    /**
     * The sequentialNumber
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $sequentialNumber = null;
    /**
     * The transactionType
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $transactionType = null;
    /**
     * The relationNumber
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $relationNumber = null;
    /**
     * The searchName
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $searchName = null;
    /**
     * The amount
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $amount = null;
    /**
     * The currencyCode
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $currencyCode = null;
    /**
     * The description
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $description = null;
    /**
     * The vat
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\VatType|null
     */
    protected ?\AppturePay\DSV\StructType\VatType $vat = null;
    /**
     * The weightCalculation
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\WeightCalculationType|null
     */
    protected ?\AppturePay\DSV\StructType\WeightCalculationType $weightCalculation = null;
    /**
     * The tariffBase
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\TariffBaseType[]
     */
    protected ?array $tariffBase = null;
    /**
     * The tariffSpecification
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\TariffSpecificationType|null
     */
    protected ?\AppturePay\DSV\StructType\TariffSpecificationType $tariffSpecification = null;
    /**
     * The type
     * @var string|null
     */
    protected ?string $type = null;
    /**
     * Constructor method for invoiceLineType
     * @uses InvoiceLineType::setActivityCode()
     * @uses InvoiceLineType::setSequentialNumber()
     * @uses InvoiceLineType::setTransactionType()
     * @uses InvoiceLineType::setRelationNumber()
     * @uses InvoiceLineType::setSearchName()
     * @uses InvoiceLineType::setAmount()
     * @uses InvoiceLineType::setCurrencyCode()
     * @uses InvoiceLineType::setDescription()
     * @uses InvoiceLineType::setVat()
     * @uses InvoiceLineType::setWeightCalculation()
     * @uses InvoiceLineType::setTariffBase()
     * @uses InvoiceLineType::setTariffSpecification()
     * @uses InvoiceLineType::setType()
     * @param \AppturePay\DSV\StructType\ActivityCodeTypeComplex $activityCode
     * @param int $sequentialNumber
     * @param string $transactionType
     * @param int $relationNumber
     * @param string $searchName
     * @param float $amount
     * @param string $currencyCode
     * @param string $description
     * @param \AppturePay\DSV\StructType\VatType $vat
     * @param \AppturePay\DSV\StructType\WeightCalculationType $weightCalculation
     * @param \AppturePay\DSV\StructType\TariffBaseType[] $tariffBase
     * @param \AppturePay\DSV\StructType\TariffSpecificationType $tariffSpecification
     * @param string $type
     */
    public function __construct(?\AppturePay\DSV\StructType\ActivityCodeTypeComplex $activityCode = null, ?int $sequentialNumber = null, ?string $transactionType = null, ?int $relationNumber = null, ?string $searchName = null, ?float $amount = null, ?string $currencyCode = null, ?string $description = null, ?\AppturePay\DSV\StructType\VatType $vat = null, ?\AppturePay\DSV\StructType\WeightCalculationType $weightCalculation = null, ?array $tariffBase = null, ?\AppturePay\DSV\StructType\TariffSpecificationType $tariffSpecification = null, ?string $type = null)
    {
        $this
            ->setActivityCode($activityCode)
            ->setSequentialNumber($sequentialNumber)
            ->setTransactionType($transactionType)
            ->setRelationNumber($relationNumber)
            ->setSearchName($searchName)
            ->setAmount($amount)
            ->setCurrencyCode($currencyCode)
            ->setDescription($description)
            ->setVat($vat)
            ->setWeightCalculation($weightCalculation)
            ->setTariffBase($tariffBase)
            ->setTariffSpecification($tariffSpecification)
            ->setType($type);
    }
    /**
     * Get activityCode value
     * @return \AppturePay\DSV\StructType\ActivityCodeTypeComplex|null
     */
    public function getActivityCode(): ?\AppturePay\DSV\StructType\ActivityCodeTypeComplex
    {
        return $this->activityCode;
    }
    /**
     * Set activityCode value
     * @param \AppturePay\DSV\StructType\ActivityCodeTypeComplex $activityCode
     * @return \AppturePay\DSV\StructType\InvoiceLineType
     */
    public function setActivityCode(?\AppturePay\DSV\StructType\ActivityCodeTypeComplex $activityCode = null): self
    {
        $this->activityCode = $activityCode;
        
        return $this;
    }
    /**
     * Get sequentialNumber value
     * @return int|null
     */
    public function getSequentialNumber(): ?int
    {
        return $this->sequentialNumber;
    }
    /**
     * Set sequentialNumber value
     * @param int $sequentialNumber
     * @return \AppturePay\DSV\StructType\InvoiceLineType
     */
    public function setSequentialNumber(?int $sequentialNumber = null): self
    {
        // validation for constraint: int
        if (!is_null($sequentialNumber) && !(is_int($sequentialNumber) || ctype_digit($sequentialNumber))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($sequentialNumber, true), gettype($sequentialNumber)), __LINE__);
        }
        $this->sequentialNumber = $sequentialNumber;
        
        return $this;
    }
    /**
     * Get transactionType value
     * @return string|null
     */
    public function getTransactionType(): ?string
    {
        return $this->transactionType;
    }
    /**
     * Set transactionType value
     * @uses \AppturePay\DSV\EnumType\TransactionTypeType::valueIsValid()
     * @uses \AppturePay\DSV\EnumType\TransactionTypeType::getValidValues()
     * @throws InvalidArgumentException
     * @param string $transactionType
     * @return \AppturePay\DSV\StructType\InvoiceLineType
     */
    public function setTransactionType(?string $transactionType = null): self
    {
        // validation for constraint: enumeration
        if (!\AppturePay\DSV\EnumType\TransactionTypeType::valueIsValid($transactionType)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \AppturePay\DSV\EnumType\TransactionTypeType', is_array($transactionType) ? implode(', ', $transactionType) : var_export($transactionType, true), implode(', ', \AppturePay\DSV\EnumType\TransactionTypeType::getValidValues())), __LINE__);
        }
        $this->transactionType = $transactionType;
        
        return $this;
    }
    /**
     * Get relationNumber value
     * @return int|null
     */
    public function getRelationNumber(): ?int
    {
        return $this->relationNumber;
    }
    /**
     * Set relationNumber value
     * @param int $relationNumber
     * @return \AppturePay\DSV\StructType\InvoiceLineType
     */
    public function setRelationNumber(?int $relationNumber = null): self
    {
        // validation for constraint: int
        if (!is_null($relationNumber) && !(is_int($relationNumber) || ctype_digit($relationNumber))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($relationNumber, true), gettype($relationNumber)), __LINE__);
        }
        $this->relationNumber = $relationNumber;
        
        return $this;
    }
    /**
     * Get searchName value
     * @return string|null
     */
    public function getSearchName(): ?string
    {
        return $this->searchName;
    }
    /**
     * Set searchName value
     * @param string $searchName
     * @return \AppturePay\DSV\StructType\InvoiceLineType
     */
    public function setSearchName(?string $searchName = null): self
    {
        // validation for constraint: string
        if (!is_null($searchName) && !is_string($searchName)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($searchName, true), gettype($searchName)), __LINE__);
        }
        $this->searchName = $searchName;
        
        return $this;
    }
    /**
     * Get amount value
     * @return float|null
     */
    public function getAmount(): ?float
    {
        return $this->amount;
    }
    /**
     * Set amount value
     * @param float $amount
     * @return \AppturePay\DSV\StructType\InvoiceLineType
     */
    public function setAmount(?float $amount = null): self
    {
        // validation for constraint: float
        if (!is_null($amount) && !(is_float($amount) || is_numeric($amount))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($amount, true), gettype($amount)), __LINE__);
        }
        $this->amount = $amount;
        
        return $this;
    }
    /**
     * Get currencyCode value
     * @return string|null
     */
    public function getCurrencyCode(): ?string
    {
        return $this->currencyCode;
    }
    /**
     * Set currencyCode value
     * @param string $currencyCode
     * @return \AppturePay\DSV\StructType\InvoiceLineType
     */
    public function setCurrencyCode(?string $currencyCode = null): self
    {
        // validation for constraint: string
        if (!is_null($currencyCode) && !is_string($currencyCode)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($currencyCode, true), gettype($currencyCode)), __LINE__);
        }
        $this->currencyCode = $currencyCode;
        
        return $this;
    }
    /**
     * Get description value
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }
    /**
     * Set description value
     * @param string $description
     * @return \AppturePay\DSV\StructType\InvoiceLineType
     */
    public function setDescription(?string $description = null): self
    {
        // validation for constraint: string
        if (!is_null($description) && !is_string($description)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($description, true), gettype($description)), __LINE__);
        }
        $this->description = $description;
        
        return $this;
    }
    /**
     * Get vat value
     * @return \AppturePay\DSV\StructType\VatType|null
     */
    public function getVat(): ?\AppturePay\DSV\StructType\VatType
    {
        return $this->vat;
    }
    /**
     * Set vat value
     * @param \AppturePay\DSV\StructType\VatType $vat
     * @return \AppturePay\DSV\StructType\InvoiceLineType
     */
    public function setVat(?\AppturePay\DSV\StructType\VatType $vat = null): self
    {
        $this->vat = $vat;
        
        return $this;
    }
    /**
     * Get weightCalculation value
     * @return \AppturePay\DSV\StructType\WeightCalculationType|null
     */
    public function getWeightCalculation(): ?\AppturePay\DSV\StructType\WeightCalculationType
    {
        return $this->weightCalculation;
    }
    /**
     * Set weightCalculation value
     * @param \AppturePay\DSV\StructType\WeightCalculationType $weightCalculation
     * @return \AppturePay\DSV\StructType\InvoiceLineType
     */
    public function setWeightCalculation(?\AppturePay\DSV\StructType\WeightCalculationType $weightCalculation = null): self
    {
        $this->weightCalculation = $weightCalculation;
        
        return $this;
    }
    /**
     * Get tariffBase value
     * @return \AppturePay\DSV\StructType\TariffBaseType[]
     */
    public function getTariffBase(): ?array
    {
        return $this->tariffBase;
    }
    /**
     * This method is responsible for validating the values passed to the setTariffBase method
     * This method is willingly generated in order to preserve the one-line inline validation within the setTariffBase method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateTariffBaseForArrayConstraintsFromSetTariffBase(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $invoiceLineTypeTariffBaseItem) {
            // validation for constraint: itemType
            if (!$invoiceLineTypeTariffBaseItem instanceof \AppturePay\DSV\StructType\TariffBaseType) {
                $invalidValues[] = is_object($invoiceLineTypeTariffBaseItem) ? get_class($invoiceLineTypeTariffBaseItem) : sprintf('%s(%s)', gettype($invoiceLineTypeTariffBaseItem), var_export($invoiceLineTypeTariffBaseItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The tariffBase property can only contain items of type \AppturePay\DSV\StructType\TariffBaseType, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set tariffBase value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\TariffBaseType[] $tariffBase
     * @return \AppturePay\DSV\StructType\InvoiceLineType
     */
    public function setTariffBase(?array $tariffBase = null): self
    {
        // validation for constraint: array
        if ('' !== ($tariffBaseArrayErrorMessage = self::validateTariffBaseForArrayConstraintsFromSetTariffBase($tariffBase))) {
            throw new InvalidArgumentException($tariffBaseArrayErrorMessage, __LINE__);
        }
        $this->tariffBase = $tariffBase;
        
        return $this;
    }
    /**
     * Add item to tariffBase value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\TariffBaseType $item
     * @return \AppturePay\DSV\StructType\InvoiceLineType
     */
    public function addToTariffBase(\AppturePay\DSV\StructType\TariffBaseType $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \AppturePay\DSV\StructType\TariffBaseType) {
            throw new InvalidArgumentException(sprintf('The tariffBase property can only contain items of type \AppturePay\DSV\StructType\TariffBaseType, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->tariffBase[] = $item;
        
        return $this;
    }
    /**
     * Get tariffSpecification value
     * @return \AppturePay\DSV\StructType\TariffSpecificationType|null
     */
    public function getTariffSpecification(): ?\AppturePay\DSV\StructType\TariffSpecificationType
    {
        return $this->tariffSpecification;
    }
    /**
     * Set tariffSpecification value
     * @param \AppturePay\DSV\StructType\TariffSpecificationType $tariffSpecification
     * @return \AppturePay\DSV\StructType\InvoiceLineType
     */
    public function setTariffSpecification(?\AppturePay\DSV\StructType\TariffSpecificationType $tariffSpecification = null): self
    {
        $this->tariffSpecification = $tariffSpecification;
        
        return $this;
    }
    /**
     * Get type value
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }
    /**
     * Set type value
     * @param string $type
     * @return \AppturePay\DSV\StructType\InvoiceLineType
     */
    public function setType(?string $type = null): self
    {
        // validation for constraint: string
        if (!is_null($type) && !is_string($type)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($type, true), gettype($type)), __LINE__);
        }
        $this->type = $type;
        
        return $this;
    }
}
