<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for fileType StructType
 * @subpackage Structs
 */
class FileType extends AbstractStructBase
{
    /**
     * The arrivalDate
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $arrivalDate = null;
    /**
     * The loadingDate
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $loadingDate = null;
    /**
     * The loadingTime
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $loadingTime = null;
    /**
     * The unloadingDate
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $unloadingDate = null;
    /**
     * The unloadingTime
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $unloadingTime = null;
    /**
     * The marksAndNumber
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $marksAndNumber = null;
    /**
     * The waybill
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $waybill = null;
    /**
     * The houseWaybill
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $houseWaybill = null;
    /**
     * The primaryReference
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $primaryReference = null;
    /**
     * The serviceLevel
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $serviceLevel = null;
    /**
     * The tariffType
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $tariffType = null;
    /**
     * The deliveryTerm
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $deliveryTerm = null;
    /**
     * The deliveryTermPlace
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $deliveryTermPlace = null;
    /**
     * The originPlaceCode
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $originPlaceCode = null;
    /**
     * The destinationPlaceCode
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $destinationPlaceCode = null;
    /**
     * The totals
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\TotalsType|null
     */
    protected ?\AppturePay\DSV\StructType\TotalsType $totals = null;
    /**
     * The dimension
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\DimensionType|null
     */
    protected ?\AppturePay\DSV\StructType\DimensionType $dimension = null;
    /**
     * The parameters
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\ParametersType|null
     */
    protected ?\AppturePay\DSV\StructType\ParametersType $parameters = null;
    /**
     * The extraParameters
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\ExtraParametersType|null
     */
    protected ?\AppturePay\DSV\StructType\ExtraParametersType $extraParameters = null;
    /**
     * The textKeys
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\TextKeysType[]
     */
    protected ?array $textKeys = null;
    /**
     * The address
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\AddressType[]
     */
    protected ?array $address = null;
    /**
     * The additionalServices
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\AdditionalServicesType[]
     */
    protected ?array $additionalServices = null;
    /**
     * The transportInstruction
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\TransportInstructionType[]
     */
    protected ?array $transportInstruction = null;
    /**
     * The planCharacteristic
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\PlanCharacteristicType[]
     */
    protected ?array $planCharacteristic = null;
    /**
     * The goodsLine
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\GoodsLineType[]
     */
    protected ?array $goodsLine = null;
    /**
     * The type
     * @var string|null
     */
    protected ?string $type = null;
    /**
     * Constructor method for fileType
     * @uses FileType::setArrivalDate()
     * @uses FileType::setLoadingDate()
     * @uses FileType::setLoadingTime()
     * @uses FileType::setUnloadingDate()
     * @uses FileType::setUnloadingTime()
     * @uses FileType::setMarksAndNumber()
     * @uses FileType::setWaybill()
     * @uses FileType::setHouseWaybill()
     * @uses FileType::setPrimaryReference()
     * @uses FileType::setServiceLevel()
     * @uses FileType::setTariffType()
     * @uses FileType::setDeliveryTerm()
     * @uses FileType::setDeliveryTermPlace()
     * @uses FileType::setOriginPlaceCode()
     * @uses FileType::setDestinationPlaceCode()
     * @uses FileType::setTotals()
     * @uses FileType::setDimension()
     * @uses FileType::setParameters()
     * @uses FileType::setExtraParameters()
     * @uses FileType::setTextKeys()
     * @uses FileType::setAddress()
     * @uses FileType::setAdditionalServices()
     * @uses FileType::setTransportInstruction()
     * @uses FileType::setPlanCharacteristic()
     * @uses FileType::setGoodsLine()
     * @uses FileType::setType()
     * @param string $arrivalDate
     * @param string $loadingDate
     * @param string $loadingTime
     * @param string $unloadingDate
     * @param string $unloadingTime
     * @param string $marksAndNumber
     * @param string $waybill
     * @param string $houseWaybill
     * @param string $primaryReference
     * @param string $serviceLevel
     * @param string $tariffType
     * @param string $deliveryTerm
     * @param string $deliveryTermPlace
     * @param string $originPlaceCode
     * @param string $destinationPlaceCode
     * @param \AppturePay\DSV\StructType\TotalsType $totals
     * @param \AppturePay\DSV\StructType\DimensionType $dimension
     * @param \AppturePay\DSV\StructType\ParametersType $parameters
     * @param \AppturePay\DSV\StructType\ExtraParametersType $extraParameters
     * @param \AppturePay\DSV\StructType\TextKeysType[] $textKeys
     * @param \AppturePay\DSV\StructType\AddressType[] $address
     * @param \AppturePay\DSV\StructType\AdditionalServicesType[] $additionalServices
     * @param \AppturePay\DSV\StructType\TransportInstructionType[] $transportInstruction
     * @param \AppturePay\DSV\StructType\PlanCharacteristicType[] $planCharacteristic
     * @param \AppturePay\DSV\StructType\GoodsLineType[] $goodsLine
     * @param string $type
     */
    public function __construct(?string $arrivalDate = null, ?string $loadingDate = null, ?string $loadingTime = null, ?string $unloadingDate = null, ?string $unloadingTime = null, ?string $marksAndNumber = null, ?string $waybill = null, ?string $houseWaybill = null, ?string $primaryReference = null, ?string $serviceLevel = null, ?string $tariffType = null, ?string $deliveryTerm = null, ?string $deliveryTermPlace = null, ?string $originPlaceCode = null, ?string $destinationPlaceCode = null, ?\AppturePay\DSV\StructType\TotalsType $totals = null, ?\AppturePay\DSV\StructType\DimensionType $dimension = null, ?\AppturePay\DSV\StructType\ParametersType $parameters = null, ?\AppturePay\DSV\StructType\ExtraParametersType $extraParameters = null, ?array $textKeys = null, ?array $address = null, ?array $additionalServices = null, ?array $transportInstruction = null, ?array $planCharacteristic = null, ?array $goodsLine = null, ?string $type = null)
    {
        $this
            ->setArrivalDate($arrivalDate)
            ->setLoadingDate($loadingDate)
            ->setLoadingTime($loadingTime)
            ->setUnloadingDate($unloadingDate)
            ->setUnloadingTime($unloadingTime)
            ->setMarksAndNumber($marksAndNumber)
            ->setWaybill($waybill)
            ->setHouseWaybill($houseWaybill)
            ->setPrimaryReference($primaryReference)
            ->setServiceLevel($serviceLevel)
            ->setTariffType($tariffType)
            ->setDeliveryTerm($deliveryTerm)
            ->setDeliveryTermPlace($deliveryTermPlace)
            ->setOriginPlaceCode($originPlaceCode)
            ->setDestinationPlaceCode($destinationPlaceCode)
            ->setTotals($totals)
            ->setDimension($dimension)
            ->setParameters($parameters)
            ->setExtraParameters($extraParameters)
            ->setTextKeys($textKeys)
            ->setAddress($address)
            ->setAdditionalServices($additionalServices)
            ->setTransportInstruction($transportInstruction)
            ->setPlanCharacteristic($planCharacteristic)
            ->setGoodsLine($goodsLine)
            ->setType($type);
    }
    /**
     * Get arrivalDate value
     * @return string|null
     */
    public function getArrivalDate(): ?string
    {
        return $this->arrivalDate;
    }
    /**
     * Set arrivalDate value
     * @param string $arrivalDate
     * @return \AppturePay\DSV\StructType\FileType
     */
    public function setArrivalDate(?string $arrivalDate = null): self
    {
        // validation for constraint: string
        if (!is_null($arrivalDate) && !is_string($arrivalDate)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($arrivalDate, true), gettype($arrivalDate)), __LINE__);
        }
        $this->arrivalDate = $arrivalDate;
        
        return $this;
    }
    /**
     * Get loadingDate value
     * @return string|null
     */
    public function getLoadingDate(): ?string
    {
        return $this->loadingDate;
    }
    /**
     * Set loadingDate value
     * @param string $loadingDate
     * @return \AppturePay\DSV\StructType\FileType
     */
    public function setLoadingDate(?string $loadingDate = null): self
    {
        // validation for constraint: string
        if (!is_null($loadingDate) && !is_string($loadingDate)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($loadingDate, true), gettype($loadingDate)), __LINE__);
        }
        $this->loadingDate = $loadingDate;
        
        return $this;
    }
    /**
     * Get loadingTime value
     * @return string|null
     */
    public function getLoadingTime(): ?string
    {
        return $this->loadingTime;
    }
    /**
     * Set loadingTime value
     * @param string $loadingTime
     * @return \AppturePay\DSV\StructType\FileType
     */
    public function setLoadingTime(?string $loadingTime = null): self
    {
        // validation for constraint: string
        if (!is_null($loadingTime) && !is_string($loadingTime)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($loadingTime, true), gettype($loadingTime)), __LINE__);
        }
        $this->loadingTime = $loadingTime;
        
        return $this;
    }
    /**
     * Get unloadingDate value
     * @return string|null
     */
    public function getUnloadingDate(): ?string
    {
        return $this->unloadingDate;
    }
    /**
     * Set unloadingDate value
     * @param string $unloadingDate
     * @return \AppturePay\DSV\StructType\FileType
     */
    public function setUnloadingDate(?string $unloadingDate = null): self
    {
        // validation for constraint: string
        if (!is_null($unloadingDate) && !is_string($unloadingDate)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($unloadingDate, true), gettype($unloadingDate)), __LINE__);
        }
        $this->unloadingDate = $unloadingDate;
        
        return $this;
    }
    /**
     * Get unloadingTime value
     * @return string|null
     */
    public function getUnloadingTime(): ?string
    {
        return $this->unloadingTime;
    }
    /**
     * Set unloadingTime value
     * @param string $unloadingTime
     * @return \AppturePay\DSV\StructType\FileType
     */
    public function setUnloadingTime(?string $unloadingTime = null): self
    {
        // validation for constraint: string
        if (!is_null($unloadingTime) && !is_string($unloadingTime)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($unloadingTime, true), gettype($unloadingTime)), __LINE__);
        }
        $this->unloadingTime = $unloadingTime;
        
        return $this;
    }
    /**
     * Get marksAndNumber value
     * @return string|null
     */
    public function getMarksAndNumber(): ?string
    {
        return $this->marksAndNumber;
    }
    /**
     * Set marksAndNumber value
     * @param string $marksAndNumber
     * @return \AppturePay\DSV\StructType\FileType
     */
    public function setMarksAndNumber(?string $marksAndNumber = null): self
    {
        // validation for constraint: string
        if (!is_null($marksAndNumber) && !is_string($marksAndNumber)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($marksAndNumber, true), gettype($marksAndNumber)), __LINE__);
        }
        $this->marksAndNumber = $marksAndNumber;
        
        return $this;
    }
    /**
     * Get waybill value
     * @return string|null
     */
    public function getWaybill(): ?string
    {
        return $this->waybill;
    }
    /**
     * Set waybill value
     * @param string $waybill
     * @return \AppturePay\DSV\StructType\FileType
     */
    public function setWaybill(?string $waybill = null): self
    {
        // validation for constraint: string
        if (!is_null($waybill) && !is_string($waybill)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($waybill, true), gettype($waybill)), __LINE__);
        }
        $this->waybill = $waybill;
        
        return $this;
    }
    /**
     * Get houseWaybill value
     * @return string|null
     */
    public function getHouseWaybill(): ?string
    {
        return $this->houseWaybill;
    }
    /**
     * Set houseWaybill value
     * @param string $houseWaybill
     * @return \AppturePay\DSV\StructType\FileType
     */
    public function setHouseWaybill(?string $houseWaybill = null): self
    {
        // validation for constraint: string
        if (!is_null($houseWaybill) && !is_string($houseWaybill)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($houseWaybill, true), gettype($houseWaybill)), __LINE__);
        }
        $this->houseWaybill = $houseWaybill;
        
        return $this;
    }
    /**
     * Get primaryReference value
     * @return string|null
     */
    public function getPrimaryReference(): ?string
    {
        return $this->primaryReference;
    }
    /**
     * Set primaryReference value
     * @param string $primaryReference
     * @return \AppturePay\DSV\StructType\FileType
     */
    public function setPrimaryReference(?string $primaryReference = null): self
    {
        // validation for constraint: string
        if (!is_null($primaryReference) && !is_string($primaryReference)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($primaryReference, true), gettype($primaryReference)), __LINE__);
        }
        $this->primaryReference = $primaryReference;
        
        return $this;
    }
    /**
     * Get serviceLevel value
     * @return string|null
     */
    public function getServiceLevel(): ?string
    {
        return $this->serviceLevel;
    }
    /**
     * Set serviceLevel value
     * @param string $serviceLevel
     * @return \AppturePay\DSV\StructType\FileType
     */
    public function setServiceLevel(?string $serviceLevel = null): self
    {
        // validation for constraint: string
        if (!is_null($serviceLevel) && !is_string($serviceLevel)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($serviceLevel, true), gettype($serviceLevel)), __LINE__);
        }
        $this->serviceLevel = $serviceLevel;
        
        return $this;
    }
    /**
     * Get tariffType value
     * @return string|null
     */
    public function getTariffType(): ?string
    {
        return $this->tariffType;
    }
    /**
     * Set tariffType value
     * @param string $tariffType
     * @return \AppturePay\DSV\StructType\FileType
     */
    public function setTariffType(?string $tariffType = null): self
    {
        // validation for constraint: string
        if (!is_null($tariffType) && !is_string($tariffType)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($tariffType, true), gettype($tariffType)), __LINE__);
        }
        $this->tariffType = $tariffType;
        
        return $this;
    }
    /**
     * Get deliveryTerm value
     * @return string|null
     */
    public function getDeliveryTerm(): ?string
    {
        return $this->deliveryTerm;
    }
    /**
     * Set deliveryTerm value
     * @param string $deliveryTerm
     * @return \AppturePay\DSV\StructType\FileType
     */
    public function setDeliveryTerm(?string $deliveryTerm = null): self
    {
        // validation for constraint: string
        if (!is_null($deliveryTerm) && !is_string($deliveryTerm)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($deliveryTerm, true), gettype($deliveryTerm)), __LINE__);
        }
        $this->deliveryTerm = $deliveryTerm;
        
        return $this;
    }
    /**
     * Get deliveryTermPlace value
     * @return string|null
     */
    public function getDeliveryTermPlace(): ?string
    {
        return $this->deliveryTermPlace;
    }
    /**
     * Set deliveryTermPlace value
     * @param string $deliveryTermPlace
     * @return \AppturePay\DSV\StructType\FileType
     */
    public function setDeliveryTermPlace(?string $deliveryTermPlace = null): self
    {
        // validation for constraint: string
        if (!is_null($deliveryTermPlace) && !is_string($deliveryTermPlace)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($deliveryTermPlace, true), gettype($deliveryTermPlace)), __LINE__);
        }
        $this->deliveryTermPlace = $deliveryTermPlace;
        
        return $this;
    }
    /**
     * Get originPlaceCode value
     * @return string|null
     */
    public function getOriginPlaceCode(): ?string
    {
        return $this->originPlaceCode;
    }
    /**
     * Set originPlaceCode value
     * @param string $originPlaceCode
     * @return \AppturePay\DSV\StructType\FileType
     */
    public function setOriginPlaceCode(?string $originPlaceCode = null): self
    {
        // validation for constraint: string
        if (!is_null($originPlaceCode) && !is_string($originPlaceCode)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($originPlaceCode, true), gettype($originPlaceCode)), __LINE__);
        }
        $this->originPlaceCode = $originPlaceCode;
        
        return $this;
    }
    /**
     * Get destinationPlaceCode value
     * @return string|null
     */
    public function getDestinationPlaceCode(): ?string
    {
        return $this->destinationPlaceCode;
    }
    /**
     * Set destinationPlaceCode value
     * @param string $destinationPlaceCode
     * @return \AppturePay\DSV\StructType\FileType
     */
    public function setDestinationPlaceCode(?string $destinationPlaceCode = null): self
    {
        // validation for constraint: string
        if (!is_null($destinationPlaceCode) && !is_string($destinationPlaceCode)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($destinationPlaceCode, true), gettype($destinationPlaceCode)), __LINE__);
        }
        $this->destinationPlaceCode = $destinationPlaceCode;
        
        return $this;
    }
    /**
     * Get totals value
     * @return \AppturePay\DSV\StructType\TotalsType|null
     */
    public function getTotals(): ?\AppturePay\DSV\StructType\TotalsType
    {
        return $this->totals;
    }
    /**
     * Set totals value
     * @param \AppturePay\DSV\StructType\TotalsType $totals
     * @return \AppturePay\DSV\StructType\FileType
     */
    public function setTotals(?\AppturePay\DSV\StructType\TotalsType $totals = null): self
    {
        $this->totals = $totals;
        
        return $this;
    }
    /**
     * Get dimension value
     * @return \AppturePay\DSV\StructType\DimensionType|null
     */
    public function getDimension(): ?\AppturePay\DSV\StructType\DimensionType
    {
        return $this->dimension;
    }
    /**
     * Set dimension value
     * @param \AppturePay\DSV\StructType\DimensionType $dimension
     * @return \AppturePay\DSV\StructType\FileType
     */
    public function setDimension(?\AppturePay\DSV\StructType\DimensionType $dimension = null): self
    {
        $this->dimension = $dimension;
        
        return $this;
    }
    /**
     * Get parameters value
     * @return \AppturePay\DSV\StructType\ParametersType|null
     */
    public function getParameters(): ?\AppturePay\DSV\StructType\ParametersType
    {
        return $this->parameters;
    }
    /**
     * Set parameters value
     * @param \AppturePay\DSV\StructType\ParametersType $parameters
     * @return \AppturePay\DSV\StructType\FileType
     */
    public function setParameters(?\AppturePay\DSV\StructType\ParametersType $parameters = null): self
    {
        $this->parameters = $parameters;
        
        return $this;
    }
    /**
     * Get extraParameters value
     * @return \AppturePay\DSV\StructType\ExtraParametersType|null
     */
    public function getExtraParameters(): ?\AppturePay\DSV\StructType\ExtraParametersType
    {
        return $this->extraParameters;
    }
    /**
     * Set extraParameters value
     * @param \AppturePay\DSV\StructType\ExtraParametersType $extraParameters
     * @return \AppturePay\DSV\StructType\FileType
     */
    public function setExtraParameters(?\AppturePay\DSV\StructType\ExtraParametersType $extraParameters = null): self
    {
        $this->extraParameters = $extraParameters;
        
        return $this;
    }
    /**
     * Get textKeys value
     * @return \AppturePay\DSV\StructType\TextKeysType[]
     */
    public function getTextKeys(): ?array
    {
        return $this->textKeys;
    }
    /**
     * This method is responsible for validating the values passed to the setTextKeys method
     * This method is willingly generated in order to preserve the one-line inline validation within the setTextKeys method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateTextKeysForArrayConstraintsFromSetTextKeys(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $fileTypeTextKeysItem) {
            // validation for constraint: itemType
            if (!$fileTypeTextKeysItem instanceof \AppturePay\DSV\StructType\TextKeysType) {
                $invalidValues[] = is_object($fileTypeTextKeysItem) ? get_class($fileTypeTextKeysItem) : sprintf('%s(%s)', gettype($fileTypeTextKeysItem), var_export($fileTypeTextKeysItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The textKeys property can only contain items of type \AppturePay\DSV\StructType\TextKeysType, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set textKeys value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\TextKeysType[] $textKeys
     * @return \AppturePay\DSV\StructType\FileType
     */
    public function setTextKeys(?array $textKeys = null): self
    {
        // validation for constraint: array
        if ('' !== ($textKeysArrayErrorMessage = self::validateTextKeysForArrayConstraintsFromSetTextKeys($textKeys))) {
            throw new InvalidArgumentException($textKeysArrayErrorMessage, __LINE__);
        }
        $this->textKeys = $textKeys;
        
        return $this;
    }
    /**
     * Add item to textKeys value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\TextKeysType $item
     * @return \AppturePay\DSV\StructType\FileType
     */
    public function addToTextKeys(\AppturePay\DSV\StructType\TextKeysType $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \AppturePay\DSV\StructType\TextKeysType) {
            throw new InvalidArgumentException(sprintf('The textKeys property can only contain items of type \AppturePay\DSV\StructType\TextKeysType, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->textKeys[] = $item;
        
        return $this;
    }
    /**
     * Get address value
     * @return \AppturePay\DSV\StructType\AddressType[]
     */
    public function getAddress(): ?array
    {
        return $this->address;
    }
    /**
     * This method is responsible for validating the values passed to the setAddress method
     * This method is willingly generated in order to preserve the one-line inline validation within the setAddress method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateAddressForArrayConstraintsFromSetAddress(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $fileTypeAddressItem) {
            // validation for constraint: itemType
            if (!$fileTypeAddressItem instanceof \AppturePay\DSV\StructType\AddressType) {
                $invalidValues[] = is_object($fileTypeAddressItem) ? get_class($fileTypeAddressItem) : sprintf('%s(%s)', gettype($fileTypeAddressItem), var_export($fileTypeAddressItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The address property can only contain items of type \AppturePay\DSV\StructType\AddressType, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set address value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\AddressType[] $address
     * @return \AppturePay\DSV\StructType\FileType
     */
    public function setAddress(?array $address = null): self
    {
        // validation for constraint: array
        if ('' !== ($addressArrayErrorMessage = self::validateAddressForArrayConstraintsFromSetAddress($address))) {
            throw new InvalidArgumentException($addressArrayErrorMessage, __LINE__);
        }
        $this->address = $address;
        
        return $this;
    }
    /**
     * Add item to address value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\AddressType $item
     * @return \AppturePay\DSV\StructType\FileType
     */
    public function addToAddress(\AppturePay\DSV\StructType\AddressType $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \AppturePay\DSV\StructType\AddressType) {
            throw new InvalidArgumentException(sprintf('The address property can only contain items of type \AppturePay\DSV\StructType\AddressType, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->address[] = $item;
        
        return $this;
    }
    /**
     * Get additionalServices value
     * @return \AppturePay\DSV\StructType\AdditionalServicesType[]
     */
    public function getAdditionalServices(): ?array
    {
        return $this->additionalServices;
    }
    /**
     * This method is responsible for validating the values passed to the setAdditionalServices method
     * This method is willingly generated in order to preserve the one-line inline validation within the setAdditionalServices method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateAdditionalServicesForArrayConstraintsFromSetAdditionalServices(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $fileTypeAdditionalServicesItem) {
            // validation for constraint: itemType
            if (!$fileTypeAdditionalServicesItem instanceof \AppturePay\DSV\StructType\AdditionalServicesType) {
                $invalidValues[] = is_object($fileTypeAdditionalServicesItem) ? get_class($fileTypeAdditionalServicesItem) : sprintf('%s(%s)', gettype($fileTypeAdditionalServicesItem), var_export($fileTypeAdditionalServicesItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The additionalServices property can only contain items of type \AppturePay\DSV\StructType\AdditionalServicesType, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set additionalServices value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\AdditionalServicesType[] $additionalServices
     * @return \AppturePay\DSV\StructType\FileType
     */
    public function setAdditionalServices(?array $additionalServices = null): self
    {
        // validation for constraint: array
        if ('' !== ($additionalServicesArrayErrorMessage = self::validateAdditionalServicesForArrayConstraintsFromSetAdditionalServices($additionalServices))) {
            throw new InvalidArgumentException($additionalServicesArrayErrorMessage, __LINE__);
        }
        $this->additionalServices = $additionalServices;
        
        return $this;
    }
    /**
     * Add item to additionalServices value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\AdditionalServicesType $item
     * @return \AppturePay\DSV\StructType\FileType
     */
    public function addToAdditionalServices(\AppturePay\DSV\StructType\AdditionalServicesType $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \AppturePay\DSV\StructType\AdditionalServicesType) {
            throw new InvalidArgumentException(sprintf('The additionalServices property can only contain items of type \AppturePay\DSV\StructType\AdditionalServicesType, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->additionalServices[] = $item;
        
        return $this;
    }
    /**
     * Get transportInstruction value
     * @return \AppturePay\DSV\StructType\TransportInstructionType[]
     */
    public function getTransportInstruction(): ?array
    {
        return $this->transportInstruction;
    }
    /**
     * This method is responsible for validating the values passed to the setTransportInstruction method
     * This method is willingly generated in order to preserve the one-line inline validation within the setTransportInstruction method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateTransportInstructionForArrayConstraintsFromSetTransportInstruction(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $fileTypeTransportInstructionItem) {
            // validation for constraint: itemType
            if (!$fileTypeTransportInstructionItem instanceof \AppturePay\DSV\StructType\TransportInstructionType) {
                $invalidValues[] = is_object($fileTypeTransportInstructionItem) ? get_class($fileTypeTransportInstructionItem) : sprintf('%s(%s)', gettype($fileTypeTransportInstructionItem), var_export($fileTypeTransportInstructionItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The transportInstruction property can only contain items of type \AppturePay\DSV\StructType\TransportInstructionType, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set transportInstruction value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\TransportInstructionType[] $transportInstruction
     * @return \AppturePay\DSV\StructType\FileType
     */
    public function setTransportInstruction(?array $transportInstruction = null): self
    {
        // validation for constraint: array
        if ('' !== ($transportInstructionArrayErrorMessage = self::validateTransportInstructionForArrayConstraintsFromSetTransportInstruction($transportInstruction))) {
            throw new InvalidArgumentException($transportInstructionArrayErrorMessage, __LINE__);
        }
        $this->transportInstruction = $transportInstruction;
        
        return $this;
    }
    /**
     * Add item to transportInstruction value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\TransportInstructionType $item
     * @return \AppturePay\DSV\StructType\FileType
     */
    public function addToTransportInstruction(\AppturePay\DSV\StructType\TransportInstructionType $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \AppturePay\DSV\StructType\TransportInstructionType) {
            throw new InvalidArgumentException(sprintf('The transportInstruction property can only contain items of type \AppturePay\DSV\StructType\TransportInstructionType, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->transportInstruction[] = $item;
        
        return $this;
    }
    /**
     * Get planCharacteristic value
     * @return \AppturePay\DSV\StructType\PlanCharacteristicType[]
     */
    public function getPlanCharacteristic(): ?array
    {
        return $this->planCharacteristic;
    }
    /**
     * This method is responsible for validating the values passed to the setPlanCharacteristic method
     * This method is willingly generated in order to preserve the one-line inline validation within the setPlanCharacteristic method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validatePlanCharacteristicForArrayConstraintsFromSetPlanCharacteristic(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $fileTypePlanCharacteristicItem) {
            // validation for constraint: itemType
            if (!$fileTypePlanCharacteristicItem instanceof \AppturePay\DSV\StructType\PlanCharacteristicType) {
                $invalidValues[] = is_object($fileTypePlanCharacteristicItem) ? get_class($fileTypePlanCharacteristicItem) : sprintf('%s(%s)', gettype($fileTypePlanCharacteristicItem), var_export($fileTypePlanCharacteristicItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The planCharacteristic property can only contain items of type \AppturePay\DSV\StructType\PlanCharacteristicType, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set planCharacteristic value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\PlanCharacteristicType[] $planCharacteristic
     * @return \AppturePay\DSV\StructType\FileType
     */
    public function setPlanCharacteristic(?array $planCharacteristic = null): self
    {
        // validation for constraint: array
        if ('' !== ($planCharacteristicArrayErrorMessage = self::validatePlanCharacteristicForArrayConstraintsFromSetPlanCharacteristic($planCharacteristic))) {
            throw new InvalidArgumentException($planCharacteristicArrayErrorMessage, __LINE__);
        }
        $this->planCharacteristic = $planCharacteristic;
        
        return $this;
    }
    /**
     * Add item to planCharacteristic value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\PlanCharacteristicType $item
     * @return \AppturePay\DSV\StructType\FileType
     */
    public function addToPlanCharacteristic(\AppturePay\DSV\StructType\PlanCharacteristicType $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \AppturePay\DSV\StructType\PlanCharacteristicType) {
            throw new InvalidArgumentException(sprintf('The planCharacteristic property can only contain items of type \AppturePay\DSV\StructType\PlanCharacteristicType, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->planCharacteristic[] = $item;
        
        return $this;
    }
    /**
     * Get goodsLine value
     * @return \AppturePay\DSV\StructType\GoodsLineType[]
     */
    public function getGoodsLine(): ?array
    {
        return $this->goodsLine;
    }
    /**
     * This method is responsible for validating the values passed to the setGoodsLine method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGoodsLine method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGoodsLineForArrayConstraintsFromSetGoodsLine(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $fileTypeGoodsLineItem) {
            // validation for constraint: itemType
            if (!$fileTypeGoodsLineItem instanceof \AppturePay\DSV\StructType\GoodsLineType) {
                $invalidValues[] = is_object($fileTypeGoodsLineItem) ? get_class($fileTypeGoodsLineItem) : sprintf('%s(%s)', gettype($fileTypeGoodsLineItem), var_export($fileTypeGoodsLineItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The goodsLine property can only contain items of type \AppturePay\DSV\StructType\GoodsLineType, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set goodsLine value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\GoodsLineType[] $goodsLine
     * @return \AppturePay\DSV\StructType\FileType
     */
    public function setGoodsLine(?array $goodsLine = null): self
    {
        // validation for constraint: array
        if ('' !== ($goodsLineArrayErrorMessage = self::validateGoodsLineForArrayConstraintsFromSetGoodsLine($goodsLine))) {
            throw new InvalidArgumentException($goodsLineArrayErrorMessage, __LINE__);
        }
        $this->goodsLine = $goodsLine;
        
        return $this;
    }
    /**
     * Add item to goodsLine value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\GoodsLineType $item
     * @return \AppturePay\DSV\StructType\FileType
     */
    public function addToGoodsLine(\AppturePay\DSV\StructType\GoodsLineType $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \AppturePay\DSV\StructType\GoodsLineType) {
            throw new InvalidArgumentException(sprintf('The goodsLine property can only contain items of type \AppturePay\DSV\StructType\GoodsLineType, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->goodsLine[] = $item;
        
        return $this;
    }
    /**
     * Get type value
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }
    /**
     * Set type value
     * @param string $type
     * @return \AppturePay\DSV\StructType\FileType
     */
    public function setType(?string $type = null): self
    {
        // validation for constraint: string
        if (!is_null($type) && !is_string($type)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($type, true), gettype($type)), __LINE__);
        }
        $this->type = $type;
        
        return $this;
    }
}
