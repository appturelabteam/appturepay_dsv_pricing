<?php

declare(strict_types=1);

namespace AppturePay\DSV\ServiceType;

use SoapFault;
use WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Pricing ServiceType
 * @subpackage Services
 */
class Pricing extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named pricingRequest
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \AppturePay\DSV\StructType\PricingRequest $parameters
     * @return \AppturePay\DSV\StructType\PricingRequestResponse|bool
     */
    public function pricingRequest(\AppturePay\DSV\StructType\PricingRequest $parameters)
    {
        try {
            $this->setResult($resultPricingRequest = $this->getSoapClient()->__soapCall('pricingRequest', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultPricingRequest;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \AppturePay\DSV\StructType\PricingRequestResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
