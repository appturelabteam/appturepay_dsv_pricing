<?php

declare(strict_types=1);

namespace AppturePay\DSV\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for referenceIndicationType EnumType
 * @subpackage Enumerations
 */
class ReferenceIndicationType extends AbstractStructEnumBase
{
    /**
     * Constant for value '6'
     * @return string '6'
     */
    const VALUE_6 = '6';
    /**
     * Constant for value '0'
     * @return string '0'
     */
    const VALUE_0 = '0';
    /**
     * Constant for value '1'
     * @return string '1'
     */
    const VALUE_1 = '1';
    /**
     * Constant for value '9'
     * @return string '9'
     */
    const VALUE_9 = '9';
    /**
     * Return allowed values
     * @uses self::VALUE_6
     * @uses self::VALUE_0
     * @uses self::VALUE_1
     * @uses self::VALUE_9
     * @return string[]
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_6,
            self::VALUE_0,
            self::VALUE_1,
            self::VALUE_9,
        ];
    }
}
