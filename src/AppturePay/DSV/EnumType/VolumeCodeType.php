<?php

declare(strict_types=1);

namespace AppturePay\DSV\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for volumeCodeType EnumType
 * @subpackage Enumerations
 */
class VolumeCodeType extends AbstractStructEnumBase
{
    /**
     * Constant for value 'C'
     * @return string 'C'
     */
    const VALUE_C = 'C';
    /**
     * Constant for value 'L'
     * @return string 'L'
     */
    const VALUE_L = 'L';
    /**
     * Return allowed values
     * @uses self::VALUE_C
     * @uses self::VALUE_L
     * @return string[]
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_C,
            self::VALUE_L,
        ];
    }
}
