<?php

declare(strict_types=1);

namespace AppturePay\DSV\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for transactionTypeType EnumType
 * @subpackage Enumerations
 */
class TransactionTypeType extends AbstractStructEnumBase
{
    /**
     * Constant for value 'P'
     * @return string 'P'
     */
    const VALUE_P = 'P';
    /**
     * Constant for value 'S'
     * @return string 'S'
     */
    const VALUE_S = 'S';
    /**
     * Return allowed values
     * @uses self::VALUE_P
     * @uses self::VALUE_S
     * @return string[]
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_P,
            self::VALUE_S,
        ];
    }
}
