<?php return array(
    'root' => array(
        'pretty_version' => 'dev-develop',
        'version' => 'dev-develop',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => 'b183476867810fa82a7b0aa42acb97900907da9b',
        'name' => 'appturepay/dsv_pricing',
        'dev' => false,
    ),
    'versions' => array(
        'appturepay/dsv_pricing' => array(
            'pretty_version' => 'dev-develop',
            'version' => 'dev-develop',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => 'b183476867810fa82a7b0aa42acb97900907da9b',
            'dev_requirement' => false,
        ),
        'wsdltophp/packagebase' => array(
            'pretty_version' => '5.0.1',
            'version' => '5.0.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../wsdltophp/packagebase',
            'aliases' => array(),
            'reference' => '3575e50d3163dd76641bbb5398d356e310170724',
            'dev_requirement' => false,
        ),
    ),
);
